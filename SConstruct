# Copyright 2021, Francisco Oliveto <franciscoliveto@gmail.com>
# SPDX-License-Identifier: BSD-2-Clause

""" SCons build recipe for the LORAD project

Targets:

build - build the software (default)
tests - build the test programs
all   - build all

--clean  - clean all normal build targets
-c       - clean all normal build targets
"""
import atexit
import os

# package version
lorad_version = "0.1"
variant_dir = 'build'

if GetOption('clean'):
    clean_targets = '%s' % variant_dir
    atexit.register(lambda: os.system('rm -rf ' + clean_targets))

SConscript('SConscript',
           exports=['lorad_version', 'variant_dir'],
           variant_dir=variant_dir,
           duplicate=True,
           must_exist=True)
