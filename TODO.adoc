= This is lorad's to-do list.

Ideas related to the design, architecture, and feature requirements are
listed here as well.

(Please, describe each entry as much and clearly as possible.)

* Replace reset.sh helper script for actual C code within liblora.
* Add "install/uninstall" targets to scons script.
* Avoid using multi-threading as much as possible: handle concurrency through event-based
callbacks. Favor event-driven architecture. Use threading only when strictly necessary.
* GPS monitoring is not lorad responsability: let gpsd take care of that instead. Use
libgps interface to request GPS-related data to gpsd.
* Add rc initialization scripts for SySV daemon style.
