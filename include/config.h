#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <stdbool.h>

#include "lora.h"

enum {
    NIFCHANNELS = LGW_MULTI_NB,
    NRADIOS = LGW_RF_CHAIN_NB
};

struct channel {
    bool enable;
    int radio;
    int if_freq;
    int bw; /* bandwidth */
    int sf; /* spreading factor */
    int dr; /* datarate */
    bool implicit_hdr;
    bool implicit_crc;
    int implicit_payload_len;
    int implicit_coderate;
};

struct gain_lut {
    int rf_power;
    int pa_gain;
    int pwr_idx;
};

struct radio {
    bool enable;
    char type[8];
    int freq;
    double rssi_offset;
    struct tcomp {
        double coeff_a;
        double coeff_b;
        double coeff_c;
        double coeff_d;
        double coeff_e;
    } rssi_tcomp;
    struct tx {
        bool enable;
        int freq_min;
        int freq_max;
        struct gain_lut gains[16];
        int ngains;
    } tx;
};

struct config {
    int clksrc;
    char type[8]; /* SPI, USB */
    char device[16]; /* /dev/spidev0.0 */
    bool lorawan_public;
    int antenna_gain;
    bool full_duplex;
    struct fine_timestamp {
        bool enable;
        char mode[16]; /* all_sf, high_capacity */
    } ts;
    struct radio radios[NRADIOS];
    struct channel chan_lora; /* multiSF IF8 LoRa channel */
    struct channel channels[NIFCHANNELS]; /* multiSF IF[0-7] channels */
    int nchannels;
    struct beacon {
        int period;
        struct freq {
            int hz;
            int nb;
            int step;
        } freq;
        int dr; /* datarate */
        int bw; /* bandwidth */
        int power;
        int id; /* information descriptor */
    } beacon;
    struct gps {
        char device[16];
        double lat;
        double lon;
        double alt;
    } gps;
};

#endif /* !_CONFIG_H_ */

