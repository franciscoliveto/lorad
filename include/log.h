/* Copyright (c) 2021, Francisco Oliveto <franciscoliveto@gmail.com>
 * SPDX-License-Identifier: BSD-2-Clause
 */
#ifndef _LOG_H_
#define _LOG_H_

#include <stdint.h>
#include <syslog.h>

extern int loglevel;

/* Initialize logging facility. */
void initlog(const char *ident, int level);

/* Enable syslog(3) facility. */
void opensyslog();

/* Assemble log lines in printf(3) style,
   use stdout, stderr, or syslog for delivery. */
void printlog(int level, const char *fmt, ...);

/* Assemble log lines based on 'label' and the
   hexadecimal representation of 'buf' of length 'len'.
   Use stdout, stderr, or syslog for delivery. */
void printlog_hex(int level, const char *label,
                  uint8_t *hexbuf, size_t hexlen);


/* Values for level are the same as the ones defined in syslog.h.
   See syslog(3) for reference. */
#define LOG(level, ...)                         \
    do {                                        \
        if ((level) <= loglevel) {              \
            printlog(level, __VA_ARGS__);       \
        }                                       \
    } while (0)                                 \

#define log_hex(level, ...)                     \
    do {                                        \
        if ((level) <= loglevel) {              \
            printlog_hex(level, __VA_ARGS__);   \
        }                                       \
    } while (0)                                 \


#define log_emergency(...) LOG(LOG_EMERG, __VA_ARGS__)
#define log_alert(...) LOG(LOG_ALERT, __VA_ARGS__)
#define log_critical(...) LOG(LOG_CRIT, __VA_ARGS__)
#define log_error(...) LOG(LOG_ERR, __VA_ARGS__)
#define log_warning(...) LOG(LOG_WARNING, __VA_ARGS__)
#define log_notice(...) LOG(LOG_NOTICE, __VA_ARGS__)
#define log_info(...) LOG(LOG_INFO, __VA_ARGS__)
#define log_debug(...) LOG(LOG_DEBUG, __VA_ARGS__)


#endif /* !_LOG_H_ */
