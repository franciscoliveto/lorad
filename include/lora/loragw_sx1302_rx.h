/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
  (C)2019 Semtech

Description:
    SX1302 RX buffer Hardware Abstraction Layer

License: Revised BSD License, see LICENSE.TXT file include in the project
*/
#ifndef _LORAGW_SX1302_RX_H
#define _LORAGW_SX1302_RX_H

/* -------------------------------------------------------------------------- */
/* --- DEPENDANCIES --------------------------------------------------------- */

#include <stdint.h>     /* C99 types*/

#include "lorad_config.h"     /* library configuration options (dynamically generated) */


typedef struct sx1302_packet_t {
    uint8_t mid;                  /* modem ID */
    uint8_t chan;                 /* channel */
    uint8_t dr;                   /* data rate */
    uint8_t cr;                   /* coding rate */
    int32_t freq_offset;          /* frequency offset */
    struct {
        uint8_t data[255];        /* the actual payload data. */
        uint8_t len;              /* length of data. */
        uint16_t crc16;           /* CRC16 of data */
    } payload;
    uint8_t nmetrics;             /* number of timestamp metrics. */
    int8_t tsmetrics[255];        /* timestamp metrics */
    int8_t tsmetrics_stddev[255]; /* ???? */

    uint8_t flags;                /* timing set, errors, and crc enable flags */
#define FLAG_TIMING       (1<<4)
#define FLAG_HEADER_ERROR (1<<3)
#define FLAG_SYNC_ERROR   (1<<2)
#define FLAG_CRC_ENABLE   (1<<1)
#define FLAG_CRC_ERROR    (1<<0)

    int8_t snr;                   /* SNR average */

    uint8_t rssic;                /* RSSI channel */
    uint8_t rssis;                /* RSSI signal */
    uint8_t rssic_max;            /* RSSI channel max delta (pos + neg) */
    uint8_t rssis_max;            /* RSSI signal max delta (pos + neg) */
#define RSSI_MAX_POS_MASK (0xF0)
#define RSSI_MAX_SIG_MASK (0x0F)

    uint32_t timestamp;
    uint8_t checksum;
} sx1302_packet_t;


/**
@struct rx_buffer_s
@brief buffer to hold the data fetched from the sx1302 RX buffer
*/
typedef struct rx_buffer_s {
    uint8_t buffer[4096];   /*!> byte array to hald the data fetched from the RX buffer */
    uint16_t buffer_size;   /*!> The number of bytes currently stored in the buffer */
    int buffer_index;       /*!> Current parsing index in the buffer */
    uint8_t buffer_pkt_nb;
} rx_buffer_t;


/**
@brief Reset the rx_buffer instance
@param self     A pointer to a rx_buffer handler
@return LGW_REG_SUCCESS if success, LGW_REG_ERROR otherwise
*/
int rx_buffer_del(rx_buffer_t * self);

/**
@brief Fetch packets from the SX1302 internal RX buffer, and count packets available.
@param self     A pointer to a rx_buffer handler
@return LGW_REG_SUCCESS if success, LGW_REG_ERROR otherwise
*/
int rx_buffer_fetch(rx_buffer_t * self);

/**
@brief Parse the rx_buffer and return the first packet available in the given structure.
@param self     A pointer to a rx_buffer handler
@param pkt      A pointer to the structure to receive the packet parsed
@return LGW_REG_SUCCESS if success, LGW_REG_ERROR otherwise
*/
int rx_buffer_pop(rx_buffer_t * self, sx1302_packet_t *pkt);

/* -------------------------------------------------------------------------- */
/* --- DEBUG FUNCTIONS PROTOTYPES ------------------------------------------- */

uint16_t rx_buffer_read_ptr_addr(void);

uint16_t rx_buffer_write_ptr_addr(void);

void rx_buffer_dump(FILE * file, uint16_t start_addr, uint16_t end_addr);

#endif

/* --- EOF ------------------------------------------------------------------ */
