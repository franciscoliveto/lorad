/* Copyright (c) 2021, Francisco Oliveto <franciscoliveto@gmail.com>
 * SPDX-License-Identifier: BSD-2-Clause
 */
#ifndef _LORA_JSON_H_
#define _LORA_JSON_H_

#include <stdint.h>

#include "lorad.h"

char *json_gateway_marshal(char *buf, size_t size, uint64_t eui);
int json_downlink_unmarshal(const char *buf, struct downlink_t *link);
char *json_uplink_marshal(char *buf, size_t size,
                          struct lora_packet_t *packets, int n,
                          bool valid_tref, struct tref tref);

#endif /* !_LORA_JSON_H_ */
