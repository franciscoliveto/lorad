#ifndef _LORAD_H_
#define _LORAD_H_

#define DEFAULT_LORAD_PORT "2960"


struct downlink_t {
    bool immediate;
    unsigned int hwtime;
    unsigned int gpstime;
    double freq;
    unsigned int radio;
    unsigned int power;
    unsigned int sf;
    unsigned int bw;
    unsigned int preamble;
    char frame[255];
    char modulation[16];
    char cr[8];
    bool pol;
    bool disable_crc;
    bool disable_header;
};

#endif /* !_LORAD_H_ */
