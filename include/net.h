/* Copyright (c) 2021, Francisco Oliveto <franciscoliveto@gmail.com>
 * SPDX-License-Identifier: BSD-2-Clause
 */
#ifndef _NET_H_
#define _NET_H_

typedef enum network_t {
    NET_UDP,
    NET_TCP,
    NET_UDP4,
    NET_UDP6,
    NET_TCP4,
    NET_TCP6,
    NET_UNIX
} network_t;

int net_listen(network_t network, const char *node,
               const char *service, int backlog);
int net_dial(network_t network, const char *host, const char *service);
const char *net_strerror(int retnum, int errnum);

#endif /* !_NET_H_ */
