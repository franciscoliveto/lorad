/* Copyright (c) 2021, Francisco Oliveto <franciscoliveto@gmail.com>
 * SPDX-License-Identifier: BSD-2-Clause
 */
#ifndef _OS_H_
#define _OS_H_

/* Detach from controlling terminal and run in the
   background as a system deamon. */
int daemonize();
int mssleep(long int msec);
size_t strlcpy(char *dst, const char *src, size_t siz);
size_t strlcat(char *dst, const char *src, size_t siz);

#endif /* !_OS_H_ */
