/* Copyright (c) 2021, Francisco Oliveto <franciscoliveto@gmail.com>
 * SPDX-License-Identifier: BSD-2-Clause
 */
#ifndef _STRFUNCS_H_
#define _STRFUNCS_H_

/* Appends the variable list of arguments to 's' in snprintf(3) style. */
void sappendf(char *s, size_t size, const char *fmt, ...);

static inline bool str_startswith(const char *s, const char *prefix)
{
    return strncmp(s, prefix, strlen(prefix)) == 0;
}

#endif /* !_STRFUNCS_H_ */
