// Copyright (c) 2022, Francisco Oliveto <franciscoliveto@gmail.com>
// SPDX-License-Identifier: BSD-2-Clause
//
// Semtech's legacy interface.
//
// legacy is responsible for handling the details of the Semtech's
// UDP protocol logic. It works cooperatibely with lorad engine via
// JSON-based requests and responses over a socket.
// legacy interface is intended to be used as a tool for testing
// lorad communication with a network server. The use of it is
// discourage on production environments.
//
// Refer to lorad_json(5) manual for more details about lorad's
// interface mechanism.
// For a detailed description of Semtech UDP protocol refer to:
// https://github.com/Lora-net/sx1302_hal/blob/master/packet_forwarder/PROTOCOL.md
package main

import (
	"bufio"
	"bytes"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"
)

const (
	pushACK  = 1
	protoVer = 2
	pullACK  = 4
	txACK    = 5
)

type RxPacket struct {
	Jver  int     `json:"jver"`
	Time  string  `json:"time"`
	Tmms  uint64  `json:"tmms"`
	Tmst  uint64  `json:"tmst"`
	Ftime int     `json:"ftime"`
	Freq  float64 `json:"freq"`
	Chan  int     `json:"chan"`
	Rfch  int     `json:"rfch"`
	Mid   int     `json:"mid"`
	Stat  int     `json:"stat"`
	Modu  string  `json:"modu"`
	Datr  string  `json:"datr"`
	Codr  string  `json:"codr"`
	Rssi  int     `json:"rssi"`
	Rssis int     `json:"rssis"`
	Lsnr  float64 `json:"lsnr"`
	Foff  int     `json:"foff"`
	Size  int     `json:"size"`
	Data  string  `json:"data"`
}

type PushData struct {
	Rxpk []RxPacket `json:"rxpk"`
}

type TxPacket struct {
	Imme bool    `json:"imme"`
	Tmst uint64  `json:"tmst"`
	Tmms uint64  `json:"tmms"`
	Freq float64 `json:"freq"`
	Rfch int     `json:"rfch"`
	Powe int     `json:"powe"`
	Modu string  `json:"modu"`
	Datr string  `json:"datr"`
	Codr string  `json:"codr"`
	Fdev int     `json:"fdev"`
	Ipol bool    `json:"ipol"`
	Prea int     `json:"prea"`
	Size int     `json:"size"`
	Data string  `json:"data"`
	Ncrc bool    `json:"ncrc"`
	Nhdr bool    `json:"nhdr"`
}

type PullResp struct {
	Txpk TxPacket `json:"txpk"`
}

type Packet struct {
	Immediate       bool    `json:"immediate"`
	Time            string  `json:"time,omitempty"`
	GPSTime         uint64  `json:"gpstime"`
	HWTime          uint64  `json:"hwtime"`
	Status          int     `json:"status,omitempty"`
	Frequency       float64 `json:"frequency"`
	Channel         int     `json:"channel,omitempty"`
	Radio           int     `json:"radio"`
	Modem           int     `json:"modem,omitempty"`
	Modulation      string  `json:"modulation"` // FIXME: only LoRa.
	SpreadingFactor int     `json:"spreading_factor"`
	Bandwidth       int     `json:"bandwidth"`
	CodingRate      string  `json:"coding_rate"`
	ChannelRSSI     int     `json:"channel_rssi,omitempty"`
	RSSI            int     `json:"rssi,omitempty"`
	SNR             float64 `json:"snr,omitempty"`
	FrequencyOffset int     `json:"frequency_offset,omitempty"`
	Frame           string  `json:"frame"` // base64-encoded
	Power           int     `json:"power"`
	Polarity        bool    `json:"polarity"`
	Preamble        int     `json:"preamble,omitempty"`
	DisableCRC      bool    `json:"disable_crc,omitempty"`
	DisableHeader   bool    `json:"disable_header,omitempty"`
}

type Uplink struct {
	Packets []Packet `json:"packets"`
}

type Downlink struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

type Version struct {
	Release    string  `json:"release"`
	ProtoMajor float64 `json:"proto_major"`
	ProtoMinor float64 `json:"proto_minor"`
}

type Gateway struct {
	EUI     string  `json:"eui"`
	Version Version `json:"version"`
}

const (
	defaultEngineHost = "localhost"
	defaultEnginePort = "2960"
	defaultHost       = "localhost"
	defaultPort       = "1700"
)

var gateway Gateway
var hexEUI []byte

func main() {
	var err error
	var conn net.Conn
	var connUp net.Conn   // upstream socket
	var connDown net.Conn // downstream socket
	var host = defaultHost
	var port = defaultPort
	var pollInterval int  // -t
	var engineHost string // -host
	var enginePort string // -port

	flag.IntVar(&pollInterval, "t", 10,
		"Poll data from the Server every <t> seconds")
	flag.StringVar(&engineHost, "host", defaultEngineHost,
		"Connect to lorad engine on <host>")
	flag.StringVar(&enginePort, "port", defaultEnginePort,
		"Connect to lorad engine on <port>")

	flag.Usage = usage
	flag.Parse()
	log.SetFlags(0)

	if pollInterval < 0 {
		log.Fatal("time can't be negative\n")
	}

	n := flag.NArg()
	if n > 1 {
		log.Fatal("too many arguments\n")
	}
	if n == 1 {
		h, p, err := splitHostPort(flag.Arg(0))
		if err != nil {
			log.Fatalf("invalid argument format: %v\n", err)
		}
		if h != "" {
			host = h
		}
		if p != "" {
			port = p
		}
	}

	conn, err = net.Dial("tcp", net.JoinHostPort(engineHost, enginePort))
	if err != nil {
		log.Fatalf("%v\n", err)
	}
	defer conn.Close()

	log.Printf("connected to lorad (%s)\n", conn.RemoteAddr().String())

	connUp, err = net.Dial("udp", net.JoinHostPort(host, port))
	if err != nil {
		log.Fatalf("%v\n", err)
	}
	defer connUp.Close()

	connDown, err = net.Dial("udp", net.JoinHostPort(host, port))
	if err != nil {
		log.Fatalf("%v\n", err)
	}
	defer connDown.Close()

	log.Printf("connected to Network Server (%s)\n",
		connUp.RemoteAddr().String())

	upch := make(chan []Packet)
	downch := make(chan Packet)
	init := make(chan int)

	// Upstream forwards incoming packets to the Network Server.
	go func() {
		for {
			packets := <-upch
			if err := pushData(connUp, packets, 5*time.Second); err != nil {
				log.Printf("upstream: %v\n", err)
			}
		}
	}()

	// Downstream pulls packets from the Network Server and forwards
	// them to the engine.
	go func() {
		<-init // blocks until GATEWAY event is arrived

		interval := time.Duration(pollInterval) * time.Second
		t := time.Now() // pull right away
		for {
			if time.Now().After(t) {
				if err := pollData(connDown, 5*time.Second); err != nil {
					log.Printf("downstream: %v\n", err)
				}
				t = time.Now().Add(interval)
			}
			if resp, err := pullResp(connDown, 1*time.Second); err != nil {
				// pullResp returns an error that wraps os.ErrDeadlineExceeded
				// indicating that the timeout has expired. This is not an actual
				// error. It just indicates that there's no data available.
				// So it is ignored.
				if !errors.Is(err, os.ErrDeadlineExceeded) {
					// an actual error
					log.Printf("downstream: %v\n", err)
				}
			} else {
				if p, err := unmarshal(resp); err != nil {
					log.Printf("downstream: %v\n", err)
				} else {
					downch <- *p // p is of type *Packet
				}
			}
		}
	}()

	r := bufio.NewReader(conn)
	for {
		select {
		case packet := <-downch:
			if p, err := json.Marshal(packet); err != nil {
				log.Printf("Packet marshaling failed: %v\n", err)
			} else {
				s := "?DOWNLINK=" + string(p) + "\n"
				log.Print("Outgoing message: ", s) // for debug
				fmt.Fprint(conn, s)                // send it to lorad
			}
		default:
			conn.SetReadDeadline(time.Now().Add(200 * time.Millisecond))
			s, err := r.ReadString('\n')
			if err != nil {
				if errors.Is(err, os.ErrDeadlineExceeded) {
					// No data available.
					continue
				}
				if err == io.EOF {
					log.Fatal("Connection was closed.\n")
				}
				log.Printf("reading failed: %v\n", err)
			}
			// a possibly new event has arrived
			s = s[:len(s)-1]                        // discard \n
			log.Printf("incoming message: %v\n", s) // for debug
			if err := handle(s, init, upch); err != nil {
				log.Fatalf("handle failed: %v\n", err)
			}
		}
	}
}

func pullResp(conn net.Conn, timeout time.Duration) ([]byte, error) {
	var n int
	var err error
	buf := make([]byte, 4098)

	conn.SetReadDeadline(time.Now().Add(timeout))
	if n, err = conn.Read(buf); err != nil {
		return nil, fmt.Errorf("pullResp failed: %w\n", err)
	}

	log.Printf("PULL_RESP (header): % x\n", buf[:4])          // for debug
	log.Printf("PULL_RESP (payload): %v\n", string(buf[4:n])) // for debug

	// TX_ACK
	b := new(bytes.Buffer)
	b.WriteByte(protoVer)
	b.Write(buf[1:3]) // token
	b.WriteByte(txACK)
	b.Write(hexEUI)
	if _, err = io.Copy(conn, b); err != nil {
		return nil, fmt.Errorf("pullResp failed: %v\n", err)
	}
	return buf[4:n], nil
}

func pushData(conn net.Conn, packets []Packet, timeout time.Duration) error {
	var err error
	var payload []byte

	token := []byte{2, 4} // doesn't matter the actual values
	buf := new(bytes.Buffer)
	buf.WriteByte(protoVer)
	buf.Write(token)
	buf.WriteByte(0x0)
	buf.Write(hexEUI)

	if payload, err = marshal(packets); err != nil {
		return fmt.Errorf("pushData failed: %v", err)
	}
	log.Printf("PUSH_DATA (header): % x\n", buf)             // for debug
	log.Printf("PUSH_DATA (payload): %v\n", string(payload)) // for debug

	buf.Write(payload)
	if _, err = io.Copy(conn, buf); err != nil {
		return fmt.Errorf("pushData failed: %v", err)
	}

	t := time.Now().Add(timeout)
	conn.SetReadDeadline(t)
	resp := make([]byte, 4)
	if _, err = io.ReadAtLeast(conn, resp, len(resp)); err != nil {
		if errors.Is(err, os.ErrDeadlineExceeded) {
			return fmt.Errorf("pushData failed: PUSH_ACK timeout\n")
		}
		return fmt.Errorf("pushData failed: %v", err)
	}
	log.Printf("PUSH_RESP: % x\n", resp) // for debug
	return nil
}

func pollData(conn net.Conn, timeout time.Duration) error {
	token := []byte{2, 4}
	buf := new(bytes.Buffer)
	buf.WriteByte(protoVer)
	buf.Write(token)
	buf.WriteByte(0x02)
	buf.Write(hexEUI)

	log.Printf("PULL_DATA: % x\n", buf)

	if _, err := io.Copy(conn, buf); err != nil {
		return fmt.Errorf("poll failed: %v\n", err)
	}
	conn.SetReadDeadline(time.Now().Add(timeout))
	b := make([]byte, 4)
	if _, err := io.ReadAtLeast(conn, b, len(b)); err != nil {
		return fmt.Errorf("poll failed: %w\n", err)
	}

	log.Printf("PULL_ACK: % x\n", b)

	if b[3] != pullACK || !bytes.Contains(b, token) {
		return fmt.Errorf("poll failed: invalid response\n")
	}
	return nil
}

func usage() {
	fmt.Fprintf(flag.CommandLine.Output(), `Usage: %s [OPTIONS] [host[:port]]

host can be a numerical IP address or a symbolic hostname.

`, os.Args[0])
	flag.PrintDefaults()
}

// splitHostPort splits a network address of the form "host:port" into host and port.
func splitHostPort(hostport string) (host, port string, err error) {
	s := strings.Split(hostport, ":")
	if l := len(s); l == 1 {
		host = s[0]
		port = ""
	} else if l == 2 {
		host = s[0]
		port = s[1]
	} else {
		err = fmt.Errorf("too many colons in address")
	}
	return
}

// handle handles incoming events from the server (lorad engine).
func handle(s string, init chan<- int, c chan<- []Packet) error {
	event, arg, ok := strings.Cut(s, "=")
	if !ok {
		return fmt.Errorf("invalid event format: %s\n", s)
	}
	switch event {
	case "GATEWAY":
		return handleGateway(arg, init)
	case "UPLINK":
		return handleUplink(arg, c)
	case "DOWNLINK":
		// A ?DOWNLINK= command has been previously sent.
		// This is its response.
		return handleDownlink(arg)
	default:
		return fmt.Errorf("JSON event %v is not supported", event)
	}
}

func handleDownlink(s string) error {
	var d Downlink

	if err := json.Unmarshal([]byte(s), &d); err != nil {
		return fmt.Errorf("handleDownlink failed: %v", err)
	}
	// TODO: do we send TX_ACK back to the server right now?
	log.Printf("handleDownlink: %s\n", d.Status) // for debug
	return nil
}

// handleGateway parses the JSON-encoded GATEWAY response s.
func handleGateway(s string, init chan<- int) error {
	var err error
	if err = json.Unmarshal([]byte(s), &gateway); err != nil {
		return fmt.Errorf("handleGateway failed: %v", err)
	}
	if hexEUI, err = hex.DecodeString(gateway.EUI); err != nil {
		return fmt.Errorf("handleGateway failed: %v", err)
	}
	log.Printf("EUI: %v\n", gateway.EUI)
	log.Printf("lorad version %v, JSON protocol v%v.%v\n",
		gateway.Version.Release, gateway.Version.ProtoMajor,
		gateway.Version.ProtoMinor)
	init <- 1 // signal downstream goroutine
	return nil
}

// hanldeUplink parses the JSON-encoded uplink event s,
// and sends the resulting packets through channel c.
func handleUplink(s string, c chan<- []Packet) error {
	var u Uplink

	if err := json.Unmarshal([]byte(s), &u); err != nil {
		return fmt.Errorf("handleUplink failed: %v", err)
	}
	c <- u.Packets
	return nil
}

// unmarshal parses the Semtech downstream JSON-encoded data and returns
// the result as a *Packet.
func unmarshal(data []byte) (*Packet, error) {
	var resp PullResp
	var p Packet

	if err := json.Unmarshal(data, &resp); err != nil {
		return nil, fmt.Errorf("JSON unmarshaling failed: %v\n", err)
	}
	p.Immediate = resp.Txpk.Imme
	p.GPSTime = resp.Txpk.Tmms
	p.HWTime = resp.Txpk.Tmst
	p.Frequency = resp.Txpk.Freq
	p.Radio = resp.Txpk.Rfch
	p.Power = resp.Txpk.Powe
	if resp.Txpk.Modu == "LORA" {
		p.Modulation = "LoRa"
		fmt.Sscanf(resp.Txpk.Datr, "SF%dBW%d", &p.SpreadingFactor, &p.Bandwidth)
	} else {
		// FIXME: packet is discarded
		return nil, fmt.Errorf("Modulation %v is not supported\n",
			resp.Txpk.Modu)
	}
	p.CodingRate = resp.Txpk.Codr
	p.Polarity = resp.Txpk.Ipol
	p.Preamble = resp.Txpk.Prea
	p.DisableCRC = resp.Txpk.Ncrc
	p.DisableHeader = resp.Txpk.Nhdr
	p.Frame = resp.Txpk.Data
	return &p, nil
}

// marshal returns the Semtech upstream's JSON encoding of packets.
func marshal(packets []Packet) ([]byte, error) {
	var data PushData
	var rp RxPacket

	const (
		invalid = iota
		noCRC
		valid
	)
	// CRC status conversion table
	convtab := [3]int{invalid: -1, noCRC: 0, valid: 1}

	rp.Jver = 1
	for _, p := range packets {
		rp.Time = p.Time
		rp.Tmms = p.GPSTime
		rp.Tmst = p.HWTime
		rp.Freq = p.Frequency
		rp.Chan = p.Channel
		rp.Rfch = p.Radio
		rp.Mid = p.Modem
		if p.Status >= 0 && p.Status < len(convtab) {
			rp.Stat = convtab[p.Status]
		} else {
			return nil,
				fmt.Errorf("invalid value (%d) in field status", p.Status)
		}
		rp.Modu = strings.ToUpper(p.Modulation)
		rp.Datr = "SF" + strconv.Itoa(p.SpreadingFactor) + "BW" + strconv.Itoa(p.Bandwidth)
		rp.Codr = p.CodingRate
		rp.Rssi = p.ChannelRSSI
		rp.Rssis = p.RSSI
		rp.Lsnr = p.SNR
		rp.Foff = p.FrequencyOffset

		frame, err := base64.StdEncoding.DecodeString(p.Frame)
		if err != nil {
			return nil, fmt.Errorf("base64 decoding failed: %v", err)
		}
		rp.Size = len(frame)
		rp.Data = p.Frame

		data.Rxpk = append(data.Rxpk, rp)
	}

	s, err := json.Marshal(data)
	if err != nil {
		return nil, fmt.Errorf("JSON marshaling failed: %v", err)
	}
	return s, nil
}
