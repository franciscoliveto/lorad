// SPDX-License-Identifier: BSD-2-Clause
package main

import (
	"fmt"
	"testing"
)

func TestSplitHostPort(t *testing.T) {
	var tests = []struct {
		input      string
		host, port string
		err        error
	}{
		{"localhost:2960", "localhost", "2960", nil},
		{"localhost", "localhost", "", nil},
		{"localhost:", "localhost", "", nil},
		{":", "", "", nil},
		{":2960", "", "2960", nil},
	}
	for _, test := range tests {
		h, p, err := splitHostPort(test.input)
		if h != test.host || p != test.port || err != test.err {
			t.Errorf("splitHostPort(%q) = %q, %q, %v, want %q, %q, %v",
				test.input, h, p, err, test.host, test.port, test.err)
		}
	}

	specialCase := "localhost:2960:3333:192.168.0.51"
	splitError := fmt.Errorf("too many colons in address")
	h, p, err := splitHostPort(specialCase)
	if h != "" || p != "" || err == nil {
		t.Errorf(`splitHostPort(%q) = %q, %q, %v, want "", "", %v`,
			specialCase, h, p, err, splitError)
	}
}
