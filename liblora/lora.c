/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
  (C)2019 Semtech

Description:
    LoRa concentrator Hardware Abstraction Layer

License: Revised BSD License, see LICENSE.TXT file include in the project
*/

/* fix an issue between POSIX and C99 */
#if __STDC_VERSION__ >= 199901L
    #define _XOPEN_SOURCE 600
#else
    #define _XOPEN_SOURCE 500
#endif

#define _GNU_SOURCE     /* needed for qsort_r to be defined */
#include <stdlib.h>     /* qsort_r */

#include <stdint.h>     /* C99 types */
#include <stdbool.h>    /* bool type */
#include <stdio.h>      /* printf fprintf */
#include <string.h>     /* memcpy */
#include <unistd.h>     /* symlink, unlink */
#include <inttypes.h>
#include <assert.h>

#include "loragw_reg.h"
#include "loragw_aux.h"
#include "loragw_com.h"
#include "loragw_i2c.h"
#include "loragw_lbt.h"
#include "loragw_sx1250.h"
#include "loragw_sx125x.h"
#include "loragw_sx1261.h"
#include "loragw_sx1302_timestamp.h"
#include "loragw_stts751.h"
#include "loragw_ad5338r.h"

#include "lora.h"
#include "driver_sx1302.h"
#include "log.h"


#define CONTEXT_COM_TYPE        lgw_context.board_cfg.com_type
#define CONTEXT_COM_PATH        lgw_context.board_cfg.com_path
#define CONTEXT_LWAN_PUBLIC     lgw_context.board_cfg.lorawan_public
#define CONTEXT_BOARD           lgw_context.board_cfg
#define CONTEXT_RF_CHAIN        lgw_context.rf_chain_cfg
#define CONTEXT_IF_CHAIN        lgw_context.if_chain_cfg
#define CONTEXT_DEMOD           lgw_context.demod_cfg
#define CONTEXT_LORA_SERVICE    lgw_context.lora_service_cfg
#define CONTEXT_FSK             lgw_context.fsk_cfg
#define CONTEXT_TX_GAIN_LUT     lgw_context.tx_gain_lut
#define CONTEXT_FINE_TIMESTAMP  lgw_context.ftime_cfg
#define CONTEXT_SX1261          lgw_context.sx1261_cfg


#define FW_VERSION_AGC_SX1250   10 /* Expected version of AGC firmware for sx1250 based gateway */
                                   /* v10 is same as v6 with improved channel check time for LBT */
#define FW_VERSION_AGC_SX125X   6  /* Expected version of AGC firmware for sx1255/sx1257 based gateway */
#define FW_VERSION_ARB          2  /* Expected version of arbiter firmware */

/* Useful bandwidth of SX125x radios to consider depending on channel bandwidth */
/* Note: the below values come from lab measurements. For any question, please contact Semtech support */
#define LGW_RF_RX_BANDWIDTH_125KHZ  1600000     /* for 125KHz channels */
#define LGW_RF_RX_BANDWIDTH_250KHZ  1600000     /* for 250KHz channels */
#define LGW_RF_RX_BANDWIDTH_500KHZ  1600000     /* for 500KHz channels */

#define LGW_RF_RX_FREQ_MIN          100E6
#define LGW_RF_RX_FREQ_MAX          1E9

/* Version string, used to identify the library version/options once compiled */
const char lgw_version_string[] = "Version: " LIBLORA_VERSION ";";

#include "arb_fw.var"           /* text_arb_sx1302_13_Nov_3 */
#include "agc_fw_sx1250.var"    /* text_agc_sx1250_05_Juillet_2019_3 */
#include "agc_fw_sx1257.var"    /* text_agc_sx1257_19_Nov_1 */

/*
The following static variable holds the gateway configuration provided by the
user that need to be propagated in the drivers.

Parameters validity and coherency is verified by the _setconf functions and
the _start and _send functions assume they are valid.
*/
static lgw_context_t lgw_context = {
    .board_cfg.com_type = LGW_COM_SPI,
    .board_cfg.com_path = "/dev/spidev0.0",
    .board_cfg.lorawan_public = true,
    .board_cfg.clksrc = 0,
    .board_cfg.full_duplex = false,
    .rf_chain_cfg = {{0}},
    .if_chain_cfg = {{0}},
    .demod_cfg = {
        .multisf_datarate = LGW_MULTI_SF_EN
    },
    .lora_service_cfg = {
        .enable = 0,    /* not used, handled by if_chain_cfg */
        .rf_chain = 0,  /* not used, handled by if_chain_cfg */
        .freq_hz = 0,   /* not used, handled by if_chain_cfg */
        .bandwidth = BW_250KHZ,
        .datarate = DR_LORA_SF7,
        .implicit_hdr = false,
        .implicit_payload_length = 0,
        .implicit_crc_en = 0,
        .implicit_coderate = 0
    },
    .fsk_cfg = {
        .enable = 0,    /* not used, handled by if_chain_cfg */
        .rf_chain = 0,  /* not used, handled by if_chain_cfg */
        .freq_hz = 0,   /* not used, handled by if_chain_cfg */
        .bandwidth = BW_125KHZ,
        .datarate = 50000,
        .sync_word_size = 3,
        .sync_word = 0xC194C1
    },
    .tx_gain_lut = {
        {
            .size = 1,
            .lut[0] = {
                .rf_power = 14,
                .dig_gain = 0,
                .pa_gain = 2,
                .dac_gain = 3,
                .mix_gain = 10,
                .offset_i = 0,
                .offset_q = 0,
                .pwr_idx = 0
            }
        },{
            .size = 1,
            .lut[0] = {
                .rf_power = 14,
                .dig_gain = 0,
                .pa_gain = 2,
                .dac_gain = 3,
                .mix_gain = 10,
                .offset_i = 0,
                .offset_q = 0,
                .pwr_idx = 0
            }
        }
    },
    .ftime_cfg = {
        .enable = false,
        .mode = LGW_FTIME_MODE_ALL_SF
    },
    .sx1261_cfg = {
        .enable = false,
        .spi_path = "/dev/spidev0.1",
        .rssi_offset = 0,
        .lbt_conf = {
            .rssi_target = 0,
            .nb_channel = 0,
            .channels = {{ 0 }}
        }
    }
};


/* I2C temperature sensor handles */
static int     ts_fd = -1;
static uint8_t ts_addr = 0xFF;

/* I2C AD5338 handles */
static int     ad_fd = -1;

int32_t lgw_sf_getval(int x);
int32_t lgw_bw_getval(int x);

static bool is_same_pkt(struct lora_packet_t *p1, struct lora_packet_t *p2);
static int remove_pkt(struct lora_packet_t * p, uint8_t * nb_pkt, uint8_t pkt_index);
static int merge_packets(struct lora_packet_t * p, uint8_t * nb_pkt);


int32_t lgw_bw_getval(int x)
{
    switch (x) {
        case BW_500KHZ: return 500000;
        case BW_250KHZ: return 250000;
        case BW_125KHZ: return 125000;
        default: return -1;
    }
}

int32_t lgw_sf_getval(int x)
{
    switch (x) {
        case DR_LORA_SF5: return 5;
        case DR_LORA_SF6: return 6;
        case DR_LORA_SF7: return 7;
        case DR_LORA_SF8: return 8;
        case DR_LORA_SF9: return 9;
        case DR_LORA_SF10: return 10;
        case DR_LORA_SF11: return 11;
        case DR_LORA_SF12: return 12;
        default: return -1;
    }
}

static bool is_same_pkt(struct lora_packet_t *p1, struct lora_packet_t *p2)
{
    if ((p1 != NULL) && (p2 != NULL)) {
        /* Criterias to determine if packets are identical:
            -- count_us should be equal or can have up to 24µs of difference (3 samples)
            -- channel should be same
            -- datarate should be same
            -- payload should be same
        */
        if ((abs((int)(p1->count_us - p2->count_us)) <= 24) &&
            (p1->if_chain == p2->if_chain) &&
            (p1->datarate == p2->datarate) &&
            (p1->size == p2->size) &&
            (memcmp(p1->payload, p2->payload, p1->size) == 0)) {

            return true;
        }
    }

    return false;
}

static int remove_pkt(struct lora_packet_t * p, uint8_t * nb_pkt, uint8_t pkt_index)
{
    if (pkt_index > ((*nb_pkt) - 1)) {
        log_error("failed to remove packet index %u\n", pkt_index);
        return -1;
    }

    /* Remove pkt from array, by replacing it with last packet of array */
    if (pkt_index == ((*nb_pkt) - 1)) {
        /* If we remove last element, just decrement nb packet counter */
        /* Do nothing */
    } else {
        /* Copy last packet onto the packet to be removed */
        memcpy(p + pkt_index, p + (*nb_pkt) - 1, sizeof(struct lora_packet_t));
    }

    *nb_pkt -= 1;

    return 0;
}

int compare_pkt_tmst(const void *a, const void *b, void *arg)
{
    struct lora_packet_t *p = (struct lora_packet_t *)a;
    struct lora_packet_t *q = (struct lora_packet_t *)b;
    int *counter = (int *)arg;
    int p_count, q_count;

    p_count = p->count_us;
    q_count = q->count_us;

    if (p_count > q_count) {
        *counter = *counter + 1;
    }

    return (p_count - q_count);
}

static int merge_packets(struct lora_packet_t * p, uint8_t * nb_pkt)
{
    uint8_t cpt;
    int j, k, pkt_dup_idx, x;
    int pkt_idx;
    bool dup_restart = false;
    int counter_qsort_swap = 0;

    /* Init number of packets in array before merge */
    cpt = *nb_pkt;

    if (cpt > 0) {
        log_debug("<----- Searching for DUPLICATEs ------\n");
    }
    for (j = 0; j < cpt; j++) {
        log_debug("  %d: tmst=%u SF=%u CRC_status=%d freq=%u chan=%u",
                j, p[j].count_us, p[j].datarate, p[j].status,
                p[j].freq_hz, p[j].if_chain);
        if (p[j].ftime_received == true) {
            log_debug(" ftime=%u\n", p[j].ftime);
        } else {
            log_debug(" ftime=NONE\n");
        }
    }

    /* Remove duplicates */
    j = 0;
    while (j < cpt) {
        for (k = (j+1); k < cpt; k++) {
            /* Searching for duplicated packets:
                -- count_us should be equal or can have up to 24µs of difference (3 samples)
                -- channel should be same
                -- datarate should be same
                -- payload should be same
            */
            if (is_same_pkt( &p[j], &p[k])) {
                /* We keep the packet which has CRC checked */
                if ((p[j].status == STAT_CRC_OK) && (p[k].status == STAT_CRC_BAD)) {
                    pkt_dup_idx = k;

                    pkt_idx = j;
                } else if ((p[j].status == STAT_CRC_BAD) && (p[k].status == STAT_CRC_OK)) {
                    pkt_dup_idx = j;

                    pkt_idx = k;
                } else {
                    /* we keep the packet which has a fine timestamp */
                    if (p[j].ftime_received == true) {
                        pkt_dup_idx = k;
                        pkt_idx = j;
                    } else {
                        pkt_dup_idx = j;
                        pkt_idx = k;
                    }
                    /* sanity check */
                    if (((p[j].ftime_received == true) &&
                         (p[k].ftime_received == true)) ||
                        ((p[j].ftime_received == false) &&
                         (p[k].ftime_received == false))) {
                        log_debug("WARNING: both duplicates have fine"
                                " timestamps, or none has ? TBC\n");
                    }
                }
                /* pkt_dup_idx contains the index to be deleted */
                log_debug("duplicate found %d:%d, deleting %d\n",
                        pkt_idx, pkt_dup_idx, pkt_dup_idx);

                /* Remove duplicated packet from packet array */
                x = remove_pkt(p, &cpt, pkt_dup_idx);
                if (x != 0) {
                    log_error("failed to remove packet "
                            "from array (%d)\n", x);
                }
                dup_restart = true;
                break;
            }
        }
        if (dup_restart == true) {
            /* Duplicate found, restart searching for 
               duplicate from first element */
            j = 0;
            dup_restart = false;
        } else {
            /* No duplicate found, continue... */
            j += 1;
        }
    }

    /* Sort the packet array by ascending counter_us value */
    qsort_r(p, cpt, sizeof(p[0]), compare_pkt_tmst, &counter_qsort_swap);
    log_debug("%d elements swapped during sorting...\n",
            counter_qsort_swap);

    if (cpt > 0) {
        log_debug("--\n");
    }
    for (j = 0; j < cpt; j++) {
        log_debug("  %d: tmst=%u SF=%d CRC_status=%d freq=%u chan=%u",
                j, p[j].count_us, p[j].datarate, p[j].status,
                p[j].freq_hz, p[j].if_chain);
        if (p[j].ftime_received == true) {
            log_debug(" ftime=%u\n", p[j].ftime);
        } else {
            log_debug(" ftime=NONE\n");
        }
    }
    if (cpt > 0) {
        log_debug(" ----------------------->\n\n" );
    }

    /* Update number of packets contained in packet array */
    *nb_pkt = cpt;

    return 0;
}

int lgw_board_setconf(struct lgw_conf_board_s *conf)
{
    /* Check input parameters */
    if ((conf->com_type != LGW_COM_SPI) &&
        (conf->com_type != LGW_COM_USB)) {
        log_error("WRONG COM TYPE\n");
        return -1;
    }

    /* set internal config according to parameters */
    CONTEXT_LWAN_PUBLIC = conf->lorawan_public;
    CONTEXT_BOARD.clksrc = conf->clksrc;
    CONTEXT_BOARD.full_duplex = conf->full_duplex;
    CONTEXT_COM_TYPE = conf->com_type;
    strncpy(CONTEXT_COM_PATH, conf->com_path, sizeof CONTEXT_COM_PATH);
    CONTEXT_COM_PATH[sizeof CONTEXT_COM_PATH - 1] = '\0';

    log_debug("board configuration: com_type: %s, "
            "com_path: %s, lorawan_public:%d, clksrc:%d, full_duplex:%d\n",
            (CONTEXT_COM_TYPE == LGW_COM_SPI) ? "SPI" : "USB",
            CONTEXT_COM_PATH, CONTEXT_LWAN_PUBLIC, CONTEXT_BOARD.clksrc,
            CONTEXT_BOARD.full_duplex);

    return 0;
}

int lgw_rxrf_setconf(uint8_t rf_chain, struct lgw_conf_rxrf_s * conf)
{
    if (conf->enable == false) {
        /* nothing to do */
        log_debug("rf_chain %d disabled\n", rf_chain);
        return 0;
    }

    if (rf_chain >= LGW_RF_CHAIN_NB) {
        log_error("NOT A VALID RF_CHAIN NUMBER\n");
        return -1;
    }

    if ((conf->type != LGW_RADIO_TYPE_SX1255) &&
        (conf->type != LGW_RADIO_TYPE_SX1257) &&
        (conf->type != LGW_RADIO_TYPE_SX1250)) {
        log_error("NOT A VALID RADIO TYPE (%d)\n", conf->type);
        return -1;
    }

    if ((conf->freq_hz < LGW_RF_RX_FREQ_MIN) ||
        (conf->freq_hz > LGW_RF_RX_FREQ_MAX)) {
        log_error("NOT A VALID RADIO CENTER FREQUENCY, "
                "PLEASE CHECK IF IT HAS BEEN GIVEN IN HZ (%u)\n",
                conf->freq_hz);
        return -1;
    }

    CONTEXT_RF_CHAIN[rf_chain].enable = conf->enable;
    CONTEXT_RF_CHAIN[rf_chain].freq_hz = conf->freq_hz;
    CONTEXT_RF_CHAIN[rf_chain].rssi_offset = conf->rssi_offset;
    CONTEXT_RF_CHAIN[rf_chain].rssi_tcomp.coeff_a = conf->rssi_tcomp.coeff_a;
    CONTEXT_RF_CHAIN[rf_chain].rssi_tcomp.coeff_b = conf->rssi_tcomp.coeff_b;
    CONTEXT_RF_CHAIN[rf_chain].rssi_tcomp.coeff_c = conf->rssi_tcomp.coeff_c;
    CONTEXT_RF_CHAIN[rf_chain].rssi_tcomp.coeff_d = conf->rssi_tcomp.coeff_d;
    CONTEXT_RF_CHAIN[rf_chain].rssi_tcomp.coeff_e = conf->rssi_tcomp.coeff_e;
    CONTEXT_RF_CHAIN[rf_chain].type = conf->type;
    CONTEXT_RF_CHAIN[rf_chain].tx_enable = conf->tx_enable;
    CONTEXT_RF_CHAIN[rf_chain].single_input_mode = conf->single_input_mode;

    log_info("rf_chain %d configuration; en:%d freq:%d "
            "rssi_offset:%f radio_type:%d tx_enable:%d "
            "single_input_mode:%d\n",  rf_chain,
            CONTEXT_RF_CHAIN[rf_chain].enable,
            CONTEXT_RF_CHAIN[rf_chain].freq_hz,
            CONTEXT_RF_CHAIN[rf_chain].rssi_offset,
            CONTEXT_RF_CHAIN[rf_chain].type,
            CONTEXT_RF_CHAIN[rf_chain].tx_enable,
            CONTEXT_RF_CHAIN[rf_chain].single_input_mode);
    return 0;
}

int lgw_rxif_setconf(uint8_t if_chain, struct lgw_conf_rxif_s * conf)
{
    int32_t bw_hz;
    uint32_t rf_rx_bandwidth;


    if (if_chain >= LGW_IF_CHAIN_NB) {
        log_error("%d NOT A VALID IF_CHAIN NUMBER\n", if_chain);
        return -1;
    }

    if (conf->enable == false) {
        CONTEXT_IF_CHAIN[if_chain].enable = false;
        CONTEXT_IF_CHAIN[if_chain].freq_hz = 0;
        log_debug("if_chain %d disabled\n", if_chain);
        return 0;
    }

    if (sx1302_get_ifmod_config(if_chain) == IF_UNDEFINED) {
        log_error("IF CHAIN %d NOT CONFIGURABLE\n", if_chain);
    }
    if (conf->rf_chain >= LGW_RF_CHAIN_NB) {
        log_error("INVALID RF_CHAIN TO ASSOCIATE WITH A LORA_STD IF CHAIN\n");
        return -1;
    }

    switch (conf->bandwidth) {
        case BW_250KHZ:
            rf_rx_bandwidth = LGW_RF_RX_BANDWIDTH_250KHZ; /* radio bandwidth */
            break;
        case BW_500KHZ:
            rf_rx_bandwidth = LGW_RF_RX_BANDWIDTH_500KHZ; /* radio bandwidth */
            break;
        default:
            /* For 125KHz and below */
            rf_rx_bandwidth = LGW_RF_RX_BANDWIDTH_125KHZ; /* radio bandwidth */
            break;
    }
    bw_hz = lgw_bw_getval(conf->bandwidth); /* channel bandwidth */
    if ((conf->freq_hz + ((bw_hz==-1)?LGW_REF_BW:bw_hz)/2) > ((int32_t)rf_rx_bandwidth/2)) {
        log_error("IF FREQUENCY %d TOO HIGH\n", conf->freq_hz);
        return -1;
    } else if ((conf->freq_hz - ((bw_hz==-1)?LGW_REF_BW:bw_hz)/2) < -((int32_t)rf_rx_bandwidth/2)) {
        log_error("IF FREQUENCY %d TOO LOW\n", conf->freq_hz);
        return -1;
    }

    /* check parameters according to the type of IF chain + modem,
    fill default if necessary, and commit configuration if everything is OK */
    switch (sx1302_get_ifmod_config(if_chain)) {
        case IF_LORA_STD:
            if (conf->bandwidth == BW_UNDEFINED) {
                conf->bandwidth = BW_250KHZ;
            }
            if (conf->datarate == DR_UNDEFINED) {
                conf->datarate = DR_LORA_SF7;
            }

            if (!IS_LORA_BW(conf->bandwidth)) {
                log_error("BANDWIDTH NOT SUPPORTED BY LORA_STD IF CHAIN\n");
                return -1;
            }
            if (!IS_LORA_DR(conf->datarate)) {
                log_error("DATARATE NOT SUPPORTED BY LORA_STD IF CHAIN\n");
                return -1;
            }

            CONTEXT_IF_CHAIN[if_chain].enable = conf->enable;
            CONTEXT_IF_CHAIN[if_chain].rf_chain = conf->rf_chain;
            CONTEXT_IF_CHAIN[if_chain].freq_hz = conf->freq_hz;
            CONTEXT_LORA_SERVICE.bandwidth = conf->bandwidth;
            CONTEXT_LORA_SERVICE.datarate = conf->datarate;
            CONTEXT_LORA_SERVICE.implicit_hdr = conf->implicit_hdr;
            CONTEXT_LORA_SERVICE.implicit_payload_length = conf->implicit_payload_length;
            CONTEXT_LORA_SERVICE.implicit_crc_en   = conf->implicit_crc_en;
            CONTEXT_LORA_SERVICE.implicit_coderate = conf->implicit_coderate;

            log_debug("LoRa 'std' if_chain %d configuration; "
                    "en:%d freq:%d bw:%d dr:%d\n", if_chain,
                    CONTEXT_IF_CHAIN[if_chain].enable,
                    CONTEXT_IF_CHAIN[if_chain].freq_hz,
                    CONTEXT_LORA_SERVICE.bandwidth,
                    CONTEXT_LORA_SERVICE.datarate);
            break;

        case IF_LORA_MULTI:
            if (conf->bandwidth == BW_UNDEFINED) {
                conf->bandwidth = BW_125KHZ;
            }
            if (conf->datarate == DR_UNDEFINED) {
                conf->datarate = DR_LORA_SF7;
            }

            if (conf->bandwidth != BW_125KHZ) {
                log_error("BANDWIDTH NOT SUPPORTED BY LORA_MULTI IF CHAIN\n");
                return -1;
            }
            if (!IS_LORA_DR(conf->datarate)) {
                log_error("DATARATE(S) NOT SUPPORTED BY LORA_MULTI IF CHAIN\n");
                return -1;
            }

            CONTEXT_IF_CHAIN[if_chain].enable = conf->enable;
            CONTEXT_IF_CHAIN[if_chain].rf_chain = conf->rf_chain;
            CONTEXT_IF_CHAIN[if_chain].freq_hz = conf->freq_hz;

            log_debug("LoRa 'multi' if_chain %d configuration;"
                    " en:%d freq:%d\n", if_chain,
                    CONTEXT_IF_CHAIN[if_chain].enable,
                    CONTEXT_IF_CHAIN[if_chain].freq_hz);
            break;

        case IF_FSK_STD:

            if (conf->bandwidth == BW_UNDEFINED) {
                conf->bandwidth = BW_250KHZ;
            }
            if (conf->datarate == DR_UNDEFINED) {
                conf->datarate = 64000; /* default datarate */
            }

            if(!IS_FSK_BW(conf->bandwidth)) {
                log_error("BANDWIDTH NOT SUPPORTED BY FSK IF CHAIN\n");
                return -1;
            }
            if(!IS_FSK_DR(conf->datarate)) {
                log_error("DATARATE NOT SUPPORTED BY FSK IF CHAIN\n");
                return -1;
            }

            CONTEXT_IF_CHAIN[if_chain].enable = conf->enable;
            CONTEXT_IF_CHAIN[if_chain].rf_chain = conf->rf_chain;
            CONTEXT_IF_CHAIN[if_chain].freq_hz = conf->freq_hz;
            CONTEXT_FSK.bandwidth = conf->bandwidth;
            CONTEXT_FSK.datarate = conf->datarate;
            if (conf->sync_word > 0) {
                CONTEXT_FSK.sync_word_size = conf->sync_word_size;
                CONTEXT_FSK.sync_word = conf->sync_word;
            }
            log_debug("FSK if_chain %d configuration; en:%d"
                    " freq:%d bw:%d dr:%d (%d real dr) sync:0x%0*" PRIu64 "\n",
                    if_chain, CONTEXT_IF_CHAIN[if_chain].enable,
                    CONTEXT_IF_CHAIN[if_chain].freq_hz,
                    CONTEXT_FSK.bandwidth,
                    CONTEXT_FSK.datarate,
                    LGW_XTAL_FREQU/(LGW_XTAL_FREQU/CONTEXT_FSK.datarate),
                    2*CONTEXT_FSK.sync_word_size,
                    CONTEXT_FSK.sync_word);
            break;

        default:
            log_error("IF CHAIN %d TYPE NOT SUPPORTED\n", if_chain);
            return -1;
    }

    return 0;
}

int lgw_demod_setconf(struct lgw_conf_demod_s *conf)
{
    CONTEXT_DEMOD.multisf_datarate = conf->multisf_datarate;
    return 0;
}

int lgw_txgain_setconf(uint8_t rf_chain, struct lgw_tx_gain_lut_s *conf)
{
    int i;

    /* Check LUT size */
    if ((conf->size < 1) || (conf->size > TX_GAIN_LUT_SIZE_MAX)) {
        log_error("TX gain LUT must have at least one entry"
                " and  maximum %d entries\n", TX_GAIN_LUT_SIZE_MAX);
        return -1;
    }

    CONTEXT_TX_GAIN_LUT[rf_chain].size = conf->size;

    for (i = 0; i < CONTEXT_TX_GAIN_LUT[rf_chain].size; i++) {
        /* Check gain range */
        if (conf->lut[i].dig_gain > 3) {
            log_error("TX gain LUT: SX1302 digital gain"
                    " must be between 0 and 3\n");
            return -1;
        }
        if (conf->lut[i].dac_gain > 3) {
            log_error("TX gain LUT: SX1257 DAC gains "
                    "must not exceed 3\n");
            return -1;
        }
        if ((conf->lut[i].mix_gain < 5) || (conf->lut[i].mix_gain > 15)) {
            log_error("TX gain LUT: SX1257 mixer gain must be"
                    " betwen [5..15]\n");
            return -1;
        }
        if (conf->lut[i].pa_gain > 3) {
            log_error("TX gain LUT: External PA gain must"
                    " not exceed 3\n");
            return -1;
        }
        if (conf->lut[i].pwr_idx > 22) {
            log_error("TX gain LUT: SX1250 power index must"
                    " not exceed 22\n");
            return -1;
        }

        /* Set internal LUT */
        CONTEXT_TX_GAIN_LUT[rf_chain].lut[i].rf_power = conf->lut[i].rf_power;
        CONTEXT_TX_GAIN_LUT[rf_chain].lut[i].dig_gain = conf->lut[i].dig_gain;
        CONTEXT_TX_GAIN_LUT[rf_chain].lut[i].pa_gain  = conf->lut[i].pa_gain;
        /* sx125x */
        CONTEXT_TX_GAIN_LUT[rf_chain].lut[i].dac_gain = conf->lut[i].dac_gain;
        CONTEXT_TX_GAIN_LUT[rf_chain].lut[i].mix_gain = conf->lut[i].mix_gain;
        CONTEXT_TX_GAIN_LUT[rf_chain].lut[i].offset_i = 0; /* To be calibrated */
        CONTEXT_TX_GAIN_LUT[rf_chain].lut[i].offset_q = 0; /* To be calibrated */
        /* sx1250 */
        CONTEXT_TX_GAIN_LUT[rf_chain].lut[i].pwr_idx = conf->lut[i].pwr_idx;
    }

    return 0;
}

int lgw_ftime_setconf(struct lgw_conf_ftime_s *conf)
{
    CONTEXT_FINE_TIMESTAMP.enable = conf->enable;
    CONTEXT_FINE_TIMESTAMP.mode = conf->mode;
    return 0;
}

int lgw_sx1261_setconf(struct lgw_conf_sx1261_s *conf)
{
    int i;

    /* Set the SX1261 global conf */
    CONTEXT_SX1261.enable = conf->enable;
    strncpy(CONTEXT_SX1261.spi_path, conf->spi_path, sizeof CONTEXT_SX1261.spi_path);
    CONTEXT_SX1261.spi_path[sizeof CONTEXT_SX1261.spi_path - 1] = '\0';
    CONTEXT_SX1261.rssi_offset = conf->rssi_offset;

    /* Set the LBT conf */
    CONTEXT_SX1261.lbt_conf.enable = conf->lbt_conf.enable;
    CONTEXT_SX1261.lbt_conf.rssi_target = conf->lbt_conf.rssi_target;
    CONTEXT_SX1261.lbt_conf.nb_channel = conf->lbt_conf.nb_channel;
    for (i = 0; i < CONTEXT_SX1261.lbt_conf.nb_channel; i++) {
        if (conf->lbt_conf.channels[i].bandwidth != BW_125KHZ &&
            conf->lbt_conf.channels[i].bandwidth != BW_250KHZ) {
            log_error("bandwidth not supported for LBT channel %d\n", i);
            return -1;
        }
        if (conf->lbt_conf.channels[i].scan_time_us != LGW_LBT_SCAN_TIME_128_US &&
            conf->lbt_conf.channels[i].scan_time_us != LGW_LBT_SCAN_TIME_5000_US) {
            log_error("scan_time_us not supported for LBT channel %d\n", i);
            return -1;
        }
        CONTEXT_SX1261.lbt_conf.channels[i] = conf->lbt_conf.channels[i];
    }

    return 0;
}

int lgw_start()
{
    int i, err;
    uint8_t fw_version_agc;

    err = lgw_connect(CONTEXT_COM_TYPE, CONTEXT_COM_PATH);
    if (err == LGW_REG_ERROR) {
        log_error("FAIL TO CONNECT BOARD\n");
        return -1;
    }

    /* Set all GPIOs to 0 */
    err = sx1302_set_gpio(0x00);
    if (err != LGW_REG_SUCCESS) {
        log_error("failed to set all GPIOs to 0\n");
        return -1;
    }

    /* Calibrate radios */
    err = sx1302_radio_calibrate(&CONTEXT_RF_CHAIN[0],
                                 CONTEXT_BOARD.clksrc,
                                 &CONTEXT_TX_GAIN_LUT[0]);
    if (err != LGW_REG_SUCCESS) {
        log_error("radio calibration failed\n");
        return -1;
    }

    /* Setup radios for RX */
    for (i = 0; i < LGW_RF_CHAIN_NB; i++) {
        if (CONTEXT_RF_CHAIN[i].enable == true) {
            err = sx1302_radio_reset(i, CONTEXT_RF_CHAIN[i].type);
            if (err != LGW_REG_SUCCESS) {
                log_error("failed to reset radio %d\n", i);
                return -1;
            }

            switch (CONTEXT_RF_CHAIN[i].type) {
                case LGW_RADIO_TYPE_SX1250:
                    err = sx1250_setup(i, CONTEXT_RF_CHAIN[i].freq_hz,
                                       CONTEXT_RF_CHAIN[i].single_input_mode);
                    break;
                case LGW_RADIO_TYPE_SX1255:
                case LGW_RADIO_TYPE_SX1257:
                    err = sx125x_setup(i, CONTEXT_BOARD.clksrc,
                                       true, CONTEXT_RF_CHAIN[i].type,
                                       CONTEXT_RF_CHAIN[i].freq_hz);
                    break;
                default:
                    log_error("radio type is not supported"
                            " (RF_CHAIN %d)\n", i);
                    return -1;
            }
            if (err != LGW_REG_SUCCESS) {
                log_error("failed to setup radio %d\n", i);
                return -1;
            }

            /* Set radio mode */
            err = sx1302_radio_set_mode(i, CONTEXT_RF_CHAIN[i].type);
            if (err != LGW_REG_SUCCESS) {
                log_error("failed to set mode for radio %d\n", i);
                return -1;
            }
        }
    }

    /* Select the radio which provides the clock to the sx1302 */
    err = sx1302_radio_clock_select(CONTEXT_BOARD.clksrc);
    if (err != LGW_REG_SUCCESS) {
        log_error("failed to get clock from radio %u\n",
                CONTEXT_BOARD.clksrc);
        return -1;
    }

    /* Release host control on radio (will be controlled by AGC) */
    err = sx1302_radio_host_ctrl(false);
    if (err != LGW_REG_SUCCESS) {
        log_error("failed to release control over radios\n");
        return -1;
    }

    /* Basic initialization of the sx1302 */
    err = sx1302_init(&CONTEXT_FINE_TIMESTAMP);
    if (err != LGW_REG_SUCCESS) {
        log_error("failed to initialize SX1302\n");
        return -1;
    }

    /* Configure PA/LNA LUTs */
    err = sx1302_pa_lna_lut_configure(&CONTEXT_BOARD);
    if (err != LGW_REG_SUCCESS) {
        log_error("failed to configure SX1302 PA/LNA LUT\n");
        return -1;
    }

    /* Configure Radio FE */
    err = sx1302_radio_fe_configure();
    if (err != LGW_REG_SUCCESS) {
        log_error("failed to configure SX1302 radio frontend\n");
        return -1;
    }

    /* Configure the Channelizer */
    err = sx1302_channelizer_configure(CONTEXT_IF_CHAIN, false);
    if (err != LGW_REG_SUCCESS) {
        log_error("failed to configure SX1302 channelizer\n");
        return -1;
    }

    /* configure LoRa 'multi-sf' modems */
    err = sx1302_lora_correlator_configure(CONTEXT_IF_CHAIN, &(CONTEXT_DEMOD));
    if (err != LGW_REG_SUCCESS) {
        log_error("failed to configure SX1302 LoRa modem correlators\n");
        return -1;
    }
    err = sx1302_lora_modem_configure(CONTEXT_RF_CHAIN[0].freq_hz);
    if (err != LGW_REG_SUCCESS) {
        log_error("failed to configure SX1302 LoRa modems\n");
        return -1;
    }

    /* configure LoRa 'single-sf' modem */
    if (CONTEXT_IF_CHAIN[8].enable == true) {
        err = sx1302_lora_service_correlator_configure(&(CONTEXT_LORA_SERVICE));
        if (err != LGW_REG_SUCCESS) {
            log_error("failed to configure SX1302 LoRa Service "
                    "modem correlators\n");
            return -1;
        }
        err = sx1302_lora_service_modem_configure(&(CONTEXT_LORA_SERVICE),
                                                  CONTEXT_RF_CHAIN[0].freq_hz);
        if (err != LGW_REG_SUCCESS) {
            log_error("failed to configure SX1302 LoRa Service modem\n");
            return -1;
        }
    }

    /* configure FSK modem */
    if (CONTEXT_IF_CHAIN[9].enable == true) {
        err = sx1302_fsk_configure(&(CONTEXT_FSK));
        if (err != LGW_REG_SUCCESS) {
            log_error("failed to configure SX1302 FSK modem\n");
            return -1;
        }
    }

    /* configure syncword */
    err = sx1302_lora_syncword(CONTEXT_LWAN_PUBLIC, CONTEXT_LORA_SERVICE.datarate);
    if (err != LGW_REG_SUCCESS) {
        log_error("failed to configure SX1302 LoRa syncword\n");
        return -1;
    }

    /* enable demodulators - to be done before starting AGC/ARB */
    err = sx1302_modem_enable();
    if (err != LGW_REG_SUCCESS) {
        log_error("failed to enable SX1302 modems\n");
        return -1;
    }

    /* Load AGC firmware */
    switch (CONTEXT_RF_CHAIN[CONTEXT_BOARD.clksrc].type) {
        case LGW_RADIO_TYPE_SX1250:
            log_debug("Loading AGC fw for sx1250\n");
            err = sx1302_agc_load_firmware(agc_firmware_sx1250);
            if (err != LGW_REG_SUCCESS) {
                log_error("failed to load AGC firmware for sx1250\n");
                return -1;
            }
            fw_version_agc = FW_VERSION_AGC_SX1250;
            break;
        case LGW_RADIO_TYPE_SX1255:
        case LGW_RADIO_TYPE_SX1257:
            log_debug("Loading AGC fw for sx125x\n");
            err = sx1302_agc_load_firmware(agc_firmware_sx125x);
            if (err != LGW_REG_SUCCESS) {
                log_error("failed to load AGC firmware for sx125x\n");
                return -1;
            }
            fw_version_agc = FW_VERSION_AGC_SX125X;
            break;
        default:
            log_error("failed to load AGC firmware, radio type"
                    " not supported (%d)\n",
                    CONTEXT_RF_CHAIN[CONTEXT_BOARD.clksrc].type);
            return -1;
    }
    err = sx1302_agc_start(fw_version_agc,
                           CONTEXT_RF_CHAIN[CONTEXT_BOARD.clksrc].type,
                           SX1302_AGC_RADIO_GAIN_AUTO,
                           SX1302_AGC_RADIO_GAIN_AUTO,
                           CONTEXT_BOARD.full_duplex,
                           CONTEXT_SX1261.lbt_conf.enable);
    if (err != LGW_REG_SUCCESS) {
        log_error("failed to start AGC firmware\n");
        return -1;
    }

    /* Load ARB firmware */
    log_debug("Loading ARB fw\n");
    err = sx1302_arb_load_firmware(arb_firmware);
    if (err != LGW_REG_SUCCESS) {
        log_error("failed to load ARB firmware\n");
        return -1;
    }
    err = sx1302_arb_start(FW_VERSION_ARB, &CONTEXT_FINE_TIMESTAMP);
    if (err != LGW_REG_SUCCESS) {
        log_error("failed to start ARB firmware\n");
        return -1;
    }

    err = sx1302_tx_configure(CONTEXT_RF_CHAIN[CONTEXT_BOARD.clksrc].type);
    if (err != LGW_REG_SUCCESS) {
        log_error("failed to configure SX1302 TX path\n");
        return -1;
    }

    err = sx1302_gps_enable(true);
    if (err != LGW_REG_SUCCESS) {
        log_error("failed to enable GPS on sx1302\n");
        return -1;
    }

    if (CONTEXT_COM_TYPE == LGW_COM_SPI) {

        if (STTS751_ENABLE) {
            /* Find the temperature sensor on the known supported ports */
            for (i = 0; i < (int)(sizeof I2C_PORT_TEMP_SENSOR); i++) {
                ts_addr = I2C_PORT_TEMP_SENSOR[i];
                err = i2c_linuxdev_open(I2C_DEVICE, ts_addr, &ts_fd);
                if (err != LGW_I2C_SUCCESS) {
                    log_error("failed to open I2C for temperature"
                            " sensor on port 0x%02X\n", ts_addr);
                    return -1;
                }

                err = stts751_configure(ts_fd, ts_addr);
                if (err != LGW_I2C_SUCCESS) {
                    log_info("no temperature sensor found on"
                            " port 0x%02X\n", ts_addr);
                    i2c_linuxdev_close(ts_fd);
                    ts_fd = -1;
                } else {
                    log_info("found temperature sensor on"
                            " port 0x%02X\n", ts_addr);
                    break;
                }
            }
            if (i == sizeof I2C_PORT_TEMP_SENSOR) {
                log_error("no temperature sensor found.\n");
                return -1;
            }
        }

        /* Configure ADC AD338R for full duplex (CN490 reference design) */
        if (CONTEXT_BOARD.full_duplex == true) {
            err = i2c_linuxdev_open(I2C_DEVICE, I2C_PORT_DAC_AD5338R, &ad_fd);
            if (err != LGW_I2C_SUCCESS) {
                log_error("failed to open I2C for ad5338r\n");
                return -1;
            }

            err = ad5338r_configure(ad_fd, I2C_PORT_DAC_AD5338R);
            if (err != LGW_I2C_SUCCESS) {
                log_error("failed to configure ad5338r\n");
                i2c_linuxdev_close(ad_fd);
                ad_fd = -1;
                return -1;
            }

            /* Turn off the PA: set DAC output to 0V */
            uint8_t volt_val[AD5338R_CMD_SIZE] = {
                0x39,
                (uint8_t)VOLTAGE2HEX_H(0),
                (uint8_t)VOLTAGE2HEX_L(0)
            };
            err = ad5338r_write(ad_fd, I2C_PORT_DAC_AD5338R, volt_val);
            if (err != LGW_I2C_SUCCESS) {
                log_error("AD5338R: failed to set DAC output to 0V\n");
                return -1;
            }
            log_info("AD5338R: Set DAC output to 0x%02X 0x%02X\n",
                    (uint8_t)VOLTAGE2HEX_H(0), (uint8_t)VOLTAGE2HEX_L(0));
        }
    }

    /* Connect to the external sx1261 for LBT or Spectral Scan */
    if (CONTEXT_SX1261.enable == true) {
        char *device = NULL;

        if (CONTEXT_COM_TYPE == LGW_COM_SPI)
            device = CONTEXT_SX1261.spi_path;

        err = sx1261_connect(CONTEXT_COM_TYPE, device);
        if (err != LGW_REG_SUCCESS) {
            log_error("failed to connect to the sx1261 "
                    "radio (LBT/Spectral Scan)\n");
            return -1;
        }

        err = sx1261_load_pram();
        if (err != LGW_REG_SUCCESS) {
            log_error("failed to patch sx1261 radio for"
                    " LBT/Spectral Scan\n");
            return -1;
        }

        err = sx1261_calibrate(CONTEXT_RF_CHAIN[0].freq_hz);
        if (err != LGW_REG_SUCCESS) {
            log_error("failed to calibrate sx1261 radio\n");
            return -1;
        }

        err = sx1261_setup();
        if (err != LGW_REG_SUCCESS) {
            log_error("failed to setup sx1261 radio\n");
            return -1;
        }
    }

    /* Set CONFIG_DONE GPIO to 1 (turn on the corresponding LED) */
    err = sx1302_set_gpio(0x01);
    if (err != LGW_REG_SUCCESS) {
        log_error("failed to set CONFIG_DONE GPIO\n");
        return -1;
    }

    return 0;
}

/* Close the LoRa concentrator. */
int lora_close()
{
    int i, x, err = 0;

    /* Abort current TX if needed */
    for (i = 0; i < LGW_RF_CHAIN_NB; i++) {
        log_info("aborting TX on chain %u\n", i);
        x = lgw_abort_tx(i);
        if (x != 0) {
            log_warning("failed to get abort TX on chain %u\n", i);
            err = -1;
        }
    }

    x = lgw_disconnect();
    if (x != 0) {
        log_error("failed to disconnect concentrator\n");
        err = -1;
    }

    if (CONTEXT_COM_TYPE == LGW_COM_SPI) {
        if (STTS751_ENABLE) {
            log_info("closing I2C for temperature sensor\n");
            x = i2c_linuxdev_close(ts_fd);
            if (x != 0) {
                log_error("failed to close I2C temperature "
                        "sensor device (err=%i)\n", x);
                err = -1;
            }
        }
        if (CONTEXT_BOARD.full_duplex == true) {
            log_info("closing I2C for AD5338R\n");
            x = i2c_linuxdev_close(ad_fd);
            if (x != 0) {
                log_error("failed to close I2C AD5338R "
                        "device (err=%i)\n", x);
                err = -1;
            }
        }
    }

    return err;
}

/* Read packets from the LoRa concentrator.
   On success, the number of packets read is returned or
   zero if there are no packets available in the concentrator.
   On error, -1 is returned. */
int lora_read(struct lora_packet_t *packets, size_t size)
{
    uint8_t i, npacket = 0, nleft = 0;
    float temp = 0;

    if (sx1302_fetch(&npacket) == -1) {
        log_warning("failed to fetch packets from SX1302.\n");
        return -1;
    }

    /* Update internal counter */
    /* WARNING: this needs to be called regularly by the upper layer */
    if (sx1302_update() != LGW_REG_SUCCESS)
        return -1;

    if (npacket == 0)
        return 0;

    if (npacket > size) {
        nleft = npacket - size;
        log_warning("not enough space allocated, fetched %d packet(s)"
                ", %d will be left in the data buffer.\n", npacket, nleft);
        npacket = size;
    }

    /* Apply RSSI temperature compensation */
    if (lgw_get_temperature(&temp) != LGW_I2C_SUCCESS) {
        log_warning("failed to get current temperature.\n");
        return -1;
    }

    for (i = 0; i < npacket; i++) {
        struct lora_packet_t *lp = &packets[i];
        float offset = 0;
        uint8_t c;

        if (sx1302_parse(&lgw_context, lp) == -1) {
            log_warning("error while parsing incoming"
                        " packet (%d).\n", i);
            return -1;
        }

        /* Apply RSSI offset calibrated for the board */

        c = lp->radio;

        lp->rssic += CONTEXT_RF_CHAIN[c].rssi_offset;
        lp->rssis += CONTEXT_RF_CHAIN[c].rssi_offset;

        offset = \
            sx1302_rssi_get_temperature_offset(&CONTEXT_RF_CHAIN[c].rssi_tcomp,
                                               temp);
        lp->rssic += offset;
        lp->rssis += offset;
        log_info("RSSI temperature offset applied: %.3f dB"
            " (current temperature %.1f C)\n", offset, temp);
    }

    log_info("%d packets were fetched, %d left.\n", npacket, nleft);

    /* Remove duplicated packets generated by double
       demod when precision timestamp is enabled */
    if (CONTEXT_FINE_TIMESTAMP.enable == true) {
        if (merge_packets(packets, &npacket) != 0) {
            log_warning("failed to remove duplicated packets.\n");
        }
        log_info("%d packets after de-duplicating.\n", npacket);
    }

    return npacket;
}


int lgw_send(struct lgw_pkt_tx_s *pkt_data)
{
    int err;
    bool lbt_tx_allowed;
    /* performances variables */
    struct timeval tm;

    /* Record function start time */
    _meas_time_start(&tm);

    if (pkt_data->rf_chain >= LGW_RF_CHAIN_NB) {
        log_error("INVALID RF_CHAIN TO SEND PACKETS\n");
        return -1;
    }

    if (CONTEXT_RF_CHAIN[pkt_data->rf_chain].tx_enable == false) {
        log_error("SELECTED RF_CHAIN IS DISABLED "
                "FOR TX ON SELECTED BOARD\n");
        return -1;
    }
    if (CONTEXT_RF_CHAIN[pkt_data->rf_chain].enable == false) {
        log_error("SELECTED RF_CHAIN IS DISABLED\n");
        return -1;
    }
    if (!IS_TX_MODE(pkt_data->tx_mode)) {
        log_error("TX_MODE NOT SUPPORTED\n");
        return -1;
    }
    if (pkt_data->modulation == MOD_LORA) {
        if (!IS_LORA_BW(pkt_data->bandwidth)) {
            log_error("BANDWIDTH NOT SUPPORTED BY LORA TX\n");
            return -1;
        }
        if (!IS_LORA_DR(pkt_data->datarate)) {
            log_error("DATARATE NOT SUPPORTED BY LORA TX\n");
            return -1;
        }
        if (!IS_LORA_CR(pkt_data->coderate)) {
            log_error("CODERATE NOT SUPPORTED BY LORA TX\n");
            return -1;
        }
        if (pkt_data->size > 255) {
            log_error("PAYLOAD LENGTH TOO BIG FOR LORA TX\n");
            return -1;
        }
    } else if (pkt_data->modulation == MOD_FSK) {
        if((pkt_data->f_dev < 1) || (pkt_data->f_dev > 200)) {
            log_error("TX FREQUENCY DEVIATION OUT OF ACCEPTABLE RANGE\n");
            return -1;
        }
        if(!IS_FSK_DR(pkt_data->datarate)) {
            log_error("DATARATE NOT SUPPORTED BY FSK IF CHAIN\n");
            return -1;
        }
        if (pkt_data->size > 255) {
            log_error("PAYLOAD LENGTH TOO BIG FOR FSK TX\n");
            return -1;
        }
    } else if (pkt_data->modulation == MOD_CW) {
        /* do nothing */
    } else {
        log_error("INVALID TX MODULATION\n");
        return -1;
    }

    /* Set PA gain with AD5338R when using full duplex CN490 ref design */
    if (CONTEXT_BOARD.full_duplex == true) {
        uint8_t volt_val[AD5338R_CMD_SIZE] = {
            0x39,
            VOLTAGE2HEX_H(2.51),
            VOLTAGE2HEX_L(2.51) /* set to 2.51V */
        };

        err = ad5338r_write(ad_fd, I2C_PORT_DAC_AD5338R, volt_val);
        if (err != LGW_I2C_SUCCESS) {
            log_error("failed to set voltage by ad5338r\n");
            return -1;
        }
        log_info("AD5338R: Set DAC output to 0x%02X 0x%02X\n",
                (uint8_t)VOLTAGE2HEX_H(2.51), (uint8_t)VOLTAGE2HEX_L(2.51));
    }

    /* Start Listen-Before-Talk */
    if (CONTEXT_SX1261.lbt_conf.enable == true) {
        err = lgw_lbt_start(&CONTEXT_SX1261, pkt_data);
        if (err != 0) {
            log_error("failed to start LBT\n");
            return -1;
        }
    }

    /* Send the TX request to the concentrator */
    err = sx1302_send(CONTEXT_RF_CHAIN[pkt_data->rf_chain].type,
                      &CONTEXT_TX_GAIN_LUT[pkt_data->rf_chain],
                      CONTEXT_LWAN_PUBLIC, &CONTEXT_FSK, pkt_data);
    if (err != LGW_REG_SUCCESS) {
        log_error("failed to send packet\n");

        if (CONTEXT_SX1261.lbt_conf.enable == true) {
            err = lgw_lbt_stop();
            if (err != 0) {
                log_error("failed to stop LBT\n");
            }
        }

        return -1;
    }

    _meas_time_stop(1, tm, __FUNCTION__);

    /* Stop Listen-Before-Talk */
    if (CONTEXT_SX1261.lbt_conf.enable == true) {
        err = lgw_lbt_tx_status(pkt_data->rf_chain, &lbt_tx_allowed);
        if (err != 0) {
            log_error("failed to get LBT TX status, TX aborted\n");

            err = sx1302_tx_abort(pkt_data->rf_chain);
            if (err != 0) {
                log_error("failed to abort TX\n");
            }
            err = lgw_lbt_stop();
            if (err != 0) {
                log_error("failed to stop LBT\n");
            }
            return -1;
        }
        if (lbt_tx_allowed == true) {
            log_info("LBT: packet is allowed to be transmitted\n");
        } else {
            log_info("LBT: (ERROR) packet is NOT allowed"
                    " to be transmitted\n");
        }

        err = lgw_lbt_stop();
        if (err != 0) {
            log_error("failed to stop LBT\n");
            return -1;
        }
    }

    if (CONTEXT_SX1261.lbt_conf.enable == true &&
        lbt_tx_allowed == false) {
        return LGW_LBT_NOT_ALLOWED;
    }
    return 0;
}

int lgw_status(uint8_t rf_chain, uint8_t select, uint8_t *code)
{
    if (rf_chain >= LGW_RF_CHAIN_NB) {
        log_error("NOT A VALID RF_CHAIN NUMBER\n");
        return -1;
    }

    if (select == TX_STATUS) {
        *code = sx1302_tx_status(rf_chain);
    } else if (select == RX_STATUS) {
        *code = sx1302_rx_status(rf_chain);
    } else {
        log_error("SELECTION INVALID, NO STATUS TO RETURN\n");
        return -1;
    }

    return 0;
}

int lgw_abort_tx(uint8_t rf_chain)
{
    if (rf_chain >= LGW_RF_CHAIN_NB) {
        log_error("NOT A VALID RF_CHAIN NUMBER\n");
        return -1;
    }

    return sx1302_tx_abort(rf_chain);
}

int lgw_get_trigcnt(uint32_t *trig_cnt_us)
{
    assert(trig_cnt_us != NULL);
    *trig_cnt_us = sx1302_timestamp_counter(true);
    return 0;
}

int lgw_get_instcnt(uint32_t *inst_cnt_us)
{
    assert(inst_cnt_us != NULL);
    *inst_cnt_us = sx1302_timestamp_counter(false);
    return 0;
}

int lgw_get_eui(uint64_t *eui)
{
    assert(eui != NULL);
    if (sx1302_get_eui(eui) != LGW_REG_SUCCESS) {
        return -1;
    }
    return 0;
}

int lgw_get_temperature(float *tp)
{
    int err = -1;

    assert(tp != NULL);

    *tp = 0;
    switch (CONTEXT_COM_TYPE) {
        case LGW_COM_SPI:
            if (STTS751_ENABLE)
                err = stts751_get_temperature(ts_fd, ts_addr, tp);
            else
                err = LGW_I2C_SUCCESS;
            break;
        case LGW_COM_USB:
            err = lgw_com_get_temperature(tp);
            break;
        default:
            log_error("wrong communication type (SHOULD NOT HAPPEN)\n");
    }
    return err;
}

const char* lgw_version_info()
{
    return lgw_version_string;
}

uint32_t lgw_time_on_air(const struct lgw_pkt_tx_s *packet)
{
    double t_fsk;
    uint32_t toa_ms, toa_us;

    if (packet == NULL) {
        log_error("Failed to compute time on air,"
                " wrong parameter\n");
        return 0;
    }

    if (packet->modulation == MOD_LORA) {
        toa_us = lora_packet_time_on_air(packet->bandwidth,
                                         packet->datarate,
                                         packet->coderate,
                                         packet->preamble,
                                         packet->no_header,
                                         packet->no_crc,
                                         packet->size,
                                         NULL, NULL, NULL);
        toa_ms = (uint32_t)( (double)toa_us / 1000.0 + 0.5 );
        log_info("LoRa packet ToA: %u ms\n", toa_ms);
    } else if (packet->modulation == MOD_FSK) {
        /* PREAMBLE + SYNC_WORD + PKT_LEN + PKT_PAYLOAD + CRC
                PREAMBLE: default 5 bytes
                SYNC_WORD: default 3 bytes
                PKT_LEN: 1 byte (variable length mode)
                PKT_PAYLOAD: x bytes
                CRC: 0 or 2 bytes
        */
        t_fsk = (8 * (double)(packet->preamble + CONTEXT_FSK.sync_word_size + 1 + packet->size + ((packet->no_crc == true) ? 0 : 2)) / (double)packet->datarate) * 1E3;

        /* Duration of packet */
        toa_ms = (uint32_t)t_fsk + 1; /* add margin for rounding */
    } else {
        toa_ms = 0;
        log_error("Cannot compute time on air for this packet,"
                " unsupported modulation (0x%02X)\n", packet->modulation);
    }

    return toa_ms;
}

int lgw_spectral_scan_start(uint32_t freq_hz, uint16_t nb_scan)
{
    int err;

    if (CONTEXT_SX1261.enable != true) {
        log_error("sx1261 is not enabled, no spectral scan\n");
        return -1;
    }

    err = sx1261_set_rx_params(freq_hz, BW_125KHZ);
    if (err != LGW_REG_SUCCESS) {
        log_error("Failed to set RX params for Spectral Scan\n");
        return -1;
    }

    err = sx1261_spectral_scan_start(nb_scan);
    if (err != LGW_REG_SUCCESS) {
        log_error("start spectral scan failed\n");
        return -1;
    }

    return 0;
}

int lgw_spectral_scan_get_status(lgw_spectral_scan_status_t * status)
{
    return sx1261_spectral_scan_status(status);
}

int lgw_spectral_scan_get_results(int16_t levels_dbm[static LGW_SPECTRAL_SCAN_RESULT_SIZE],
                                  uint16_t results[static LGW_SPECTRAL_SCAN_RESULT_SIZE])
{
    return sx1261_spectral_scan_get_results(CONTEXT_SX1261.rssi_offset,
                                            levels_dbm, results);
}

int lgw_spectral_scan_abort()
{
    return sx1261_spectral_scan_abort();
}
