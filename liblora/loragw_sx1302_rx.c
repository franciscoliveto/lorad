/*
  / _____)             _              | |
  ( (____  _____ ____ _| |_ _____  ____| |__
  \____ \| ___ |    (_   _) ___ |/ ___)  _ \
  _____) ) ____| | | || |_| ____( (___| | | |
  (______/|_____)_|_|_| \__)_____)\____)_| |_|
  (C)2019 Semtech

  Description:
  SX1302 RX buffer Hardware Abstraction Layer

  License: Revised BSD License, see LICENSE.TXT file include in the project
*/
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "loragw_aux.h"
#include "loragw_reg.h"
#include "loragw_sx1302_rx.h"
#include "loragw_sx1302_timestamp.h"

#include "sx1302_hal.h"
#include "log.h"
#include "os.h"


enum {
    LORA_MODEM_ID = 16,
    FSK_MODEM_ID = 17
};

enum {
    SYNCWORD_LSB = 0xA5,
    SYNCWORD_MSB = 0xC0,
    METADATASIZ = 23 /* 9 header, 13 footer, 1 checksum */
};



int rx_buffer_del(rx_buffer_t * self)
{
    /* Reset index & size */
    self->buffer_size = 0;
    self->buffer_index = 0;
    self->buffer_pkt_nb = 0;

    return LGW_REG_SUCCESS;
}

/* Fetch a stream of bytes from SX1302's FIFO data buffer
   and save them to the memory buffer structure pointed to
   by 'self'. Compute the number of packets that the stream
   of bytes represent and update the counter variable in 'self'.
   It returns 0 on success, otherwise -1 is returned. */
int rx_buffer_fetch(rx_buffer_t * self)
{
    int res;
    uint8_t buf[2];
    uint8_t payload_len;
    uint16_t metrics_len, next_pkt_idx;
    int idx;
    uint16_t size0, size1;

    /* Check if there is data in the FIFO */

    /* reg address + 0 = MSB (Most Significant Byte)
       reg address + 1 = LSB (Least Significant Byte)
    */
    lgw_reg_rb(SX1302_REG_RX_TOP_RX_BUFFER_NB_BYTES_MSB_RX_BUFFER_NB_BYTES,
               buf, sizeof(buf));
    size0 = buf[0] << 8 | buf[1];

    /* FIXME: why is this being done? */

    /* Workaround for multi-byte read issue: read again and
       ensure new read is not lower than the previous one */
    lgw_reg_rb(SX1302_REG_RX_TOP_RX_BUFFER_NB_BYTES_MSB_RX_BUFFER_NB_BYTES,
               buf, sizeof(buf));
    size1 = buf[0] << 8 | buf[1];


    self->buffer_size = (size1 > size0) ? size1 : size0;

    /* Fetch bytes from FIFO if any */
    if (self->buffer_size > 0) {
        log_debug("data size: %d\n", self->buffer_size);

        memset(self->buffer, 0, sizeof(self->buffer));
        res = lgw_mem_rb(0x4000, self->buffer, self->buffer_size, true);
        if (res != LGW_REG_SUCCESS) {
            log_error("failed to read RX buffer, SPI error\n");
            return LGW_REG_ERROR;
        }

        log_hex(LOG_DEBUG, "raw data: ", self->buffer,
                    self->buffer_size);

        /* Sanity check: is there at least 1 complete packet in the buffer */
        if (self->buffer_size < METADATASIZ) {
            log_warning("not enough data to have a complete"
                " packet, discard data buffer.\n");
            return rx_buffer_del(self);
        }

        /* Sanity check: is there a syncword at 0 ? 
           If not, move to the first syncword found */
        idx = 0;
        while (idx <= (self->buffer_size - 2)) {
            if ((self->buffer[idx] == SYNCWORD_LSB) &&
                (self->buffer[idx + 1] == SYNCWORD_MSB)) {
                log_debug("syncword found at idx %d\n", idx);
                break;
            } else {
                log_debug("syncword not found at idx %d\n", idx);
                idx += 1;
            }
        }
        if (idx > self->buffer_size - 2) {
            log_warning("no syncword found, discard rx_buffer\n");
            return rx_buffer_del(self);
        }
        if (idx != 0) {
            log_info("re-sync rx_buffer at idx %d\n", idx);
            memmove((void *)(self->buffer), (void *)(self->buffer + idx),
                    self->buffer_size - idx);
            self->buffer_size -= idx;
        }

        /* Rewind and parse buffer to get the number of packet fetched */
        idx = 0;
        while (idx < self->buffer_size) {
            if ((self->buffer[idx] != SYNCWORD_LSB) ||
                (self->buffer[idx + 1] != SYNCWORD_MSB)) {
                log_warning("syncword not found at idx %d, "
                    "discard the rx_buffer\n", idx);
                return rx_buffer_del(self);
            }
            /* One packet found in the buffer */
            self->buffer_pkt_nb += 1;

            /* Compute the number of bytes for this packet */
            payload_len = self->buffer[idx + 2];
            metrics_len = self->buffer[idx + 9 + payload_len + 12] * 2;

            next_pkt_idx =  METADATASIZ + payload_len + metrics_len;

            /* Move to next packet */
            idx += (int)next_pkt_idx;
        }
    }

    /* Initialize the current buffer index to iterate on */
    self->buffer_index = 0;

    return LGW_REG_SUCCESS;
}

/* Return the computed checksum. */
static uint8_t sx1302_checksum(uint8_t *buf, size_t len)
{
    size_t i;
    uint8_t sum = 0;

    for (i = 0; i < len; i++)
        sum += buf[i];
    return sum;
}

static const char *flagsdump(uint8_t flags)
{
    static char buf[64];
    struct {
        uint8_t mask;
        const char *name;
    } *p, names[] = {
        {FLAG_CRC_ENABLE, "CRC_ENABLE"},
        {FLAG_CRC_ERROR, "CRC_ERROR"},
        {FLAG_SYNC_ERROR, "SYNC_ERROR"},
        {FLAG_HEADER_ERROR, "HEADER_ERROR"},
        {FLAG_TIMING, "TIMING"},
        {0, NULL}
    };

    memset(buf, 0, sizeof(buf));
    buf[0] = '(';
    for (p = names; p->name != NULL; p++)
        if (flags & p->mask) {
            strlcat(buf, p->name, sizeof(buf));
            strlcat(buf, "|", sizeof(buf));
        }
    if (buf[1] != '\0')
        buf[strlen(buf)-1] = '\0';
    strlcat(buf, ")", sizeof(buf));
    return buf;
}

int rx_buffer_pop(rx_buffer_t *self, sx1302_packet_t *sp)
{
    uint8_t checksum, cs;
    int pos;
    uint8_t *buf;
    size_t metrics_len, size;

    pos = self->buffer_index;
    buf = self->buffer;

    if (pos >= self->buffer_size) {
        log_debug("no more data to be parsed.\n");
        return -1;
    }

    /* SX1302 packet engine FIFO's data layout. This information was re-created
       from the original packet's parser code.

       packet example:
       A5 C0 1F 02 83 08 40 41 0F 80 07 C0 C7 00 80 02 00 01 35 AB 40 5E 91 25 59
       75 1A 5D 26 1C 14 58 54 A8 8E 54 F8 E5 7E 4D 00 2B 8E 8D 01 10 44 F3 1F 85
       67 FB 00 73

       syncword:         A5C0
       payload_length:   1F       (31)
       channel:          02       (2)
       CRC_enable-rates: 83       (dr == 8, cr == 1, crc_enable == 1)
       modem_id:         08       (8)
       frequency_offset: 40410F   (0F4140 == 999744)
       payload:          8007C0C7008002000135AB405E912559751A5D261C145854A88E54F8E57E4D
       errors/timing:    00       (crc_err == 0, sync_err == 0, header_err == 0, timing == 0)
       SNR:              2B       (43)
       rssi_channel:     8E       (142)
       rssi_signal:      8D       (141)
       rssi_channel_max: 01       (pos == 0, neg == 1)
       rssi_signal_max:  10       (pos == 1, neg == 0)
       timestamp:        44F31F85 (851FF344 == 2233463620)
       payload_CRC16:    67FB     (FB67 == 64359)
       TS_metrics_count: 00
       checksum:         73

       Packet Format:

       POS                      FIELD              DESCRIPTION
       ---                      -----              -----------
       [0]                      syncword           0xA5 (first byte, LSB?)
       [1]                      syncword           0xC0 (second byte, MSB?)
       [2]                      payload length     1 byte
       [3]                      channel            1 byte
       [4]                      CRC enable/rates   [7-4] data rate
                                                   [3-1] coding rate
                                                   [0] CRC enable
       [5]                      modem ID           1 byte
       [6 to 8]                 frequency offset   3 bytes, LSB first.
       [9 to 9+N-1]             payload            packet's payload. N is the payload's length.
       [9+N]                    errors/timing      bits:
                                                        [7-5] unused
                                                        [4] timing set
                                                        [3] header error
                                                        [2] sync error
                                                        [1] unused
                                                        [0] CRC error
       [9+N+1]                  SNR                1 byte
       [9+N+2]                  RSSI channel       1 byte
       [9+N+3]                  RSSI signal        1 byte
       [9+N+4]                  RSSI channel max.  [7-4] pos delta
                                                   [3-0] neg delta
       [9+N+5]                  RSSI signal max.   [7-4] pos delta
                                                   [3-0] neg delta
       [9+N+6 to 9+N+9]         timestamp          4 bytes, LSB first.
       [9+N+10 to 9+N+11]       CRC16              payload's CRC. 2 bytes, LSB first.

       [9+N+12]                 TS metrics count   1 byte

       [9+N+13 to (9+N+13+M)-1] TS metrics         M = (TS metrics count) * 2 (in bytes)

       [9+N+13+M]               checksum           1 byte
    */

    if ((buf[pos] != SYNCWORD_LSB) ||
        (buf[pos+1] != SYNCWORD_MSB)) {
        log_debug("invalid syncword (0x%02X%02X). Ignore "
                  "packet.\n", buf[pos], buf[pos+1]);
        rx_buffer_del(self); /* FIXME: remove when appropriate */
        return -1;
    }

    log_debug("syncword: 0x%02X%02X\n", SYNCWORD_LSB, SYNCWORD_MSB);

    sp->payload.len = buf[pos+2];
    uint16_t n = pos + 9 + sp->payload.len; /* metadata footer index 0 */
    sp->nmetrics = buf[n+12];
    metrics_len = sp->nmetrics * 2; /* FIXME: why multiply by 2 */

    /* size of the whole packet (in bytes) */
    size = METADATASIZ + sp->payload.len + metrics_len;

    if ((pos + size) > self->buffer_size) {
        log_debug("aborting truncated message, "
                  "size (%u)\n", self->buffer_size);
        rx_buffer_del(self); /* FIXME: remove when appropriate */
        return -1;
    }

    checksum = buf[pos+size-1];
    cs = sx1302_checksum(buf, size-1);
    if (checksum != cs) {
        log_debug("invalid checksum (got:0x%02X "
                  "calc:0x%02X). Packet discarded.\n", checksum, cs);
        rx_buffer_del(self); /* FIXME: remove when appropriate */
        return -1;
    }

    sp->chan = buf[pos+3];                /* channel */
    sp->dr = buf[pos+4] >> 4;             /* data rate */
    sp->cr = (buf[pos+4] & 0x0F) >> 1;    /* coding rate */
    sp->flags = (buf[pos+4] & 0x01) << 1; /* CRC enable */
    sp->mid = buf[pos+5];                 /* modem ID */

    sp->freq_offset = buf[pos+8] << 16;   /* frequency offset */
    sp->freq_offset |= buf[pos+7] << 8;
    sp->freq_offset |= buf[pos+6];

    /* FIXME: what is this? what does it do? We don't have the SX1302
       chip's spec so we can't be sure what's really going on here. But
       this was being done; it's safe to assume that it might be important. */
    if (sp->freq_offset >= (1<<19)) { /* Handle signed value on 20bits */
        sp->freq_offset = sp->freq_offset - (1<<20);
    }

    memcpy(sp->payload.data, &buf[pos+9], sp->payload.len);

    sp->flags |= buf[n] & 0x1D; /* errors/timing flags */
    sp->snr = buf[n+1];         /* SNR */
    sp->rssic = buf[n+2];       /* RSSI channel */
    sp->rssis = buf[n+3];       /* RSSI signal */
    sp->rssic_max = buf[n+4];   /* RSSI channel max. */
    sp->rssis_max = buf[n+5];   /* RSSI signal max. */

    /* packet timestamp (32 MHz) */
    sp->timestamp = buf[n+9] << 24;
    sp->timestamp |= buf[n+8] << 16;
    sp->timestamp |= buf[n+7] << 8;
    sp->timestamp |= buf[n+6];

    sp->payload.crc16 = buf[n+11] << 8;
    sp->payload.crc16 |= buf[n+10];

    /* TS metrics: it is expected the nb_symbols
       parameter is set to 0 here, so stddev must be 0. */
    if (metrics_len > sizeof(sp->tsmetrics))
        metrics_len = sizeof(sp->tsmetrics);

    memcpy(sp->tsmetrics, &buf[n+13], metrics_len);
    memset(sp->tsmetrics_stddev, 0, sizeof(sp->tsmetrics_stddev));

    log_debug("    payload size: %u\n", sp->payload.len);
    log_debug("         channel: %u\n", sp->chan);
    log_debug("       data rate: %u\n", sp->dr);
    log_debug("     coding rate: %u\n", sp->cr);
    log_debug("           flags: %s\n", flagsdump(sp->flags));
    log_debug("           modem: %u\n", sp->mid);
    log_debug("frequency offset: %u\n", sp->freq_offset);
    log_hex(LOG_DEBUG, "         payload: ",
                sp->payload.data, sp->payload.len);
    log_debug("             snr: %d\n", sp->snr);
    log_debug("    rssi channel: %u\n", sp->rssic);
    log_debug("     rssi signal: %u\n", sp->rssis);
    log_debug("rssi channel max: %u\n", sp->rssic_max);
    log_debug(" rssi signal max: %u\n", sp->rssis_max);
    log_debug("       timestamp: %u\n", sp->timestamp);
    log_debug("           crc16: %u (0x%04X)\n",
            sp->payload.crc16, sp->payload.crc16);
    log_debug("      ts metrics: %u\n", sp->nmetrics);

    if (sp->nmetrics > 0) {
        log_hex(LOG_DEBUG, "metrics: ",
                    (uint8_t *) sp->tsmetrics, metrics_len);
        log_debug("metrics_stddev: NONE (nb_symbols=0)\n");
    }
    log_debug("        checksum: 0x%02X\n", checksum);

    /* FIXME: do this have to be here? */

    /* Sanity checks: check the range of few metadata */
    if (sp->mid > FSK_MODEM_ID) {
        log_debug("modem ID (%d) is out of range.\n", sp->mid);
        return -1;
    }

    if (sp->mid <= LORA_MODEM_ID) { /* LoRa modems */
        if (sp->chan > 9) { /* FIXME: what does 9 mean? */
            log_debug("channel (%d) is out of range.\n", sp->chan);
            return -1;
        }
        if ((sp->dr < 5) || (sp->dr > 12)) { /* FIXME: 5 and 12? */
            log_debug("SF (%d) is out of range.\n", sp->dr);
            return -1;
        }
    } else { /* FSK modem */
        /* TODO: must be validated. */
    }

    /* point to the next packet */
    self->buffer_index += size;

    /* Update the number of packets currently stored in the rx_buffer */
    self->buffer_pkt_nb -= 1;

    return 0;
}

uint16_t rx_buffer_read_ptr_addr(void)
{
    int32_t val;
    uint16_t addr;

    /* mandatory to read MSB first */
    lgw_reg_r(SX1302_REG_RX_TOP_RX_BUFFER_LAST_ADDR_READ_MSB_LAST_ADDR_READ, &val); 
    addr  = (uint16_t)(val << 8);
    lgw_reg_r(SX1302_REG_RX_TOP_RX_BUFFER_LAST_ADDR_READ_LSB_LAST_ADDR_READ, &val);
    addr |= (uint16_t)val;

    return addr;
}


uint16_t rx_buffer_write_ptr_addr(void)
{
    int32_t val;
    uint16_t addr;

    /* mandatory to read MSB first */
    lgw_reg_r(SX1302_REG_RX_TOP_RX_BUFFER_LAST_ADDR_WRITE_MSB_LAST_ADDR_WRITE, &val);
    addr  = (uint16_t)(val << 8);
    lgw_reg_r(SX1302_REG_RX_TOP_RX_BUFFER_LAST_ADDR_WRITE_LSB_LAST_ADDR_WRITE, &val);
    addr |= (uint16_t)val;

    return addr;
}

#if notdef
void rx_buffer_dump(FILE * file, uint16_t start_addr, uint16_t end_addr)
{
    int i;
    uint8_t rx_buffer_debug[4096];

    printf("Dumping %u bytes, from 0x%X to 0x%X\n", end_addr - start_addr + 1, start_addr, end_addr);

    memset(rx_buffer_debug, 0, sizeof rx_buffer_debug);

    lgw_reg_w(SX1302_REG_RX_TOP_RX_BUFFER_DIRECT_RAM_IF, 1);
    lgw_mem_rb(0x4000 + start_addr, rx_buffer_debug, end_addr - start_addr + 1, false);
    lgw_reg_w(SX1302_REG_RX_TOP_RX_BUFFER_DIRECT_RAM_IF, 0);

    for (i = 0; i < (end_addr - start_addr + 1); i++) {
        if (file == NULL) {
            printf("%02X ", rx_buffer_debug[i]);
        } else {
            fprintf(file, "%02X ", rx_buffer_debug[i]);
        }
    }
    if (file == NULL) {
        printf("\n");
    } else {
        fprintf(file, "\n");
    }

    /* Switching to direct-access memory could lead to corruption, so to be done only for debugging */
    assert(0);
}
#endif
