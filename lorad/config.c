#include "config.h"
#include "toml.h"

struct config config = {0};

const struct toml_key_t gps_keys[] = {
    {"device", string_t, .addr.string = config.gps.device,
     .len = sizeof(config.gps.device)},
    {"latitude", real_t, .addr.real = &config.gps.lat},
    {"longitude", real_t, .addr.real = &config.gps.lon},
    {"altitude", real_t, .addr.real = &config.gps.alt}
};

const struct toml_key_t beacon_keys[] = {
    {"period", integer_t, .addr.integer = &config.beacon.period},
    {"freq_nb", integer_t, .addr.integer = &config.beacon.freq.nb},
    {"freq_hz", integer_t, .addr.integer = &config.beacon.freq.hz},
    {"freq_step", integer_t, .addr.integer = &config.beacon.freq.step},
    {"datarate", integer_t, .addr.integer = &config.beacon.dr},
    {"bw_hz", integer_t, .addr.integer = &config.beacon.bw},
    {"power", integer_t, .addr.integer = &config.beacon.power},
    {"beacon_infodesc", integer_t, .addr.integer = &config.beacon.id}
};

const struct toml_key_t channels_keys[] = {
    {"enable", boolean_t, TABLEFIELD(struct channel, enable)},
    {"radio", integer_t, TABLEFIELD(struct channel, radio)},
    {"if", integer_t, TABLEFIELD(struct channel, if_freq)}
};

const struct toml_key_t lora_channel_keys[] = {
    {"enable", boolean_t, .addr.boolean = &config.chan_lora.enable},
    {"radio", integer_t, .addr.integer = &config.chan_lora.radio},
    {"if", integer_t, .addr.integer = &config.chan_lora.if_freq},
    {"bandwidth", integer_t, .addr.integer = &config.chan_lora.bw},
    {"spread_factor", integer_t, .addr.integer = &config.chan_lora.sf},
    {"implicit_hdr", boolean_t, .addr.boolean = &config.chan_lora.implicit_hdr},
    {"implicit_crc_en", boolean_t,
     .addr.boolean = &config.chan_lora.implicit_crc},
    {"implicit_payload_length", integer_t,
     .addr.integer = &config.chan_lora.implicit_payload_len},
    {"implicit_coderate", integer_t,
     .addr.integer = &config.chan_lora.implicit_coderate}
};

const struct toml_key_t gain_lut_keys[] = {
    {"rf_power", integer_t, TABLEFIELD(struct gain_lut, rf_power)},
    {"pa_gain", integer_t, TABLEFIELD(struct gain_lut, pa_gain)},
    {"pwr_idx", integer_t, TABLEFIELD(struct gain_lut, pwr_idx)},
    {NULL}
};

const struct toml_key_t radio_0_keys[] = {
    {"enable", boolean_t, .addr.boolean = &config.radios[0].enable},
    {"type", string_t, .addr.string = config.radios[0].type,
     .len = sizeof(config.radios[0].type)},
    {"freq", integer_t, .addr.integer = &config.radios[0].freq},
    {"rssi_offset", real_t, .addr.real = &config.radios[0].rssi_offset},
    {"rssi_tcomp_coeff_a", real_t,
     .addr.real = &config.radios[0].rssi_tcomp.coeff_a},
    {"rssi_tcomp_coeff_b", real_t,
     .addr.real = &config.radios[0].rssi_tcomp.coeff_b},
    {"rssi_tcomp_coeff_c", real_t,
     .addr.real = &config.radios[0].rssi_tcomp.coeff_c},
    {"rssi_tcomp_coeff_d", real_t,
     .addr.real = &config.radios[0].rssi_tcomp.coeff_d},
    {"rssi_tcomp_coeff_e", real_t,
     .addr.real = &config.radios[0].rssi_tcomp.coeff_e},
    {"tx_enable", boolean_t, .addr.boolean = &config.radios[0].tx.enable},
    {"tx_freq_min", integer_t, .addr.integer = &config.radios[0].tx.freq_min},
    {"tx_freq_max", integer_t, .addr.integer = &config.radios[0].tx.freq_max},
    {"tx_gain_lut", array_t,
     TABLEARRAY(config.radios[0].tx.gains, gain_lut_keys,
                &config.radios[0].tx.ngains)},
    {NULL}
};

const struct toml_key_t radio_1_keys[] = {
    {"enable", boolean_t, .addr.boolean = &config.radios[1].enable},
    {"type", string_t, .addr.string = config.radios[1].type,
     .len = sizeof(config.radios[1].type)},
    {"freq", integer_t, .addr.integer = &config.radios[1].freq},
    {"rssi_offset", real_t, .addr.real = &config.radios[1].rssi_offset},
    {"rssi_tcomp_coeff_a", real_t,
     .addr.real = &config.radios[1].rssi_tcomp.coeff_a},
    {"rssi_tcomp_coeff_b", real_t,
     .addr.real = &config.radios[1].rssi_tcomp.coeff_b},
    {"rssi_tcomp_coeff_c", real_t,
     .addr.real = &config.radios[1].rssi_tcomp.coeff_c},
    {"rssi_tcomp_coeff_d", real_t,
     .addr.real = &config.radios[1].rssi_tcomp.coeff_d},
    {"rssi_tcomp_coeff_e", real_t,
     .addr.real = &config.radios[1].rssi_tcomp.coeff_e},
    {"tx_enable", boolean_t, .addr.boolean = &config.radios[1].tx.enable},
    {"tx_freq_min", integer_t, .addr.integer = &config.radios[1].tx.freq_min},
    {"tx_freq_max", integer_t, .addr.integer = &config.radios[1].tx.freq_max},
    {"tx_gain_lut", array_t,
     TABLEARRAY(config.radios[1].tx.gains, gain_lut_keys,
                &config.radios[1].tx.ngains)},
    {NULL}
};

const struct toml_key_t config_keys[] = {
    {"type", string_t, .addr.string = config.type,
     .len = sizeof(config.type)},
    {"device", string_t, .addr.string = config.device,
     .len = sizeof(config.device)},
    {"clksrc", integer_t, .addr.integer = &config.clksrc},
    {"antenna_gain", integer_t, .addr.integer = &config.antenna_gain},
    {"lorawan_public", boolean_t, .addr.boolean = &config.lorawan_public},
    {"full_duplex", boolean_t, .addr.boolean = &config.full_duplex},
    {"fine_timestamp_enable", boolean_t, .addr.boolean = &config.ts.enable},
    {"fine_timestamp_mode", string_t, .addr.string = config.ts.mode,
     .len = sizeof(config.ts.mode)},
    {"channels", array_t,
     TABLEARRAY(config.channels, channels_keys, &config.nchannels)},
    {"LoRa_channel", table_t, .addr.keys = lora_channel_keys},
    {"radio_0", table_t, .addr.keys = radio_0_keys},
    {"radio_1", table_t, .addr.keys = radio_1_keys},
    {"beacon", table_t, .addr.keys = beacon_keys},
    {"gps", table_t, .addr.keys = gps_keys},
    {NULL}
};
