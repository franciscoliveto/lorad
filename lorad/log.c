/* log.c -- logging facility.
 *
 * Copyright (c) 2021, Francisco Oliveto <franciscoliveto@gmail.com>
 * SPDX-License-Identifier: BSD-2-Clause
 */
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

#include "log.h"

enum { NLEVELS = 8 };

int loglevel = LOG_ERR;
static const char *logident = NULL;
static bool logopen = false;

static const char *strlevels[NLEVELS] = {
    "EMERG",
    "ALERT",
    "CRIT",
    "ERR",
    "WARNING",
    "NOTICE",
    "INFO",
    "DEBUG"
};

void initlog(const char *ident, int level)
{
    logident = ident;
    loglevel = level;
}

void opensyslog()
{
    assert(logident != NULL);
    openlog(logident, LOG_PID, LOG_USER);
    logopen = true;
}

void printlog(int level, const char *fmt, ...)
{
    va_list ap;
    
    /* We might have gone background. Use
       syslog facility.

       FIXME: is there a generic way
       to know whether the process has gone background?
       (can I avoid using a flag?)
       Is this enough?: getppid() == 1 */
    if (logopen) {
        va_start(ap, fmt);
        vsyslog(level, fmt, ap);
        va_end(ap);
    } else {
        char buf[BUFSIZ];
        const char *strlevel = "UNK";
        size_t len;

        /* TODO: add timestamp. */

        if (level >= 0 && level < NLEVELS)
            strlevel = strlevels[level];

        assert(logident != NULL);
        snprintf(buf, sizeof(buf), "%s:%s: ", logident, strlevel);
        len = strlen(buf);

        va_start(ap, fmt);
        vsnprintf(buf + len, sizeof(buf) - len, fmt, ap);
        va_end(ap);

        fputs(buf, stderr);
    }
}

void printlog_hex(int level, const char *label,
                  uint8_t *hexbuf, size_t hexlen)
{
    size_t i;
    char buf[BUFSIZ];
    size_t len = 0;

    for (i = 0; i < hexlen; i++) {
        snprintf(buf + len, sizeof(buf) - len, "%02X ", hexbuf[i]);
        len = strlen(buf);
    }

    printlog(level, "%s%s\n", label, buf);
}
