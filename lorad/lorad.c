/*
 * / _____)             _              | |
 *( (____  _____ ____ _| |_ _____  ____| |__
 * \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 * _____) ) ____| | | || |_| ____( (___| | | |
 *(______/|_____)_|_|_| \__)_____)\____)_| |_|
 * (C)2019 Semtech
 *
 * Description:
 *    Configure Lora concentrator and forward packets to a server
 *    Use GPS for packet timestamping.
 *    Send a becon at a regular interval without server intervention
 *
 * License: Revised BSD License, see LICENSE.TXT file include in the project
 *
 * Copyright (c) 2021, Francisco Oliveto <franciscoliveto@gmail.com>
 */
#include "lorad_config.h" /* must be before all includes */

/* fix an issue between POSIX and C99 */
#if __STDC_VERSION__ >= 199901L
    #define _XOPEN_SOURCE 600
#else
    #define _XOPEN_SOURCE 500
#endif

#include <stdint.h>         /* C99 types */
#include <stdbool.h>        /* bool type */
#include <stdio.h>          /* printf, fprintf, snprintf, fopen, fputs */
#include <inttypes.h>       /* PRIx64, PRIu64... */

#include <string.h>         /* memset */
#include <signal.h>         /* sigaction */
#include <setjmp.h>
#include <time.h>           /* time, clock_gettime, strftime, gmtime */
#include <sys/time.h>       /* timeval */
#include <unistd.h>
#include <fcntl.h> /* fcntl(2) */
#include <stdlib.h>         /* atoi, exit */
#include <errno.h>          /* error messages */
#include <math.h>           /* modf */

#include <sys/socket.h>     /* socket specific definitions */
#include <netinet/in.h>     /* INET constants and stuff */
#include <arpa/inet.h>      /* IP address conversion stuff */
#include <netdb.h>          /* gai_strerror */

#include <sys/types.h> /* open(2), ftruncate(2) */
#include <sys/stat.h>
#include <pthread.h>

#include <assert.h>

#include "jitqueue.h"
#include "base64.h"
#include "lora.h"
#include "loragw_aux.h"
#include "loragw_reg.h"
#include "loragw_gps.h"

#include "log.h"
#include "os.h"
#include "strfuncs.h"
#include "net.h"
#include "lorad.h"
#include "lora_json.h"
#include "mjson.h"
#include "toml.h"
#include "config.h"


#define GPS_REF_MAX_AGE     30          /* maximum admitted delay in seconds of GPS loss before considering latest GPS sync unusable */
#define FETCH_SLEEP_MS      10          /* nb of ms waited when a fetch return no packets */
#define BEACON_POLL_MS      50          /* time in ms between polling of beacon TX status */


#define XERR_INIT_AVG       16          /* nb of measurements the XTAL correction is averaged on as initial value */
#define XERR_FILT_COEF      256         /* coefficient for low-pass XTAL error tracking */


#define MIN_LORA_PREAMB 6 /* minimum Lora preamble length for this application */
#define STD_LORA_PREAMB 8
#define MIN_FSK_PREAMB  3 /* minimum FSK preamble length for this application */
#define STD_FSK_PREAMB  5

#define UNIX_GPS_EPOCH_OFFSET 315964800 /* Number of seconds ellapsed between 01.Jan.1970 00:00:00
                                                                          and 06.Jan.1980 00:00:00 */
/* spectral scan */
typedef struct spectral_scan_s {
    bool enable;            /* enable spectral scan thread */
    uint32_t freq_hz_start; /* first channel frequency, in Hz */
    uint8_t nb_chan;        /* number of channels to scan (200kHz between each channel) */
    uint16_t nb_scan;       /* number of scan points for each frequency scan */
    uint32_t pace_s;        /* number of seconds between 2 scans in the thread */
} spectral_scan_t;


/* hardware access control and correction */
pthread_mutex_t mx_concent = PTHREAD_MUTEX_INITIALIZER; /* control access to the concentrator */
static pthread_mutex_t mx_xcorr = PTHREAD_MUTEX_INITIALIZER; /* control access to the XTAL correction */
static bool xtal_correct_ok = false; /* set true when XTAL correction is stable enough */
static double xtal_correct = 1.0;


static int gps_tty_fd = -1;
static bool gps_enabled = false;

/* GPS time reference */
static pthread_mutex_t mx_timeref = PTHREAD_MUTEX_INITIALIZER; /* control access to GPS time reference */
static bool gps_ref_valid; /* is GPS reference acceptable (ie. not too old) */
static struct tref time_reference_gps; /* time reference used for GPS <-> timestamp conversion */


static pthread_mutex_t mx_meas_gps = PTHREAD_MUTEX_INITIALIZER; /* control access to the GPS statistics */
static bool gps_coord_valid; /* could we get valid GPS coordinates ? */
static struct coord_s meas_gps_coord; /* GPS position of the gateway */
static struct coord_s meas_gps_err; /* GPS position of the gateway */


/* Just In Time TX scheduling */
static struct jit_queue_s jit_queue[LGW_RF_CHAIN_NB];

/* Spectral Scan */
static spectral_scan_t spectral_scan_params = {
    .enable = false,
    .freq_hz_start = 0,
    .nb_chan = 0,
    .nb_scan = 0,
    .pace_s = 10
};


static void gps_process_sync(void);
static void gps_process_coords(void);

void thread_down(void);
void thread_jit(void);
void thread_gps(void);
void thread_valid(void);
void thread_spectral_scan(void);

#define NELEMS(array) (sizeof(array) / sizeof(array[0]))

enum { BACKLOG = 1 };

extern struct config config;
extern struct toml_key_t config_keys[];

static jmp_buf jmpbuf;
static volatile sig_atomic_t signalled = 0;
static fd_set all_fds;
static int maxfd = 0;

/* FIXME: These will be removed/replaced */
static struct lgw_tx_gain_lut_s txlut[NRADIOS] = {{{{0}}, 0}};



static void signal_handler(int signum)
{
    signalled = (sig_atomic_t) signum;
}

static inline void usage(const char *progname)
{
    printf("Usage: %s [options]\n\n"
           "Options:\n"
           "  -c <file>       Read configuration from <file>.\n"
           "  -L <level>      Set logging <level> [0-7] (default 3).\n"
           "  -n              Don't go background. Needed if lorad is "
           "started and controlled by an init system.\n"
           "  -G              Listen on all addresses (INADDR_ANY).\n"
           "  -h              Display this help and exit.\n"
           "  -v              Display version information and exit.\n",
           progname);
}

/* FIXME: to be used with beacon. */

/* static uint16_t crc16(const uint8_t * data, unsigned size) { */
/*     const uint16_t crc_poly = 0x1021; */
/*     const uint16_t init_val = 0x0000; */
/*     uint16_t x = init_val; */
/*     unsigned i, j; */

/*     if (data == NULL)  { */
/*         return 0; */
/*     } */

/*     for (i=0; i<size; ++i) { */
/*         x ^= (uint16_t)data[i] << 8; */
/*         for (j=0; j<8; ++j) { */
/*             x = (x & 0x8000) ? (x<<1) ^ crc_poly : (x<<1); */
/*         } */
/*     } */

/*     return x; */
/* } */


#define LOCKFILE RUNDIR "/lorad.pid"

/* record-lock (exclusive write lock) the entire file
   referenced by 'fd'. If the file is already locked,
   -1 is returned, and errno is set to EACCES or EAGAIN. */
static inline int lockfile(int fd)
{
    struct flock lock;

    lock.l_type = F_WRLCK;
    lock.l_whence = SEEK_SET;
    lock.l_start = 0;
    lock.l_len = 0;
    return fcntl(fd, F_SETLK, &lock);
}

/* test whether LOCKFILE (usually /var/run/lorad.pid) is
   created and locked. If it is, 1 is returned. If no
   instance of the process is running, LOCKFILE is created
   and locked, and 0 is returned. Exit the process in case of error.*/
static int already_running()
{
    int fd;
    char buf[16];

    if ((fd = open(LOCKFILE, O_RDWR|O_CREAT,
                   S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH)) == -1) {
        fprintf(stderr, "Can't open %s: %s (%d)\n",
                LOCKFILE, strerror(errno), errno);
        exit(1);
    }

    if (lockfile(fd) == -1) {
        if (errno == EACCES || errno == EAGAIN) {
            /* Operation is prohibited by locks
               held by other processes. */
            close(fd);
            return 1;
        }
        fprintf(stderr, "Can't lock %s: %s (%d)\n",
                LOCKFILE, strerror(errno), errno);
        exit(1);
    }
    ftruncate(fd, 0);
    snprintf(buf, sizeof(buf), "%d\n", getpid());
    write(fd, buf, strlen(buf));
    return 0;
}

/* Writes buf to socket sockfd. */
static ssize_t lorad_write(int sockfd, const char *buf, size_t len)
{
    ssize_t n;

    if ((n = send(sockfd, buf, len, 0)) == -1) {
        /* FIXME: what would be the conditions for this to happen?
           should we request a transmit operation only when the
           socket fd is ready for writing (pselect())? */
        if (errno == EAGAIN ||
            errno == EWOULDBLOCK)
            /* the socket is marked nonblocking and send() would
               block. Let the user be warned about this. */
            log_warning("<-- client: send() would block.\n");
        else
            log_error("<-- client: send() failed: %s (%d)\n",
                      strerror(errno), errno);

        /* FIXME: detach client here? */
        return -1;
    }
    return n;
}

/* Writes the NULL-terminated string s plus a newline ('\n')
   to socket sockfd. */
static ssize_t lorad_writeln(int sockfd, const char *s)
{
    const char *nl = "\n";
    ssize_t n;

    if ((n = lorad_write(sockfd, s, strlen(s))) == -1) {
        return -1;
    }
    if (lorad_write(sockfd, nl, 1) == -1) {
        return -1;
    }
    return n + 1;
}

/* Forward incoming packets to 'sockfd' socket. */
static int forwardto(int sockfd, struct lora_packet_t *packets, int n,
                         bool valid_tref, struct tref tref)
{
    char buf[4096] = "UPLINK=";
    size_t len = strlen(buf);

    if (json_uplink_marshal(buf + len, sizeof(buf) - len, packets, n,
                            valid_tref, tref) == NULL) {
        log_error("<-- client: uplink JSON encoding failed.\n");
        return -1;
    }
    lorad_writeln(sockfd, buf);
    log_debug("<-- client: %s\n", buf);
    return 0;
}

/* Greets a client by sending a GATEWAY event. */
static void lorad_greetings(int sockfd, uint64_t eui)
{
    char buf[512] = "GATEWAY=";
    size_t len = strlen(buf);
    json_gateway_marshal(buf + len, sizeof(buf) - len, eui);
    lorad_writeln(sockfd, buf);
    log_debug("<-- client: %s\n", buf);
}

/* Track the largest fd currently in use */
static void adjust_max_fd(int fd, bool on)
{
    if (on) {
        if (fd > maxfd)
            maxfd = fd;
    } else if (fd == maxfd) {
        int i;

        maxfd = 0;
        for (i = 0; i < FD_SETSIZE; i++)
            if (FD_ISSET(i, &all_fds))
                maxfd = i;
    }
}

/* Wait for incoming data. */
static int await_data(fd_set *rfds, struct lora_packet_t *packets,
                      size_t size, int *npacket)
{
    struct timespec ts = {
        .tv_sec = 0,
        .tv_nsec = (FETCH_SLEEP_MS % 1000) * 1000000
    };
    fd_set fds = *rfds;

    while (signalled == 0) {
        int r;

        /* TODO: (THIS IS JUST AN IDEA) it would be great if we had a
           nonblocking lora_poll() function that "polls" the concentrator
           for packets. It would read the sx130x data FIFO's status
           register, read the data from the FIFO, parse it, and return
           the number of packets available in the liblora's internal
           static array of packets waiting to be read by lora_read()
           function. */
        pthread_mutex_lock(&mx_concent);
        r = lora_read(packets, size);
        pthread_mutex_unlock(&mx_concent);
        if (r == -1) {
            log_warning("can't read LoRa incoming packet(s).\n");
        } else if (r > 0) {
            /* This is equivalent to what would happend with
               the descriptor set if pselect() returned 0: the
               descriptors in the set are not ready, thus they
               are zeroed out. */
            FD_ZERO(rfds);
            *npacket = r;
            return 1; /* packets available */
        }

        r = pselect(maxfd + 1, rfds, NULL, NULL, &ts, NULL);
        if (r == -1) {
            if (errno == EINTR) {
                log_info("pselect: a signal was caught\n");
                continue;
            }
            log_error("pselect failed: %s (%d).\n",
                      strerror(errno), errno);
            return -1;
        }
        if (r == 0) {
            /* timeout: all the descriptor sets were zeroed out. */
            *rfds = fds; /* restore the sets. */
        } else
            return 1; /* descriptors are ready. */
    }
    return 0; /* a signal was caught */
}

/* Close file descriptor 'fd' and remove it from the sets. */
static void detach_client(int *fd)
{
    FD_CLR(*fd, &all_fds);
    adjust_max_fd(*fd, false);
    close(*fd);
    *fd = -1;
}

static int get_tx_gain_lut_index(uint8_t rf_chain, int8_t rf_power, uint8_t *lut_index)
{
    uint8_t pow_index;
    int current_best_index = -1;
    uint8_t current_best_match = 0xFF;
    int diff;

    /* Check input parameters */
    if (lut_index == NULL) {
        log_error("%s - wrong parameter\n", __FUNCTION__);
        return -1;
    }

    /* Search requested power in TX gain LUT */
    for (pow_index = 0; pow_index < txlut[rf_chain].size; pow_index++) {
        diff = rf_power - txlut[rf_chain].lut[pow_index].rf_power;
        if (diff < 0) {
            /* The selected power must be lower or equal to requested one */
            continue;
        } else {
            /* Record the index corresponding to the closest rf_power available in LUT */
            if ((current_best_index == -1) || (diff < current_best_match)) {
                current_best_match = diff;
                current_best_index = pow_index;
            }
        }
    }

    /* Return corresponding index */
    if (current_best_index > -1) {
        *lut_index = (uint8_t)current_best_index;
    } else {
        *lut_index = 0;
        log_error("%s - failed to find tx gain lut index\n", __FUNCTION__);
        return -1;
    }

    return 0;
}

/* Process downlink object. */
static int schedule_downlink(struct downlink_t *link, char *err, size_t errsiz)
{
    struct lgw_pkt_tx_s packet = {0};
    enum jit_pkt_type_e type;
    uint32_t curtime;

    if (link->immediate) {
        type = JIT_PKT_TYPE_DOWNLINK_CLASS_C;
        log_info("a packet will be sent in \"immediate\" mode\n");
    } else {
        if (link->hwtime > 0) {
            /* TX procedure: send on timestamp value */
            packet.count_us = link->hwtime;

            /* Concentrator timestamp is given,
               we consider it is a Class A downlink */
            type = JIT_PKT_TYPE_DOWNLINK_CLASS_A;
        } else {
            struct tref local_ref;
            struct timespec ts;
            double integer, frac;

            /* TX procedure: send on GPS time
               (converted to timestamp value) */
            if (link->gpstime == 0) {
                strlcpy(err, "Missing mandatory field: gpstime", errsiz);
                return -1;
            }

            if (!gps_enabled) {
                snprintf(err, errsiz, "GPS disabled, can't send packet "
                         "on GPS time (%d)\"}", link->gpstime);
                return -1;
            }

            if (gps_ref_valid == true) {
                pthread_mutex_lock(&mx_timeref);
                local_ref = time_reference_gps;
                pthread_mutex_unlock(&mx_timeref);
            } else {
                snprintf(err, errsiz, "No valid GPS time reference, "
                         "can't send packet on (%d)\"}", link->gpstime);
                return -1;
            }

            /* FIXME: improve this. */
            frac = modf((double)link->gpstime/1E3, &integer);
            ts.tv_sec = (time_t)integer;
            ts.tv_nsec = (long)(frac * 1E9);

            /* transform GPS time to timestamp */
            if (lgw_gps2cnt(local_ref, ts, &packet.count_us) == -1) {
                strlcpy(err, "Could not convert GPS "
                         "time to timestamp", errsiz);
                return -1;
            }

            log_info("a packet will be sent on timestamp value"
                     " %u (calculated from GPS time)\n", packet.count_us);

            /* GPS timestamp is given, we consider it is a Class B downlink */
            type = JIT_PKT_TYPE_DOWNLINK_CLASS_B;
        }
    }

    packet.no_crc = link->disable_crc;
    packet.no_header = link->disable_header;

    if (link->freq == 0) {
        strlcpy(err, "Missing mandatory field: freq", errsiz);
        return -1;
    }
    packet.freq_hz = (uint32_t)((double)(1.0e6) * link->freq);

    if (link->radio >= NRADIOS) {
        snprintf(err, errsiz, "radio %d is out of range", link->radio);
        return -1;
    }
    
    packet.rf_chain = link->radio;
    if (!config.radios[link->radio].tx.enable) {
        snprintf(err, errsiz, "transmission is not enable"
                 " on radio %d", link->radio);
        return -1;
    }

    packet.rf_power = link->power - config.antenna_gain;

    if (link->modulation[0] == '\0') {
        strlcpy(err, "Missing mandatory field: modulation", errsiz);
        return -1;
    }

    if (strcmp(link->modulation, "LoRa") == 0) {
        packet.modulation = MOD_LORA;

        if (link->sf == 0) {
            strlcpy(err, "Missing mandatory field: sf", errsiz);
            return -1;
        }

        if (link->sf < DR_LORA_SF5 || link->sf > DR_LORA_SF12) {
            snprintf(err, errsiz, "Invalid spreading factor"
                     " (%d)", link->sf);
            return -1;
        }
        packet.datarate = link->sf;

        switch (link->bw) {
        case 125:
            packet.bandwidth = BW_125KHZ;
            break;
        case 250:
            packet.bandwidth = BW_250KHZ;
            break;
        case 500:
            packet.bandwidth = BW_500KHZ;
            break;
        default:
            snprintf(err, errsiz, "Invalid bandwidth (%d)", link->bw);
            return -1;
        }

        if (link->cr[0] == '\0') {
            strlcpy(err, "Missing mandatory field: cr", errsiz);
            return -1;
        }
        if (strcmp(link->cr, "4/5") == 0)
            packet.coderate = CR_LORA_4_5;
        else if (strcmp(link->cr, "4/6") == 0)
            packet.coderate = CR_LORA_4_6;
        else if (strcmp(link->cr, "2/3") == 0)
            packet.coderate = CR_LORA_4_6;
        else if (strcmp(link->cr, "4/7") == 0)
            packet.coderate = CR_LORA_4_7;
        else if (strcmp(link->cr, "4/8") == 0)
            packet.coderate = CR_LORA_4_8;
        else if (strcmp(link->cr, "1/2") == 0)
            packet.coderate = CR_LORA_4_8;
        else {
            snprintf(err, errsiz, "Invalid coding rate (%s)", link->cr);
            return -1;
        }

        packet.invert_pol = link->pol;

        if (link->preamble > 0) {
            if (link->preamble >= MIN_LORA_PREAMB) {
                packet.preamble = link->preamble;
            } else {
                packet.preamble = (uint16_t)MIN_LORA_PREAMB;
            }
        } else {
            packet.preamble = (uint16_t)STD_LORA_PREAMB;
        }
#if notdef /* FSK is not currently supported. */
    } else if (strcmp(link->modulation, "FSK") == 0) {
        packet.modulation = MOD_FSK;

        if (link->baudrate == 0) {
            strlcpy(err, "Missing mandatory field: baudrate", errsiz);
            return -1;
        }
        packet.datarate = link->baudrate;

        if (link->freq_dev) {
            strlcpy(err, "Missing mandatory field: "
                    "(freq_deviation)", errsiz);
            return -1;
        }
        /* JSON value in Hz, packet.f_dev in kHz */
        packet.f_dev = (uint8_t)(link->freq_dev / 1000.0);

        if (link->preamble > 0) {
            if (link->preamble >= MIN_FSK_PREAMB) {
                packet.preamble = (uint16_t)link->preamble;
            } else {
                packet.preamble = (uint16_t)MIN_FSK_PREAMB;
            }
        } else {
            packet.preamble = (uint16_t)STD_FSK_PREAMB;
        }
#endif
    } else {
        snprintf(err, errsiz, "Invalid modulation (%s)",
                 link->modulation);
        return -1;
    }

    if (link->frame[0] == '\0') {
        strlcpy(err, "Missing mandatory field: frame", errsiz);
        return -1;
    }

    packet.size = b64_to_bin(link->frame, strlen(link->frame),
                             packet.payload, sizeof(packet.payload));

    packet.tx_mode = link->immediate ? IMMEDIATE : TIMESTAMPED;

    uint32_t freq_min = config.radios[link->radio].tx.freq_min;
    uint32_t freq_max = config.radios[link->radio].tx.freq_max;
    if (packet.freq_hz < freq_min || packet.freq_hz > freq_max) {
        snprintf(err, errsiz, "Packet rejected, frequency "
                 "not supported (%d)", packet.freq_hz);
        return -1;
    }

    {
        uint8_t i = 0;
        int r;
        r = get_tx_gain_lut_index(link->radio, packet.rf_power, &i);
        if (r < 0 || txlut[link->radio].lut[i].rf_power != packet.rf_power) {
            /* this RF power is not supported, throw a warning,
               and use the closest lower power supported */
            log_warning("Requested transmission power is not"
                        " supported (%ddBm), actual power used: %ddBm\n",
                        packet.rf_power, txlut[link->radio].lut[i].rf_power);
            packet.rf_power = txlut[link->radio].lut[i].rf_power;
        }
    }

    pthread_mutex_lock(&mx_concent);
    lgw_get_instcnt(&curtime);
    pthread_mutex_unlock(&mx_concent);

    if (jit_enqueue(&jit_queue[link->radio],
                    curtime, &packet, type) != JIT_ERROR_OK) {
        strlcpy(err, "Packet REJECTED, JiT failed", errsiz);
        return -1;
    }
    return 0;
}

/* Execute requests (?DOWNLINK, etc.) */
static void handle_request(int sockfd, const char *req)
{
    char resp[512];

    resp[0] = '\0';
    if (req[0] == '?') {
        req++;
        if (str_startswith(req, "DOWNLINK=")) {
            struct downlink_t link;
            int status;

            strlcpy(resp, "DOWNLINK=", sizeof(resp));

            status = json_downlink_unmarshal(req + 9, &link);
            if (status != 0) {
                sappendf(resp, sizeof(resp), "{\"status\":\"ERROR\","
                         "\"message\":\"Invalid DOWNLINK object: %s\"}",
                         json_error_string(status));
                log_error("<-- client: response: %s\n", resp);
            } else {
                char err[1024];

                if (schedule_downlink(&link, err, sizeof(err)) == -1) {
                    sappendf(resp, sizeof(resp), "{\"status\":\"ERROR\","
                             "\"message\":\"%s\"}", err);
                    log_error("<-- client: response %s\n", resp);
                } else {
                    strlcat(resp, "{\"status\":\"OK\"}", sizeof(resp));
                    log_debug("<-- client: response %s\n", resp);
                }
            }
        } else {
            strlcpy(resp, "ERROR=", sizeof(resp));
            sappendf(resp, sizeof(resp),
                     "{\"message\":\"Unknown command\"}");
            log_error("<-- client: ERROR response: %s\n", resp);
        }
        lorad_writeln(sockfd, resp);
    }
}

/* Initialize radio transceiver n. */
static int init_radio(int n, const struct radio *radio)
{
    struct lgw_conf_rxrf_s rf = {0};
    
    rf.enable = radio->enable;
    if (rf.enable) {
        if (strcmp(radio->type, "SX1255") == 0) {
            rf.type = LGW_RADIO_TYPE_SX1255;
        } else if (strcmp(radio->type, "SX1257") == 0) {
            rf.type = LGW_RADIO_TYPE_SX1257;
        } else if (strcmp(radio->type, "SX1250") == 0) {
            rf.type = LGW_RADIO_TYPE_SX1250;
        } else {
            log_error("config.radio_%d.type %s is invalid\n",
                      n, radio->type);
            return -1;
        }
        rf.freq_hz = radio->freq;
        rf.rssi_offset = radio->rssi_offset;
        rf.rssi_tcomp.coeff_a = radio->rssi_tcomp.coeff_a;
        rf.rssi_tcomp.coeff_b = radio->rssi_tcomp.coeff_b;
        rf.rssi_tcomp.coeff_c = radio->rssi_tcomp.coeff_c;
        rf.rssi_tcomp.coeff_d = radio->rssi_tcomp.coeff_d;
        rf.rssi_tcomp.coeff_e = radio->rssi_tcomp.coeff_e;
        /* TODO: add "config.radio_x.single_input_mode" parameter. */
        rf.tx_enable = radio->tx.enable;
        if (rf.tx_enable) {
            int i;
            /* TODO: add "dac_gain", "mix_gain", "dig_gain". */
            for (i = 0; i < (int)NELEMS(txlut[n].lut) &&
                         i < radio->tx.ngains; i++) {
                struct lgw_tx_gain_s *lut = &txlut[n].lut[i];
                /* FIXME: mix_gain was hardcoded in the original code.
                   What is it? */
                lut->mix_gain = 5;
                lut->pwr_idx = radio->tx.gains[i].pwr_idx;
                lut->rf_power = radio->tx.gains[i].rf_power;
                lut->pa_gain = radio->tx.gains[i].pa_gain;
            }
            txlut[n].size = i;
            if (lgw_txgain_setconf(n, &txlut[n]) == -1) {
                log_error("lgw_txgain_setconf failed\n");
                return -1;
            }
        }
        if (lgw_rxrf_setconf(n, &rf) == -1) {
            log_error("lgw_rxrf_setconf failed\n");
            return -1;
        }
    }
    return 0;
}

/* Initialize IF channel n. */
static int init_channel(int n, const struct channel *channel)
{
    struct lgw_conf_rxif_s ifc = {0};

    ifc.enable = channel->enable;
    ifc.rf_chain = channel->radio;
    ifc.freq_hz = channel->if_freq;
    switch (channel->bw) {
        case 500000:
            ifc.bandwidth = BW_500KHZ;
            break;
        case 250000:
            ifc.bandwidth = BW_250KHZ;
            break;
        case 125000:
            ifc.bandwidth = BW_125KHZ;
            break;
        default:
            ifc.bandwidth = BW_UNDEFINED;
    }

    ifc.datarate = DR_UNDEFINED;
    if (channel->sf >= 5 && channel->sf <= 12) {
        ifc.datarate = channel->sf;
    }
    ifc.implicit_hdr = channel->implicit_hdr;
    ifc.implicit_crc_en = channel->implicit_crc;
    ifc.implicit_payload_length = channel->implicit_payload_len;
    ifc.implicit_coderate = channel->implicit_coderate;
    if (lgw_rxif_setconf(n, &ifc) == -1) {
        log_error("lgw_rxif_setconf failed\n");
        return -1;
    }
    return 0;
}

/* Initialize SX130x engine and radio transceivers. */
static int init_hardware(const struct config *engine)
{
    /* TODO: load sx1261's settings (lgw_sx1261_setconf) */
    struct lgw_conf_board_s bc = {0};
    struct lgw_conf_ftime_s ts = {0};
    struct lgw_conf_demod_s dem = {0};

    bc.com_type = LGW_COM_UNKNOWN;
    if (strcmp(engine->type, "SPI") == 0) {
        bc.com_type = LGW_COM_SPI;
    }
    strlcpy(bc.com_path, engine->device, sizeof(bc.com_path));
    bc.lorawan_public = engine->lorawan_public;
    bc.clksrc = engine->clksrc;
    bc.full_duplex = engine->full_duplex;
    if (lgw_board_setconf(&bc) == -1) {
        log_error("lgw_board_setconf failed\n");
        return -1;
    }

    ts.enable = engine->ts.enable;
    if (ts.enable) {
        if (strcmp(engine->ts.mode, "high_capacity") == 0) {
            ts.mode = LGW_FTIME_MODE_HIGH_CAPACITY;
        } else if (strcmp(engine->ts.mode, "all_sf") == 0) {
            ts.mode = LGW_FTIME_MODE_ALL_SF;
        } else {
            log_error("config.fine_timestamp_mode %s is invalid\n",
                      engine->ts.mode);
            return -1;
        }
        lgw_ftime_setconf(&ts);
    }

    for (int i = 0; i < NRADIOS; i++) {
        if (init_radio(i, &engine->radios[i]) == -1) {
            log_error("init_radio failed on radio %d\n", i);
            return -1;
        }
    }

    /* enable all SFs by default */
    dem.multisf_datarate = 0xFF;
    lgw_demod_setconf(&dem);

    /* IF[0-7] channels */
    for (int i = 0; i < engine->nchannels; i++) {
        if (init_channel(i, &engine->channels[i]) == -1) {
            log_error("IF%d channel init failed\n", i);
            return -1;
        }
    }
    /* IF8 LoRa channel */
    if (init_channel(8, &engine->chan_lora) == -1) {
        log_error("IF8 LoRa channel init failed\n");
        return -1;
    }
    return 0;
}

int main(int argc, char *argv[])
{
    int i;

    /* threads */
    pthread_t thrid_gps;
    pthread_t thrid_valid;
    pthread_t thrid_jit;
    pthread_t thrid_ss;

    int opt;
    FILE *toml_fp = NULL;
    char *toml_file = NULL;
    int status;
    int level = LOG_ERR;
    bool go_background = true;
    uint64_t eui;
    int sfd = -1; /* server socket fd */
    int cfd = -1; /* client socket fd. */
    char *service = NULL;
    const char *host = "localhost";


    while ((opt = getopt(argc, argv, "L:c:nGvh")) != -1) {
        switch (opt) {
        case 'L':
            level = (int)strtol(optarg, NULL, 10);
            break;
        case 'c':
            toml_file = optarg;
            break;
        case 'n':
            go_background = false;
            break;
        case 'G':
            host = NULL; /* listen on all addresses (INADDR_ANY) */
            break;
        case 'v':
            printf("%s: version %s\n"
                   "liblora: version %s\n",
                   argv[0], VERSION, LIBLORA_VERSION);
            exit(EXIT_SUCCESS);
        case 'h':
            usage(argv[0]);
            exit(EXIT_SUCCESS);
        default: /* '?' */
            usage(argv[0]);
            exit(EXIT_FAILURE);
        }
    }

    if (toml_file == NULL) {
        fprintf(stderr, "%s: configuration file is mandatory\n", argv[0]);
        usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    if (level < 0 || level > LOG_DEBUG) {
        fprintf(stderr, "%s: invalid log level (%d)\n", argv[0], level);
        usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    if (already_running()) {
        fprintf(stderr, "an instance of lorad is already running.\n");
        exit(EXIT_FAILURE);
    }

    initlog("lorad", level);

    if (go_background) {
        if (daemonize() == -1) {
            fprintf(stderr, "can't go background.\n");
            exit(EXIT_FAILURE);
        }
        /* If we made it to here, it means we are
           running in background.
           FIXME: I'm doing this right here to tell the log
           that lorad became a daemon so it can send logging
           output to syslogd instead of stderr. But there has
           to be a way to avoid this and let the process decide
           for itself whether it's background or not. */
        opensyslog();
    }

    log_info("launching (version %s)\n", VERSION);

    {
        struct sigaction sa;

        sa.sa_flags = 0;
        sigfillset(&sa.sa_mask);
        sa.sa_handler = signal_handler;
        sigaction(SIGQUIT, &sa, NULL);
        sigaction(SIGINT, &sa, NULL);
        sigaction(SIGTERM, &sa, NULL);
        sigaction(SIGHUP, &sa, NULL);
    }

    if (setjmp(jmpbuf) > 0)
        log_warning("lorad restarted by SIGHUP\n");

    signalled = 0;

    if ((toml_fp = fopen(toml_file, "r")) == NULL) {
        log_error("can't open file %s\n", toml_file);
        exit(EXIT_FAILURE);
    }
    if ((status = toml_load(toml_fp, config_keys)) == -1) {
        log_error("toml_load failed: %d.\n", status);
        exit(EXIT_FAILURE);
    }
    fclose(toml_fp);

    if (init_hardware(&config) == -1) {
        log_error("concentrator initialization failed.\n");
        exit(EXIT_FAILURE);
    }

    /* FIXME: this will eventually be deprecated.
       We'll get gps data from gpsd service. */
    i = lgw_gps_enable(config.gps.device, "ubx7",
                       0, &gps_tty_fd);
    if (i != LGW_GPS_SUCCESS) {
        log_warning("[main] impossible to open %s for"
                    " GPS sync (check permissions)\n",
                    config.gps.device);
        gps_enabled = false;
        gps_ref_valid = false;
    } else {
        log_info("[main] TTY port %s open for GPS "
                 "synchronization\n", config.gps.device);
        gps_enabled = true;
        gps_ref_valid = false;
    }


    /* get timezone info */
    tzset();

    FD_ZERO(&all_fds);
    maxfd = 0;

    service = getservbyname("lorad", "tcp") ? "lorad" : DEFAULT_LORAD_PORT;
    if ((sfd = net_listen(NET_TCP, host, service, BACKLOG)) < 0) {
        log_error("can't listen on port %s: %s\n",
                  service, net_strerror(sfd, errno));
        exit(EXIT_FAILURE);
    }
    log_info("listening on port %s.\n", service);

    FD_SET(sfd, &all_fds);
    adjust_max_fd(sfd, true);


    /* FIXME: this must go into liblora. */
    if (system("/usr/local/bin/reset.sh start") != 0) {
        log_error("failed to reset SX1302, check your reset.sh script\n");
        exit(EXIT_FAILURE);
    }

    if (lgw_start() == -1) {
        log_error("Failed to start the concentrator\n");
        exit(EXIT_FAILURE);
    }

    log_info("Concentrator started, packet can now be received\n");

    if (lgw_get_eui(&eui) == -1) {
        log_error("Failed to get concentrator EUI\n");
        exit(EXIT_FAILURE);
    }

    log_info("Concentrator EUI: 0x%016" PRIx64 "\n", eui);

    i = pthread_create(&thrid_jit, NULL,
                       (void * (*)(void *))thread_jit,
                       NULL);
    if (i != 0) {
        log_error("[main] impossible to create JIT thread\n");
        exit(EXIT_FAILURE);
    }

    /* spawn thread for background spectral scan */
    if (spectral_scan_params.enable == true) {
        i = pthread_create(&thrid_ss, NULL,
                           (void * (*)(void *))thread_spectral_scan,
                           NULL);
        if (i != 0) {
            log_error("[main] impossible to create Spectral Scan thread\n");
            exit(EXIT_FAILURE);
        }
    }

    /* spawn thread to manage GPS */
    if (gps_enabled == true) {
        i = pthread_create(&thrid_gps, NULL,
                           (void * (*)(void *))thread_gps,
                           NULL);
        if (i != 0) {
            log_error("[main] impossible to create GPS thread\n");
            exit(EXIT_FAILURE);
        }
        i = pthread_create(&thrid_valid, NULL,
                           (void * (*)(void *))thread_valid,
                           NULL);
        if (i != 0) {
            log_error("[main] impossible to create validation thread\n");
            exit(EXIT_FAILURE);
        }
    }

    /* FIXME: study this! */
    jit_queue_init(&jit_queue[0]);
    jit_queue_init(&jit_queue[1]);

    while (signalled == 0) {
        bool ref_ok = false;
        struct tref local_ref = {0};
        struct lora_packet_t packets[255];
        int r, npacket = 0;
        fd_set rfds = all_fds;

        r = await_data(&rfds, packets, NELEMS(packets), &npacket);
        if (r == -1) {
            log_error("await_data error.\n");
            /* FIXME: should we exit, should we try again? */
            exit(EXIT_FAILURE);
        }
        if (r == 0) {
            log_info("await_data interrupted by signal.\n");
            continue;
        }

        /* access GPS time. */
        if (gps_enabled) {
            pthread_mutex_lock(&mx_timeref);
            ref_ok = gps_ref_valid;
            local_ref = time_reference_gps;
            pthread_mutex_unlock(&mx_timeref);
        }

        /* always be open to new client connections. */
        if (sfd >= 0 && FD_ISSET(sfd, &rfds)) {
            int sockfd;
            struct sockaddr addr;
            socklen_t addrsiz = sizeof(addr);

            sockfd = accept(sfd, &addr, &addrsiz);
            if (sockfd == -1)
                log_error("accept failed: %s (%d).\n",
                          strerror(errno), errno);
            else if (cfd >= 0) {
                log_warning("can't accept connection, "
                            "a client is already active.\n");
                close(sockfd);
            } else {
                int opts;

                cfd = sockfd;

                if ((opts = fcntl(cfd, F_GETFL)) == -1) {
                    log_error("fcntl(F_GETFL) failed: %s (%d)\n",
                              strerror(errno), errno);
                    close(cfd);
                } else if (fcntl(cfd, F_SETFL, opts|O_NONBLOCK) == -1) {
                    log_error("fcntl(F_SETFL) failed: %s (%d)\n",
                              strerror(errno), errno);
                    close(cfd);
                } else {
                    /* TODO: log client's IP:port. */
                    log_info("--> client: connected on fd %d.\n", cfd);
                    FD_SET(cfd, &all_fds);
                    adjust_max_fd(cfd, true);
                    lorad_greetings(cfd, eui);
                }
            }
        }

        /* be ready to forward incoming LoRa packets to
           the interface client. */
        if (npacket > 0) {
            if (cfd >= 0) {
                log_info("<-- client: forwarding incoming LoRa packets.\n");
                forwardto(cfd, packets, npacket, ref_ok, local_ref);
            }
        }

        /* accept and execute commands for the interface client. */
        if (cfd >= 0 && FD_ISSET(cfd, &rfds)) {
            char buf[512];
            ssize_t len;
            /* TODO: wrap this in a readline() function. */
            len = recv(cfd, buf, sizeof(buf)-1, 0);
            if (len == -1) {
                /* recv(2) states that "if no messages are available
                   at the socket, the receive calls wait for a message
                   to arrive, unless the socket is nonblocking, in
                   which case the value -1 is returned and the external
                   variable errno is set to EAGAIN or EWOULDBLOCK".
                   If we got here it was because the socket is ready
                   for a nonblocking read operation, so this should not
                   happen. Let's just warning the user and move on. */
                if (errno == EAGAIN ||
                    errno == EWOULDBLOCK) {
                    log_warning("--> client: no available data,"
                                " recv() would block.\n");
                    continue;
                }
                detach_client(&cfd);
                log_error("--> client: recv failed: %s (%d).\n",
                          strerror(errno), errno);
            } else if (len == 0) { /* peer has disconnected. */
                detach_client(&cfd);
                log_info("--> client: disconnected.\n");
            } else {
                buf[len] = '\0';
                /* FIXME: what if multiple requests (streams of US-ASCII 
                   characters ending in \n) are received? how do we
                   handle this situation? */
                if (buf[len-1] == '\n') {
                    buf[len-1] = '\0';
                }
                log_debug("--> client: %s\n", buf);
                handle_request(cfd, buf);
            }
        }
    }

    i = pthread_join(thrid_jit, NULL);
    if (i != 0)
        log_error("failed to join JIT thread with"
                  " %d - %s\n", i, strerror(errno));

    if (spectral_scan_params.enable == true) {
        i = pthread_join(thrid_ss, NULL);
        if (i != 0)
            log_error("failed to join Spectral Scan thread"
                      " with %d - %s\n", i, strerror(errno));
    }

    if (gps_enabled == true) {
        /* don't wait for GPS thread,
           no access to concentrator board */
        pthread_cancel(thrid_gps);
        /* don't wait for validation thread,
           no access to concentrator board */
        pthread_cancel(thrid_valid);

        i = lgw_gps_disable(gps_tty_fd);
        if (i == 0)
            log_info("GPS closed successfully\n");
        else
            log_warning("failed to close GPS successfully\n");
    }

    if (lora_close() == -1) {
        log_error("failed to close the LoRa concentrator"
                  " successfully.\n");
        exit(EXIT_FAILURE);
    }

    /* FIXME: this must go into liblora. */
    if (system("/usr/local/bin/reset.sh stop") != 0) {
        log_error("failed to reset SX1302, check your reset.sh script\n");
        exit(EXIT_FAILURE);
    }

    /* restart on SIGHUP */
    if ((int) signalled == SIGHUP) {
        longjmp(jmpbuf, 1);
    }

    log_info("exiting\n");
    return EXIT_SUCCESS;
}

/* FIXME: add beacon functionality. */

/* void thread_down(void) */
/* { */
/*     /\* beacon variables *\/ */
/*     struct lgw_pkt_tx_s beacon_pkt; */
/*     uint8_t beacon_chan; */
/*     uint8_t beacon_loop; */
/*     size_t beacon_RFU1_size = 0; */
/*     size_t beacon_RFU2_size = 0; */
/*     uint8_t beacon_pyld_idx = 0; */
/*     time_t diff_beacon_time; */
/*     /\* gps time of next beacon packet *\/ */
/*     struct timespec next_beacon_gps_time; */
/*     /\* gps time of last enqueued beacon packet *\/ */
/*     struct timespec last_beacon_gps_time; */
/*     int retry; */

/*     /\* beacon data fields, byte 0 is Least Significant Byte *\/ */
/*     /\* 3 bytes, derived from  */
/*        reference latitude *\/ */
/*     int32_t field_latitude; */
/*     /\* 3 bytes, derived from */
/*        reference longitude *\/ */
/*     int32_t field_longitude; */
/*     uint16_t field_crc1, field_crc2; */



/*     /\* beacon variables initialization *\/ */
/*     last_beacon_gps_time.tv_sec = 0; */
/*     last_beacon_gps_time.tv_nsec = 0; */

/*     /\* beacon packet parameters *\/ */
/*     beacon_pkt.tx_mode = ON_GPS; /\* send on PPS pulse *\/ */
/*     beacon_pkt.rf_chain = 0; /\* antenna A *\/ */
/*     beacon_pkt.rf_power = config.beacon.power; */
/*     beacon_pkt.modulation = MOD_LORA; */
/*     switch (config.beacon.bw) { */
/*         case 125000: */
/*             beacon_pkt.bandwidth = BW_125KHZ; */
/*             break; */
/*         case 500000: */
/*             beacon_pkt.bandwidth = BW_500KHZ; */
/*             break; */
/*         default: */
/*             /\* should not happen *\/ */
/*             log_error("unsupported bandwidth for beacon\n"); */
/*             exit(EXIT_FAILURE); */
/*     } */
/*     switch (config.beacon.dr) { */
/*         case 8: */
/*             beacon_pkt.datarate = DR_LORA_SF8; */
/*             beacon_RFU1_size = 1; */
/*             beacon_RFU2_size = 3; */
/*             break; */
/*         case 9: */
/*             beacon_pkt.datarate = DR_LORA_SF9; */
/*             beacon_RFU1_size = 2; */
/*             beacon_RFU2_size = 0; */
/*             break; */
/*         case 10: */
/*             beacon_pkt.datarate = DR_LORA_SF10; */
/*             beacon_RFU1_size = 3; */
/*             beacon_RFU2_size = 1; */
/*             break; */
/*         case 12: */
/*             beacon_pkt.datarate = DR_LORA_SF12; */
/*             beacon_RFU1_size = 5; */
/*             beacon_RFU2_size = 3; */
/*             break; */
/*         default: */
/*             /\* should not happen *\/ */
/*             log_error("unsupported datarate for beacon\n"); */
/*             exit(EXIT_FAILURE); */
/*     } */
/*     beacon_pkt.size = beacon_RFU1_size + 4 + 2 + 7 + beacon_RFU2_size + 2; */
/*     beacon_pkt.coderate = CR_LORA_4_5; */
/*     beacon_pkt.invert_pol = false; */
/*     beacon_pkt.preamble = 10; */
/*     beacon_pkt.no_crc = true; */
/*     beacon_pkt.no_header = true; */

/*     /\* network common part beacon fields (little endian) *\/ */
/*     for (i = 0; i < (int)beacon_RFU1_size; i++) { */
/*         beacon_pkt.payload[beacon_pyld_idx++] = 0x0; */
/*     } */

/*     /\* network common part beacon fields (little endian) *\/ */
/*     beacon_pyld_idx += 4; /\* time (variable), filled later *\/ */
/*     beacon_pyld_idx += 2; /\* crc1 (variable), filled later *\/ */

/*     /\* calculate the latitude and longitude that must be publicly reported *\/ */
/*     field_latitude = (int32_t)((config.gps.lat / 90.0) * (double)(1<<23)); */
/*     if (field_latitude > (int32_t)0x007FFFFF) { */
/*         field_latitude = (int32_t)0x007FFFFF; /\* +90 N is represented as 89.99999 N *\/ */
/*     } else if (field_latitude < (int32_t)0xFF800000) { */
/*         field_latitude = (int32_t)0xFF800000; */
/*     } */
/*     field_longitude = (int32_t)((config.gps.lon / 180.0) * (double)(1<<23)); */
/*     if (field_longitude > (int32_t)0x007FFFFF) { */
/*         field_longitude = (int32_t)0x007FFFFF; /\* +180 E is represented as 179.99999 E *\/ */
/*     } else if (field_longitude < (int32_t)0xFF800000) { */
/*         field_longitude = (int32_t)0xFF800000; */
/*     } */

/*     /\* gateway specific beacon fields *\/ */
/*     beacon_pkt.payload[beacon_pyld_idx++] = config.beacon.id; */
/*     beacon_pkt.payload[beacon_pyld_idx++] = 0xFF &  field_latitude; */
/*     beacon_pkt.payload[beacon_pyld_idx++] = 0xFF & (field_latitude >>  8); */
/*     beacon_pkt.payload[beacon_pyld_idx++] = 0xFF & (field_latitude >> 16); */
/*     beacon_pkt.payload[beacon_pyld_idx++] = 0xFF &  field_longitude; */
/*     beacon_pkt.payload[beacon_pyld_idx++] = 0xFF & (field_longitude >>  8); */
/*     beacon_pkt.payload[beacon_pyld_idx++] = 0xFF & (field_longitude >> 16); */

/*     /\* RFU *\/ */
/*     for (i = 0; i < (int)beacon_RFU2_size; i++) { */
/*         beacon_pkt.payload[beacon_pyld_idx++] = 0x0; */
/*     } */

/*     /\* CRC of the beacon gateway specific part fields *\/ */
/*     field_crc2 = crc16((beacon_pkt.payload + 6 + beacon_RFU1_size), 7 + beacon_RFU2_size); */
/*     beacon_pkt.payload[beacon_pyld_idx++] = 0xFF &  field_crc2; */
/*     beacon_pkt.payload[beacon_pyld_idx++] = 0xFF & (field_crc2 >> 8); */

/*     /\* JIT queue initialization *\/ */
    
/*             /\* Pre-allocate beacon slots in JiT queue,  */

/*                to check downlink collisions *\/ */
/*             beacon_loop = JIT_NUM_BEACON_IN_QUEUE - jit_queue[0].num_beacon; */
/*             retry = 0; */
/*             while (beacon_loop && (config.beacon.period != 0)) { */

/*                 pthread_mutex_lock(&mx_timeref); */
/*                 /\* Wait for GPS to be ready before inserting */
/*                    beacons in JiT queue *\/ */
/*                 if ((gps_ref_valid == true) && */
/*                     (xtal_correct_ok == true)) { */

/*                     /\* compute GPS time for next beacon to come      *\/ */
/*                     /\*   LoRaWAN: T = k*config.beacon.period + TBeaconDelay *\/ */
/*                     /\*            with TBeaconDelay = [1.5ms +/- 1µs]*\/ */
/*                     if (last_beacon_gps_time.tv_sec == 0) { */
/*                         /\* if no beacon has been queued, get next slot from current GPS time *\/ */
/*                         diff_beacon_time = time_reference_gps.gps.tv_sec % ((time_t)config.beacon.period); */
/*                         next_beacon_gps_time.tv_sec = time_reference_gps.gps.tv_sec + */
/*                                                         ((time_t)config.beacon.period - diff_beacon_time); */
/*                     } else { */
/*                         /\* if there is already a beacon, take it as reference *\/ */
/*                         next_beacon_gps_time.tv_sec = last_beacon_gps_time.tv_sec + config.beacon.period; */
/*                     } */
/*                     /\* now we can add a config.beacon.period to the reference to get next beacon GPS time *\/ */
/*                     next_beacon_gps_time.tv_sec += (retry * config.beacon.period); */
/*                     next_beacon_gps_time.tv_nsec = 0; */

/* #if DEBUG_BEACON */
/*                     { */
/*                         time_t time_unix; */

/*                         time_unix = time_reference_gps.gps.tv_sec + UNIX_GPS_EPOCH_OFFSET; */
/*                         log_debug("GPS-now : %s", ctime(&time_unix)); */
/*                         time_unix = last_beacon_gps_time.tv_sec + UNIX_GPS_EPOCH_OFFSET; */
/*                         log_debug("GPS-last: %s", ctime(&time_unix)); */
/*                         time_unix = next_beacon_gps_time.tv_sec + UNIX_GPS_EPOCH_OFFSET; */
/*                         log_debug("GPS-next: %s", ctime(&time_unix)); */
/*                     } */
/* #endif */

/*                     /\* convert GPS time to concentrator time,  */
/*                        and set packet counter for JiT trigger *\/ */
/*                     lgw_gps2cnt(time_reference_gps, next_beacon_gps_time, */
/*                                 &(beacon_pkt.count_us)); */
/*                     pthread_mutex_unlock(&mx_timeref); */

/*                     /\* apply frequency correction to beacon TX frequency *\/ */
/*                     if (config.beacon.freq.nb > 1) { */
/*                         /\* floor rounding *\/ */
/*                         beacon_chan = (next_beacon_gps_time.tv_sec / config.beacon.period) % config.beacon.freq.nb;  */
/*                     } else { */
/*                         beacon_chan = 0; */
/*                     } */
/*                     /\* Compute beacon frequency *\/ */
/*                     beacon_pkt.freq_hz = config.beacon.freq.hz + (beacon_chan * config.beacon.freq.step); */

/*                     /\* load time in beacon payload *\/ */
/*                     beacon_pyld_idx = beacon_RFU1_size; */
/*                     beacon_pkt.payload[beacon_pyld_idx++] = 0xFF &  next_beacon_gps_time.tv_sec; */
/*                     beacon_pkt.payload[beacon_pyld_idx++] = 0xFF & (next_beacon_gps_time.tv_sec >>  8); */
/*                     beacon_pkt.payload[beacon_pyld_idx++] = 0xFF & (next_beacon_gps_time.tv_sec >> 16); */
/*                     beacon_pkt.payload[beacon_pyld_idx++] = 0xFF & (next_beacon_gps_time.tv_sec >> 24); */

/*                     /\* calculate CRC *\/ */
/*                     /\* CRC for the network common part *\/ */
/*                     field_crc1 = crc16(beacon_pkt.payload, 4 + beacon_RFU1_size); */
/*                     beacon_pkt.payload[beacon_pyld_idx++] = 0xFF & field_crc1; */
/*                     beacon_pkt.payload[beacon_pyld_idx++] = 0xFF & (field_crc1 >> 8); */

/*                     /\* Insert beacon packet in JiT queue *\/ */
/*                     pthread_mutex_lock(&mx_concent); */
/*                     lgw_get_instcnt(&current_concentrator_time); */
/*                     pthread_mutex_unlock(&mx_concent); */
/*                     jit_result = jit_enqueue(&jit_queue[0], */
/*                                              current_concentrator_time, */
/*                                              &beacon_pkt, JIT_PKT_TYPE_BEACON); */
/*                     if (jit_result == JIT_ERROR_OK) { */
/*                         /\* One more beacon in the queue *\/ */
/*                         beacon_loop--; */
/*                         retry = 0; */
/*                         /\* keep this beacon time as reference for next one to be programmed *\/ */
/*                         last_beacon_gps_time.tv_sec = next_beacon_gps_time.tv_sec; */

/*                         /\* display beacon payload *\/ */
/*                         log_info("Beacon queued (count_us=%u, " */
/*                                  "freq_hz=%u, size=%u):\n", */
/*                                  beacon_pkt.count_us, beacon_pkt.freq_hz, */
/*                                  beacon_pkt.size); */
/*                         printf( "   => " ); */
/*                         for (i = 0; i < beacon_pkt.size; ++i) { */
/*                             log_debug("%02X ", beacon_pkt.payload[i]); */
/*                         } */
/*                         log_debug("\n"); */
/*                     } else { */
/*                         log_debug("--> beacon queuing failed with %d\n", jit_result); */

/*                         /\* In case previous enqueue failed, we retry one */
/*                            period later until it succeeds. */
/*                            Note: In case the GPS has been unlocked for */
/*                            a while, there can be lots of retries to be */
/*                            done from last beacon time to a new valid one *\/ */
/*                         retry++; */
/*                         log_debug("--> beacon queuing retry=%d\n", retry); */
/*                     } */
/*                 } else { */
/*                     pthread_mutex_unlock(&mx_timeref); */
/*                     break; */
/*                 } */
/* } */

void print_tx_status(uint8_t tx_status) {
    switch (tx_status) {
        case TX_OFF:
            log_info("[jit] lgw_status returned TX_OFF\n");
            break;
        case TX_FREE:
            log_info("[jit] lgw_status returned TX_FREE\n");
            break;
        case TX_EMITTING:
            log_info("[jit] lgw_status returned TX_EMITTING\n");
            break;
        case TX_SCHEDULED:
            log_info("[jit] lgw_status returned TX_SCHEDULED\n");
            break;
        default:
            log_info("[jit] lgw_status returned UNKNOWN (%d)\n", tx_status);
            break;
    }
}


/* -------------------------------------------------------------------------- */
/* --- THREAD 3: CHECKING PACKETS TO BE SENT FROM JIT QUEUE AND SEND THEM --- */

void thread_jit(void) {
    int result = 0;
    struct lgw_pkt_tx_s pkt;
    int pkt_index = -1;
    uint32_t current_concentrator_time;
    enum jit_error_e jit_result;
    enum jit_pkt_type_e pkt_type;
    uint8_t tx_status;
    int i;

    while (signalled == 0) {
        wait_ms(10);

        for (i = 0; i < LGW_RF_CHAIN_NB; i++) {
            /* transfer data and metadata to the concentrator, and schedule TX */
            pthread_mutex_lock(&mx_concent);
            lgw_get_instcnt(&current_concentrator_time);
            pthread_mutex_unlock(&mx_concent);
            jit_result = jit_peek(&jit_queue[i], current_concentrator_time, &pkt_index);
            if (jit_result == JIT_ERROR_OK) {
                if (pkt_index > -1) {
                    jit_result = jit_dequeue(&jit_queue[i], pkt_index, &pkt, &pkt_type);
                    if (jit_result == JIT_ERROR_OK) {
                        /* update beacon stats */
                        if (pkt_type == JIT_PKT_TYPE_BEACON) {
                            /* Compensate breacon frequency with xtal error */
                            pthread_mutex_lock(&mx_xcorr);
                            pkt.freq_hz = (uint32_t)(xtal_correct * (double)pkt.freq_hz);
                            log_debug("beacon_pkt.freq_hz=%u "
                                "(xtal_correct=%.15lf)\n",
                                pkt.freq_hz, xtal_correct);
                            pthread_mutex_unlock(&mx_xcorr);

                            log_info("Beacon dequeued (count_us=%u)\n", pkt.count_us);
                        }

                        /* check if concentrator is free for sending new packet */
                        pthread_mutex_lock(&mx_concent); /* may have to wait for a fetch to finish */
                        result = lgw_status(pkt.rf_chain, TX_STATUS, &tx_status);
                        pthread_mutex_unlock(&mx_concent); /* free concentrator ASAP */
                        if (result == -1) {
                            log_warning("[jit%d] lgw_status failed\n", i);
                        } else {
                            if (tx_status == TX_EMITTING) {
                                log_error("concentrator is currently "
                                    "emitting on rf_chain %d\n", i);
                                print_tx_status(tx_status);
                                continue;
                            } else if (tx_status == TX_SCHEDULED) {
                                log_warning("a downlink was already "
                                    "scheduled on rf_chain %d, overwritting"
                                    " it...\n", i);
                                print_tx_status(tx_status);
                            } else {
                                /* Nothing to do */
                            }
                        }

                        /* send packet to concentrator */
                        pthread_mutex_lock(&mx_concent); /* may have to wait for a fetch to finish */
                        if (spectral_scan_params.enable == true) {
                            result = lgw_spectral_scan_abort();
                            if (result != 0) {
                                log_warning("[jit%d] lgw_spectral_scan_abort failed\n", i);
                            }
                        }
                        result = lgw_send(&pkt);
                        pthread_mutex_unlock(&mx_concent); /* free concentrator ASAP */
                        if (result != 0) {
                            log_warning("[jit] lgw_send failed on rf_chain %d\n", i);
                            continue;
                        } else {
                            log_debug("lgw_send done on rf_chain %d:"
                                " count_us=%u\n", i, pkt.count_us);
                        }
                    } else {
                        log_error("jit_dequeue failed on rf_chain %d"
                            " with %d\n", i, jit_result);
                    }
                }
            } else if (jit_result == JIT_ERROR_EMPTY) {
                /* Do nothing, it can happen */
            } else {
                log_error("jit_peek failed on rf_chain %d with %d\n", i, jit_result);
            }
        }
    }

    log_debug("\nINFO: End of JIT thread\n");
}

/* -------------------------------------------------------------------------- */
/* --- THREAD 4: PARSE GPS MESSAGE AND KEEP GATEWAY IN SYNC ----------------- */

static void gps_process_sync(void) {
    struct timespec gps_time;
    struct timespec utc;
    uint32_t trig_tstamp; /* concentrator timestamp associated with PPM pulse */
    int i = lgw_gps_get(&utc, &gps_time, NULL, NULL);

    /* get GPS time for synchronization */
    if (i != LGW_GPS_SUCCESS) {
        log_warning("[gps] could not get GPS time from GPS\n");
        return;
    }

    /* get timestamp captured on PPM pulse  */
    pthread_mutex_lock(&mx_concent);
    i = lgw_get_trigcnt(&trig_tstamp);
    pthread_mutex_unlock(&mx_concent);
    if (i != 0) {
        log_warning("[gps] failed to read concentrator timestamp\n");
        return;
    }

    /* try to update time reference with the new GPS time & timestamp */
    pthread_mutex_lock(&mx_timeref);
    i = lgw_gps_sync(&time_reference_gps, trig_tstamp, utc, gps_time);
    pthread_mutex_unlock(&mx_timeref);
    if (i != LGW_GPS_SUCCESS) {
        log_warning("[gps] GPS out of sync, keeping previous time reference\n");
    }
}

static void gps_process_coords(void) {
    /* position variable */
    struct coord_s coord;
    struct coord_s gpserr;
    int    i = lgw_gps_get(NULL, NULL, &coord, &gpserr);

    /* update gateway coordinates */
    pthread_mutex_lock(&mx_meas_gps);
    if (i == LGW_GPS_SUCCESS) {
        gps_coord_valid = true;
        meas_gps_coord = coord;
        meas_gps_err = gpserr;
        // TODO: report other GPS statistics (typ. signal quality & integrity)
    } else {
        gps_coord_valid = false;
    }
    pthread_mutex_unlock(&mx_meas_gps);
}

void thread_gps(void) {
    /* serial variables */
    char serial_buff[128]; /* buffer to receive GPS data */
    size_t wr_idx = 0;     /* pointer to end of chars in buffer */

    /* variables for PPM pulse GPS synchronization */
    enum gps_msg latest_msg; /* keep track of latest NMEA message parsed */

    /* initialize some variables before loop */
    memset(serial_buff, 0, sizeof serial_buff);

    while (signalled == 0) {
        size_t rd_idx = 0;
        size_t frame_end_idx = 0;

        /* blocking non-canonical read on serial port */
        ssize_t nb_char = read(gps_tty_fd, serial_buff + wr_idx, LGW_GPS_MIN_MSG_SIZE);
        if (nb_char <= 0) {
            log_warning("[gps] read() returned value %zd\n", nb_char);
            continue;
        }
        wr_idx += (size_t)nb_char;

        /*******************************************
         * Scan buffer for UBX/NMEA sync chars and *
         * attempt to decode frame if one is found *
         *******************************************/
        while (rd_idx < wr_idx) {
            size_t frame_size = 0;

            /* Scan buffer for UBX sync char */
            if (serial_buff[rd_idx] == (char)LGW_GPS_UBX_SYNC_CHAR) {

                /***********************
                 * Found UBX sync char *
                 ***********************/
                latest_msg = lgw_parse_ubx(&serial_buff[rd_idx], (wr_idx - rd_idx), &frame_size);

                if (frame_size > 0) {
                    if (latest_msg == INCOMPLETE) {
                        /* UBX header found but frame appears to be missing bytes */
                        frame_size = 0;
                    } else if (latest_msg == INVALID) {
                        /* message header received but message appears to be corrupted */
                        log_warning("[gps] could not get a valid message from GPS (no time)\n");
                        frame_size = 0;
                    } else if (latest_msg == UBX_NAV_TIMEGPS) {
                        gps_process_sync();
                    }
                }
            } else if (serial_buff[rd_idx] == (char)LGW_GPS_NMEA_SYNC_CHAR) {
                /************************
                 * Found NMEA sync char *
                 ************************/
                /* scan for NMEA end marker (LF = 0x0a) */
                char* nmea_end_ptr = memchr(&serial_buff[rd_idx],(int)0x0a, (wr_idx - rd_idx));

                if(nmea_end_ptr) {
                    /* found end marker */
                    frame_size = nmea_end_ptr - &serial_buff[rd_idx] + 1;
                    latest_msg = lgw_parse_nmea(&serial_buff[rd_idx], frame_size);

                    if(latest_msg == INVALID || latest_msg == UNKNOWN) {
                        /* checksum failed */
                        frame_size = 0;
                    } else if (latest_msg == NMEA_RMC) { /* Get location from RMC frames */
                        gps_process_coords();
                    }
                }
            }

            if (frame_size > 0) {
                /* At this point message is a checksum verified frame
                   we're processed or ignored. Remove frame from buffer */
                rd_idx += frame_size;
                frame_end_idx = rd_idx;
            } else {
                rd_idx++;
            }
        } /* ...for(rd_idx = 0... */

        if (frame_end_idx) {
          /* Frames have been processed. Remove bytes to end of last processed frame */
          memcpy(serial_buff, &serial_buff[frame_end_idx], wr_idx - frame_end_idx);
          wr_idx -= frame_end_idx;
        } /* ...for(rd_idx = 0... */

        /* Prevent buffer overflow */
        if ((sizeof(serial_buff) - wr_idx) < LGW_GPS_MIN_MSG_SIZE) {
            memcpy(serial_buff, &serial_buff[LGW_GPS_MIN_MSG_SIZE], wr_idx - LGW_GPS_MIN_MSG_SIZE);
            wr_idx -= LGW_GPS_MIN_MSG_SIZE;
        }
    }
    log_debug("\nINFO: End of GPS thread\n");
}

/* -------------------------------------------------------------------------- */
/* --- THREAD 5: CHECK TIME REFERENCE AND CALCULATE XTAL CORRECTION --------- */

void thread_valid(void) {

    /* GPS reference validation variables */
    long gps_ref_age = 0;
    bool ref_valid_local = false;
    double xtal_err_cpy;

    /* variables for XTAL correction averaging */
    unsigned init_cpt = 0;
    double init_acc = 0.0;
    double x;

    /* correction debug */
    // FILE * log_file = NULL;
    // time_t now_time;
    // char log_name[64];

    /* initialization */
    // time(&now_time);
    // strftime(log_name,sizeof log_name,"xtal_err_%Y%m%dT%H%M%SZ.csv",localtime(&now_time));
    // log_file = fopen(log_name, "w");
    // setbuf(log_file, NULL);
    // fprintf(log_file,"\"xtal_correct\",\"XERR_INIT_AVG %u XERR_FILT_COEF %u\"\n", XERR_INIT_AVG, XERR_FILT_COEF); // DEBUG

    /* main loop task */
    while (signalled == 0) {
        wait_ms(1000);

        /* calculate when the time reference was last updated */
        pthread_mutex_lock(&mx_timeref);
        gps_ref_age = (long)difftime(time(NULL), time_reference_gps.systime);
        if ((gps_ref_age >= 0) && (gps_ref_age <= GPS_REF_MAX_AGE)) {
            /* time ref is ok, validate and  */
            gps_ref_valid = true;
            ref_valid_local = true;
            xtal_err_cpy = time_reference_gps.xtal_err;
            //printf("XTAL err: %.15lf (1/XTAL_err:%.15lf)\n", xtal_err_cpy, 1/xtal_err_cpy); // DEBUG
        } else {
            /* time ref is too old, invalidate */
            gps_ref_valid = false;
            ref_valid_local = false;
        }
        pthread_mutex_unlock(&mx_timeref);

        /* manage XTAL correction */
        if (ref_valid_local == false) {
            /* couldn't sync, or sync too old -> invalidate XTAL correction */
            pthread_mutex_lock(&mx_xcorr);
            xtal_correct_ok = false;
            xtal_correct = 1.0;
            pthread_mutex_unlock(&mx_xcorr);
            init_cpt = 0;
            init_acc = 0.0;
        } else {
            if (init_cpt < XERR_INIT_AVG) {
                /* initial accumulation */
                init_acc += xtal_err_cpy;
                ++init_cpt;
            } else if (init_cpt == XERR_INIT_AVG) {
                /* initial average calculation */
                pthread_mutex_lock(&mx_xcorr);
                xtal_correct = (double)(XERR_INIT_AVG) / init_acc;
                //printf("XERR_INIT_AVG=%d, init_acc=%.15lf\n", XERR_INIT_AVG, init_acc);
                xtal_correct_ok = true;
                pthread_mutex_unlock(&mx_xcorr);
                ++init_cpt;
                // fprintf(log_file,"%.18lf,\"average\"\n", xtal_correct); // DEBUG
            } else {
                /* tracking with low-pass filter */
                x = 1 / xtal_err_cpy;
                pthread_mutex_lock(&mx_xcorr);
                xtal_correct = xtal_correct - xtal_correct/XERR_FILT_COEF + x/XERR_FILT_COEF;
                pthread_mutex_unlock(&mx_xcorr);
                // fprintf(log_file,"%.18lf,\"track\"\n", xtal_correct); // DEBUG
            }
        }

        //printf("Time ref: %s, XTAL correct: %s (%.15lf)\n", ref_valid_local?"valid":"invalid", xtal_correct_ok?"valid":"invalid", xtal_correct); // DEBUG
    }
    log_debug("\nINFO: End of validation thread\n");
}

/* -------------------------------------------------------------------------- */
/* --- THREAD 6: BACKGROUND SPECTRAL SCAN                           --------- */

void thread_spectral_scan(void) {
    int i, x;
    uint32_t freq_hz = spectral_scan_params.freq_hz_start;
    uint32_t freq_hz_stop = spectral_scan_params.freq_hz_start + spectral_scan_params.nb_chan * 200E3;
    int16_t levels[LGW_SPECTRAL_SCAN_RESULT_SIZE];
    uint16_t results[LGW_SPECTRAL_SCAN_RESULT_SIZE];
    struct timeval tm_start;
    lgw_spectral_scan_status_t status;
    uint8_t tx_status = TX_FREE;
    bool spectral_scan_started;
    bool exit_thread = false;

    /* main loop task */
    while (signalled == 0) {
        /* Pace the scan thread (1 sec min), and avoid waiting several seconds when exit */
        for (i = 0; i < (int)(spectral_scan_params.pace_s ? spectral_scan_params.pace_s : 1); i++) {
            if (signalled) {
                exit_thread = true;
                break;
            }
            wait_ms(1000);
        }
        if (exit_thread == true) {
            break;
        }

        spectral_scan_started = false;

        /* Start spectral scan (if no downlink programmed) */
        pthread_mutex_lock(&mx_concent);
        /* -- Check if there is a downlink programmed */
        for (i = 0; i < LGW_RF_CHAIN_NB; i++) {
            if (config.radios[i].tx.enable) {
                x = lgw_status((uint8_t)i, TX_STATUS, &tx_status);
                if (x != 0) {
                    log_error("failed to get TX status on chain %d\n", i);
                } else {
                    if (tx_status == TX_SCHEDULED || tx_status == TX_EMITTING) {
                        log_info("skip spectral scan (downlink programmed on RF chain %d)\n", i);
                        break; /* exit for loop */
                    }
                }
            }
        }
        if (tx_status != TX_SCHEDULED && tx_status != TX_EMITTING) {
            x = lgw_spectral_scan_start(freq_hz, spectral_scan_params.nb_scan);
            if (x != 0) {
                log_error("spectral scan start failed\n");
                pthread_mutex_unlock(&mx_concent);
                continue; /* main while loop */
            }
            spectral_scan_started = true;
        }
        pthread_mutex_unlock(&mx_concent);

        if (spectral_scan_started == true) {
            /* Wait for scan to be completed */
            status = LGW_SPECTRAL_SCAN_STATUS_UNKNOWN;
            timeout_start(&tm_start);
            do {
                /* handle timeout */
                if (timeout_check(tm_start, 2000) != 0) {
                    log_error("%s: TIMEOUT on Spectral Scan\n", __FUNCTION__);
                    break;  /* do while */
                }

                /* get spectral scan status */
                pthread_mutex_lock(&mx_concent);
                x = lgw_spectral_scan_get_status(&status);
                pthread_mutex_unlock(&mx_concent);
                if (x != 0) {
                    log_error("spectral scan status failed\n");
                    break; /* do while */
                }

                /* wait a bit before checking status again */
                wait_ms(10);
            } while (status != LGW_SPECTRAL_SCAN_STATUS_COMPLETED && status != LGW_SPECTRAL_SCAN_STATUS_ABORTED);

            if (status == LGW_SPECTRAL_SCAN_STATUS_COMPLETED) {
                /* Get spectral scan results */
                memset(levels, 0, sizeof levels);
                memset(results, 0, sizeof results);
                pthread_mutex_lock(&mx_concent);
                x = lgw_spectral_scan_get_results(levels, results);
                pthread_mutex_unlock(&mx_concent);
                if (x != 0) {
                    log_error("spectral scan get results failed\n");
                    continue; /* main while loop */
                }

                /* FIXME: find a way to print out a binary buffer using
                   LOG_xxx macros. */
                log_info("SPECTRAL SCAN - %u Hz:\n", freq_hz);
                for (i = 0; i < LGW_SPECTRAL_SCAN_RESULT_SIZE; i++)
                    log_info("%u\n", results[i]);

                /* Next frequency to scan */
                freq_hz += 200000; /* 200kHz channels */
                if (freq_hz >= freq_hz_stop) {
                    freq_hz = spectral_scan_params.freq_hz_start;
                }
            } else if (status == LGW_SPECTRAL_SCAN_STATUS_ABORTED) {
                log_info("%s: spectral scan has been aborted\n", __FUNCTION__);
            } else {
                log_error("%s: spectral scan status us unexpected 0x%02X\n", __FUNCTION__, status);
            }
        }
    }
    log_debug("End of Spectral Scan thread\n");
}
