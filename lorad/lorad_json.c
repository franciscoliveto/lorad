/* lorad_json.c -- encoding and decoding of JSON objects.
 *
 * Copyright (c) 2021, Francisco Oliveto <franciscoliveto@gmail.com>
 * SPDX-License-Identifier: BSD-2-Clause
 */
#include "lorad_config.h" /* must be before all includes */
#include "lora/lora.h"

#include <stdio.h>
#include <math.h> /* roundf() */
#include <string.h>
#include <inttypes.h> /* PRIu64 */
#include <stdint.h> /* uint64_t */

#include "mjson.h"
#include "lorad.h"
#include "strfuncs.h"
#include "os.h"

/* FIXME: it'll be removed */
#include "loragw_gps.h"
#include "base64.h"


/* Return the JSON enconding of a GATEWAY object. */
char *json_gateway_marshal(char *buf, size_t size, uint64_t eui)
{
    snprintf(buf, size,
             "{\"eui\":\"%016"PRIX64"\",\"version\":{\"release\":\"%s\","
             "\"proto_major\":%d,\"proto_minor\":%d}}", eui, VERSION,
             PROTO_VERSION_MAJOR, PROTO_VERSION_MINOR);
    return buf;
}

/* Parses the JSON-encoded DOWNLINK object and stores the result into 'link'. */
int json_downlink_unmarshal(const char *buf, struct downlink_t *link)
{
    const struct json_attr_t attrs[] = {
        {"immediate", t_boolean, .addr.boolean = &link->immediate},
        {"hwtime", t_uinteger, .addr.uinteger = &link->hwtime,
         .dflt.uinteger = 0},
        {"gpstime", t_uinteger, .addr.uinteger = &link->gpstime,
         .dflt.uinteger = 0},
        {"frequency", t_real, .addr.real = &link->freq},
        {"radio", t_uinteger, .addr.uinteger = &link->radio},
        {"power", t_uinteger, .addr.uinteger = &link->power},
        {"modulation", t_string, .addr.string = link->modulation,
         .len = sizeof(link->modulation)},
        {"spreading_factor", t_uinteger, .addr.uinteger = &link->sf,
         .dflt.uinteger = 0},
        {"bandwidth", t_uinteger, .addr.uinteger = &link->bw,
         .dflt.uinteger = 0},
        {"coding_rate", t_string, .addr.string = link->cr,
         .len = sizeof(link->cr)},
        {"polarity", t_boolean, .addr.boolean = &link->pol,
         .dflt.boolean = true},
        {"preamble", t_uinteger, .addr.uinteger = &link->preamble,
         .dflt.uinteger = 0},
        {"frame", t_string, .addr.string = link->frame,
         .len = sizeof(link->frame)},
        {"disable_crc", t_boolean, .addr.boolean = &link->disable_crc,
         .dflt.boolean = false},
        {"disable_header", t_boolean, .addr.boolean = &link->disable_header,
         .dflt.boolean = false},

        /* ignore all unknown fields. */
        {"", t_ignore, .addr = {NULL}},
        {NULL}
    };

    return json_read_object(buf, attrs, NULL);
}

/* Returns the JSON enconding of a packet object. */
static char *json_packet_marshal(char *buf, size_t size,
                                 struct lora_packet_t *lp,
                                 bool valid_tref, struct tref tref)
{
    char frame[512];
    struct timespec ts;
    struct tm *tm;
    int status;

    strlcat(buf, "{", size);
    /* FIXME: this will eventually be removed from here. */
    if (valid_tref) {
        if (lgw_cnt2utc(tref, lp->count_us, &ts) == 0) {
            tm = gmtime(&ts.tv_sec);
            sappendf(buf, size, /* ISO8601 */
                     "\"time\":\"%04i-%02i-%02iT%02i:%02i:%02i.%06liZ\",",
                     tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday,
                     tm->tm_hour, tm->tm_min, tm->tm_sec, ts.tv_nsec/1000);
        }

        if (lgw_cnt2gps(tref, lp->count_us, &ts) == 0) {
            /* GPS time in milliseconds since 06.Jan.1980 */
            uint64_t ms = ts.tv_sec*1E3 + ts.tv_nsec/1E6;
            sappendf(buf, size, "\"gpstime\":%"PRIu64",", ms);
        }
    }

    sappendf(buf, size, "\"hwtime\":%u", lp->count_us);

    /* FIXME: PPS, fine timestamp? */
    /* fine timestamp: number of nanoseconds since last PPS. */
    /* if (lp->ftime_received) */
    /*     sappendf(buf, size, ",\"ftime\":%u", lp->ftime); */

    /* IF channel, radio, frequency, modem ID */
    sappendf(buf, size, ",\"channel\":%1u,\"radio\":%1u,"
             "\"frequency\":%.6lf,\"modem\":%u", lp->if_chain,
             lp->radio, (double)lp->freq_hz/1e6,
             lp->modem_id);

    switch (lp->status) {
    case STAT_CRC_BAD:
        status = 0;
        break;
    case STAT_NO_CRC:
        status = 1;
        break;
    case STAT_CRC_OK:
        status = 2;
        break;
    default:
        /* TODO: return error codes using errno variable. */
        return NULL;
    }
    sappendf(buf, size, ",\"status\":%d", status);

    if (lp->modulation == MOD_FSK) {
        strlcat(buf, ",\"modulation\":\"FSK\"", size);
        sappendf(buf, size, ",\"baudrate\":%u", lp->datarate);
    } else if (lp->modulation == MOD_LORA) {
#define NCODERATES 5
        int bw = 0;
        const int bwtab[] = {
            0,
            0,
            0,
            0,
            125, /* BW_125KHZ */
            250, /* BW_250KHZ */
            500 /* BW_500KHZ */
        };

        strlcat(buf, ",\"modulation\":\"LoRa\"", size);

        if (lp->bandwidth >= BW_125KHZ &&
            lp->bandwidth <= BW_500KHZ) {
            bw = bwtab[lp->bandwidth];
        }

        sappendf(buf, size, ",\"spreading_factor\":%d", lp->datarate);
        sappendf(buf, size, ",\"bandwidth\":%d", bw);

        if (lp->coderate < NCODERATES) {
            const char *crtab[NCODERATES] = {
                "OFF", /* CR_UNDEFINED (0) */
                "4/5", /* CR_LORA_4_5 (0x01) */
                "4/6", /* CR_LORA_4_6 (0x02) */
                "4/7", /* CR_LORA_4_7 (0x03) */
                "4/8" /* CR_LORA_4_8 (0x04) */
            };
            sappendf(buf, size, ",\"coding_rate\":\"%s\"", crtab[lp->coderate]);
        } else {
            /* TODO: return error codes using errno variable. */
            return NULL;
        }

        sappendf(buf, size, ",\"rssi\":%.0f", roundf(lp->rssis));
        sappendf(buf, size, ",\"snr\":%.1f", lp->snr);
        sappendf(buf, size, ",\"frequency_offset\":%d", lp->freq_offset);
    } else {
        /* TODO: return error codes using errno variable. */
        return NULL;
    }

    sappendf(buf, size, ",\"channel_rssi\":%.0f", roundf(lp->rssic));

    /* TODO: replace base64 module for a better one. */
    bin_to_b64(lp->payload, lp->size, frame, sizeof(frame));
    sappendf(buf, size, ",\"frame\":\"%s\"", frame);

    strlcat(buf, "}", size);
    return buf;
}

/* Returns the JSON enconding of a list of packet objects. */
static char *json_packets_marshal(char *buf, size_t size,
                                  struct lora_packet_t *packets, int n,
                                  bool valid_tref, struct tref tref)
{
    int i;

    strlcat(buf, "[", size);
    for (i = 0; i < n; i++) {
        if (i > 0) {
            strlcat(buf, ",", size);
        }
        if (json_packet_marshal(buf, size, &packets[i],
                                valid_tref, tref) == NULL) {
            return NULL;
        }
    }
    strlcat(buf, "]", size);
    return buf;
}

/* Returns the JSON enconding of an uplink object. */
char *json_uplink_marshal(char *buf, size_t size,
                          struct lora_packet_t *packets, int n,
                          bool valid_tref, struct tref tref)
{
    strlcpy(buf, "{\"packets\":", size);
    if (json_packets_marshal(buf, size, packets, n,
                             valid_tref, tref) == NULL) {
        return NULL;
    }
    strlcat(buf, "}", size);
    return buf;
}
