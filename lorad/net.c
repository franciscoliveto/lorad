/* net.c -- network facilities.
 *
 * Copyright (c) 2021, Francisco Oliveto <franciscoliveto@gmail.com>
 * SPDX-License-Identifier: BSD-2-Clause
 */
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include "net.h"


/* Create a passive socket on the named 'network'.
   Currently supported network is "NET_TCP".
   'node' specifies either a numerical network address or a network hostname.
   If it's NULL net_listen listens on all addresses (INADDR_ANY).
   On success, a file descriptor for the new socket is returned.
   On error, a negative number is returned, and 'errno' is set appropriately. */
int net_listen(network_t network, const char *node,
               const char *service, int backlog)
{
    struct addrinfo hints = {0};
    struct addrinfo *list, *aip;
    int sd, err;
    int enable = 1;

    switch (network) {
    case NET_TCP:
        hints.ai_family = AF_UNSPEC;     /* Allow IPv4 or IPv6 */
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_protocol = IPPROTO_TCP;
        break;
    default: /* network not supported */
        errno = EINVAL;
        return -1;
    }
    hints.ai_flags = AI_PASSIVE; /* intended to be bound for listening. */

    err = getaddrinfo(node, service, &hints, &list);
    if (err != 0) {
        errno = err;
        return -2;
    }
    /* getaddrinfo() returns a list of address structures.
       Try each address until we successfully bind(2).
       If socket(2) (or bind(2)) fails, we (close the socket
       and) try the next address. */
    for (aip = list; aip != NULL; aip = aip->ai_next) {
        sd = socket(aip->ai_family, aip->ai_socktype, aip->ai_protocol);
        if (sd == -1)
            continue;

        /* from socket(7) man page:
           SO_REUSEADDR
                     Indicates that the rules used in validating addresses
                     supplied in a bind(2) call should allow reuse of local
                     addresses. For AF_INET sockets this means that a socket
                     may bind, except when there is an active listening 
                     socket bound to the address. When the listening socket
                     is bound to INADDR_ANY with a specific port then it is
                     not possible to bind to this port for any local address.
                     Argument is an integer boolean flag. */
        if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR,
                       &enable, sizeof(enable)) == -1) {
            close(sd);
            return -1; /* FIXME: why would setsockopt(2) fail? */
        }

        if (bind(sd, aip->ai_addr, aip->ai_addrlen) == 0)
            break; /* success */

        close(sd);
    }
    if (aip == NULL) {   /* could not bind */
        freeaddrinfo(list);
        return -1;
    }

    freeaddrinfo(list);

    /* mark sd as a passive socket. */
    if (network == NET_TCP &&
        listen(sd, backlog) == -1)
        return -1;
    return sd;
}

/* Connect to the address composed by host and service on the named network.
   Currently supported networks are "NET_TCP" and "NET_UDP".
   On success, a file descriptor for the new socket is returned.
   On error, a negative number is returned, and 'errno' is set appropriately. */
int net_dial(network_t network, const char *host, const char *service)
{
    struct addrinfo hints = {0};
    struct addrinfo *list, *aip;
    int sd, err;

    /* Obtain address(es) matching host/port */

    switch (network) {
    case NET_TCP:
        hints.ai_family = AF_UNSPEC;     /* Allow IPv4 or IPv6 */
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_protocol = IPPROTO_TCP;
        break;
    case NET_UDP:
        hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
        hints.ai_socktype = SOCK_DGRAM;
        hints.ai_protocol = IPPROTO_UDP;
        break;
    default: /* network not supported */
        errno = EINVAL;
        return -1;
    }
    hints.ai_flags = 0;

    err = getaddrinfo(host, service, &hints, &list);
    if (err != 0) {
        errno = err;
        return -2;
    }

    /* getaddrinfo() returns a list of address structures.
       Try each address until we successfully connect(2).
       If socket(2) (or connect(2)) fails, we (close the socket
       and) try the next address. */

    for (aip = list; aip != NULL; aip = aip->ai_next) {
        sd = socket(aip->ai_family, aip->ai_socktype, aip->ai_protocol);
        if (sd == -1)
            continue;

        if (connect(sd, aip->ai_addr, aip->ai_addrlen) == 0)
            break; /* success */

        close(sd);
    }

    if (aip == NULL) {   /* no address succeeded */
        freeaddrinfo(list);
        return -1;
    }

    freeaddrinfo(list);
    return sd;
}

/* Return a string describing error number based on the
   return value of the previously-called net_* function. */
const char *net_strerror(int retnum, int errnum)
{
    switch (retnum) {
    case -1:
        return strerror(errnum);
    case -2:
        return gai_strerror(errnum);
    }
    return "invalid return value";
}

