/* os.c -- functions to deal with compatibility issues across Unix-like OSes.
 *
 * Copyright (c) 2021, Francisco Oliveto <franciscoliveto@gmail.com>
 * SPDX-License-Identifier: BSD-2-Clause
 */
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <time.h>


/* Suspend execution for millisecond intervals. */
int mssleep(long int msec)
{
    struct timespec ts;

    ts.tv_sec = msec / 1000;
    ts.tv_nsec = (msec % 1000) * 1000000;
    return nanosleep(&ts, NULL);
}

int daemonize()
{
    pid_t pid;
    int fd0, fd1, fd2;
    unsigned long i;
    struct rlimit rl;

    /* Clear file creation mask. */
    umask(0);

    if (getrlimit(RLIMIT_NOFILE, &rl) == -1) {
        fprintf(stderr, "can't get file limit: %s (%d)\n",
                strerror(errno), errno);
        //exit(1);
        return -1;
    }

    /* Become a session leader to lose controlling TTY. */
    if ((pid = fork()) == -1) {
        fprintf(stderr, "can't fork: %s (%d)\n", strerror(errno), errno);
        //exit(1);
        return -1;
    }
    if (pid > 0) /* parent */
        exit(0);
    setsid();

    /* Ensure future opens won't allocate controlling TTYs. */
    if ((pid = fork()) == -1) {
        fprintf(stderr, "can't fork: %s (%d)\n", strerror(errno), errno);
        //exit(1);
        return -1;
    }
    if (pid > 0) /* parent */
        exit(0);

    /* Change the current working directory to the root so
       we won't prevent file systems from being unmounted. */
    if (chdir("/") == -1) {
        fprintf(stderr, "can't change directory to /: %s (%d)\n",
                strerror(errno), errno);
        //exit(1);
        return -1;
    }

    /* Close all open file descripors. */
    if (rl.rlim_max == RLIM_INFINITY)
        rl.rlim_max = 1024;
    for (i = 0; i < rl.rlim_max; i++)
        close(i);

    /* Attach file descriptors 0, 1, and 2 to /dev/null. */
    fd0 = open("/dev/null", O_RDWR);
    fd1 = dup(0);
    fd2 = dup(0);
    if (fd0 != 0 || fd1 != 1 || fd2 != 2) {
        /* unexpected file descriptors */
        //exit(1);
        return -1;
    }
    return 0;
}


/* Copyright 2017 by the GPSD project
 * SPDX-License-Identifier: BSD-2-clause
 *
 * os_compat.c
 */

/*
 * Provide BSD strlcat()/strlcpy() on platforms that don't have it
 *
 * These versions use memcpy and strlen() because they are often
 * heavily optimized down to assembler level. Thus, likely to be
 * faster even with the function call overhead.
 */

#ifndef HAVE_STRLCAT

#include <string.h>

/*
 * Appends src to string dst of size siz (unlike strncat, siz is the
 * full size of dst, not space left).  At most siz-1 characters
 * will be copied.  Always NUL terminates (unless siz <= strlen(dst)).
 * Returns strlen(src) + MIN(siz, strlen(initial dst)).
 * If retval >= siz, truncation occurred.
 */
size_t strlcat(char *dst, const char *src, size_t siz)
{
    size_t slen = strlen(src);
    size_t dlen = strlen(dst);
    if (siz != 0) {
        if (dlen + slen < siz)
            memcpy(dst + dlen, src, slen + 1);
        else {
            memcpy(dst + dlen, src, siz - dlen - 1);
            dst[siz - 1] = '\0';
        }
    }
    return dlen + slen;
}

#ifdef __UNUSED__
/*      $OpenBSD: strlcat.c,v 1.13 2005/08/08 08:05:37 espie Exp $      */

/*
 * Copyright 1998 Todd C. Miller <Todd.Miller@courtesan.com>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

size_t strlcat(char *dst, const char *src, size_t siz)
{
    char *d = dst;
    const char *s = src;
    size_t n = siz;
    size_t dlen;

    /* Find the end of dst and adjust bytes left but don't go past end */
    while (n-- != 0 && *d != '\0')
        d++;
    dlen = (size_t) (d - dst);
    n = siz - dlen;

    if (n == 0)
        return (dlen + strlen(s));
    while (*s != '\0') {
        if (n != 1) {
            *d++ = *s;
            n--;
        }
        s++;
    }
    *d = '\0';

    return (dlen + (s - src));  /* count does not include NUL */
}
#endif /* __UNUSED__ */
#endif /* !HAVE_STRLCAT */

#ifndef HAVE_STRLCPY

#include <string.h>

/*
 * Copy src to string dst of size siz.  At most siz-1 characters
 * will be copied.  Always NUL terminates (unless siz == 0).
 * Returns strlen(src); if retval >= siz, truncation occurred.
 */
size_t strlcpy(char *dst, const char *src, size_t siz)
{
    size_t len = strlen(src);
    if (siz != 0) {
        if (len >= siz) {
            memcpy(dst, src, siz - 1);
            dst[siz - 1] = '\0';
        } else
            memcpy(dst, src, len + 1);
    }
    return len;
}

#endif /* !HAVE_STRLCPY */

/* End of strlcat()/strlcpy() section */
