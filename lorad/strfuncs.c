/* strfuncs.c -- useful string functions.
 *
 * Copyright (c) 2021, Francisco Oliveto <franciscoliveto@gmail.com>
 * SPDX-License-Identifier: BSD-2-Clause
 */
#include <string.h>
#include <stdarg.h>
#include <stdio.h>


void sappendf(char *s, size_t size, const char *fmt, ...)
{
    va_list ap;
    size_t l = strlen(s);

    va_start(ap, fmt);
    vsnprintf(s + l, size - l, fmt, ap);
    va_end(ap);
}
