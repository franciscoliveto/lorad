#!/usr/bin/env python3
"""
loradsim -- a simple lorad simulator.


Examples of JSON-encoded requests and responses.

GATEWAY={"eui":"0016c001ff19cd8a,"version":{"release":"0.1",
"proto_major":1,"proto_minor":2}}

UPLINK={"packets":[{"time":"2022-06-20T22:23:17.077059Z","gpstime":1339799016077,
"hwtime":3751873590,"channel":3,"radio":0,"freq":917.400000,"modem":8,"status":2,
"modulation":"LoRa","sf":10,"bw":125,"cr":"4/5","rssis":-87,"snr":7.5,
"freq_offset":-5555,"rssic":-86,"frame":"ABERERERERERjDQhAAujBAC6AndQS8M="}]}

ERROR={"message":"Unrecognized request"}

?DOWNLINK={"immediate":false,"freq":925.1,"radio":0,"cr":"4/5",
"frame":"IEWMdfvyX8BxZeo95RboZm8=","modulation":"LoRa","sf":10,
"bw":500,"hwtime":71549506,"power":27,"pol":true}

DOWNLINK={"status":"OK"}
DOWNLINK={"status":"ERROR","message":"Frequency 918.200000 is not supported"}
"""
import sys
import socketserver
import selectors
import json
import getopt


VERS = '0.1'
PROTO_MAJOR = 0
PROTO_MINOR = 1

# Gateway's Extended Unique Identifier (EUI-64)
EUI = '0016C001ff19CD8A'

mock_packets = [{"time":"2022-06-30T11:43:27.255374Z",
                 "gpstime":1340624626255,"hwtime":66549506,
                 "channel":3,"radio":0,"freq":917.400000,
                 "modem":8,"status":2,"modulation":"LoRa",
                 "sf":10,"bw":125,"cr":"4/5","rssis":-83,
                 "snr":11.5,"freq_offset":-6479,"rssic":-83,
                 "frame":"ABERERERERERjDQhAAujBABTDf5zkx4="}]
# TODO:
# - independent threads representing LoRa devices that simulate sending
#   packets to loradsim.
# - devices generate "frames".
# - packets are sent to the connected client as they are received.


def gateway(eui, release, proto_major, proto_minor):
    """Builds a GATEWAY event."""
    v = {'release': release,
         'proto_major': proto_major,
         'proto_minor': proto_minor}
    return 'GATEWAY=' + json.dumps({"eui": eui, "version": v},
                                   separators=(',', ':')) + '\n'

def uplink(l):
    """Builds an UPLINK event."""
    p = {'packets': l}
    return 'UPLINK=' + json.dumps(p, separators=(',', ':')) + '\n'


def downlink(arg):
    """Builds a DOWNLINK response."""
    print(arg)
    # TODO: We might also want to simulate an ERROR response here.
    return 'DOWNLINK=' + json.dumps({'status': 'OK'},
                                  separators=(',', ':')) + '\n'


def handle_get(c):
    """Handle GET requests."""
    raise Exception('unknown request %s' % c)


def handle_set(c, arg):
    """Handle SET requests."""
    if arg == "":
        raise Exception("missing request's argument")

    if c == 'DOWNLINK':
        return downlink(arg)
    else:
        raise Exception('unknown request %s' % c)
    
    
def handle(req):
    """Handle incoming requests."""
    if req[0] == '?':
        req = req[1:]
        if ';' in req:
            i = req.index(';')
            return handle_get(req[:i])
        elif '=' in req:
            s = req.split('=', maxsplit=1)
            return handle_set(s[0], s[1])
        else:
            raise Exception("missing either ';' or '=' characters")
    else:
        raise Exception("missing request's start '?' character")


class InterfaceHandler(socketserver.BaseRequestHandler):
    """
    The request handler class for our server.
    """
    def __init__(self, request, client_address, server):
        self.connected = True
        super().__init__(request, client_address, server)

    def handle(self):
        # self.request is the TCP socket connected to the client

        sys.stdout.write('new connection from %s:%s\n' %
                         (self.client_address[0],
                          self.client_address[1]))
        
        g = gateway(EUI, VERS, PROTO_MAJOR, PROTO_MINOR)
        self.request.sendall(g.encode())

        with selectors.DefaultSelector() as sel:
            sel.register(self.request, selectors.EVENT_READ)

            while self.connected:
                wait = 5  # seconds

                # block until socket 'self.request' becomes ready for reading,
                # or the timeout expires.
                ready = sel.select(wait)
                if ready:
                    try:
                        req = self.request.recv(4096).decode()
                    except ConnectionError as e:
                        sys.stderr.write(f'recv failed: {e}\n')
                        return

                    if req:
                        sys.stdout.write('request: ' + req)
                        try:
                            resp = handle(req)
                            sys.stdout.write('response: ' + resp)
                            self.request.sendall(resp.encode())
                        except Exception as e:
                            sys.stderr.write(str(e) + '\n')
                    else:
                        sys.stdout.write('connection has been closed\n')
                        self.connected = False
                else:
                    e = uplink(mock_packets)
                    sys.stdout.write(f'Forwarding UPLINK packets... \n{e}')
                    self.request.sendall(e.encode())


if __name__ == "__main__":
    def usage(progname):
        sys.stdout.write(
            f"""Usage: {progname} [-h HOST, --host=HOST] [-p PORT, --port=PORT] [--help] [--version]
                
HOST is the hostname or IPv4 address where the server is binding to. Default is localhost.
PORT is the listening port. Default is 2960.\n"""
        )
        sys.exit(0)

    long_opts = ['host', 'port', 'help', 'version']
    try:
        (opts, args) = getopt.getopt(sys.argv[1:], "h:p:", long_opts)
    except getopt.GetoptError as e:
        sys.exit(str(e))

    host, port = "localhost", 2960

    for opt, optarg in opts:
        if opt in ('-h', '--host'):
            host = optarg
        elif opt in ('-p', '--port'):
            port = optarg
        elif opt == '--help':
            usage(sys.argv[0])
        elif opt == '--version':
            sys.stdout.write(f'Version: {VERS}\n')
            sys.exit(0)

    try:
        socketserver.TCPServer.allow_reuse_address = True
        # Create the server
        with socketserver.TCPServer((host, port), InterfaceHandler) as server:
            # Activate the server; this will keep running until you
            # interrupt the program with Ctrl-C
            server.serve_forever()
    except KeyboardInterrupt:
        sys.stderr.write('aborted.\n')
