/* test_net.c -- test for net.c: net_listen(), net_dial().
 *
 * Copyright (c) 2021, Francisco Oliveto <franciscoliveto@gmail.com>
 * SPDX-License-Identifier: BSD-2-Clause
 */
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <arpa/inet.h>
#include <string.h>

#include "net.h"


int main()
{
    const char *service = "2960";
    const char *host = "localhost";
    int backlog = 1;
    int sfd = -1;
    fd_set rfds;
    struct timespec ts;


    if ((sfd = net_listen(NET_TCP, host, service, backlog)) < 0) {
        fprintf(stderr, "can't listen on port %s: %s\n",
                service, net_strerror(sfd, errno));
        exit(EXIT_FAILURE);
    }

    for (;;) {
        int r;

        FD_ZERO(&rfds);
        FD_SET(sfd, &rfds);

        ts.tv_sec = 5;
        ts.tv_nsec = 0;

        printf("waiting for incoming connections"
               " on port %s.\n", service);

        if ((r = pselect(sfd+1, &rfds, NULL, NULL, &ts, NULL)) == -1) {
            if (errno == EINTR) {
                fputs("pselect interrupted by signal.\n", stderr);
                continue;
            }
            fprintf(stderr, "pselect failed: %s (%d).\n",
                    strerror(errno), errno);
            exit(EXIT_FAILURE);
        }
        if (r == 0)
            fputs("pselect timeout.\n", stdout);
        else {
            if (FD_ISSET(sfd, &rfds)) {
                struct sockaddr addr;
                socklen_t addrsiz = sizeof(addr);
                int sock;

                if ((sock = accept(sfd, &addr, &addrsiz)) == -1)
                    fprintf(stderr, "accept failed: %s (%d).\n",
                            strerror(errno), errno);
                else {
                    printf("client connected on socket %d.\n", sock);
                    puts("drop connection now.");
                    close(sock);
                }
            }
        }
    }

    return EXIT_SUCCESS;
}
