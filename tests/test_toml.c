/* test_toml.c -- test for toml.c.
 *
 * Copyright (c) 2022, Francisco Oliveto <franciscoliveto@gmail.com>
 * SPDX-License-Identifier: BSD-2-Clause
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>

#include "toml.h"
#include "config.h"


static void assert_real(const char *key, double want, double got)
{
    if (want != got) {
        fprintf(stderr, "'%s' expecting '%f', got '%f'.\n",
                key, want, got);
        exit(EXIT_FAILURE);
    }
}

static void assert_boolean(const char *key, bool want, bool got)
{
    if (want != got) {
        fprintf(stderr, "'%s' expecting '%s', got '%s'.\n",
                key, want ? "true" : "false", got ? "true" : "false");
        exit(EXIT_FAILURE);
    }
}

static void assert_signed_integer(const char *key, long int want, long int got)
{
    if (want != got) {
        fprintf(stderr, "'%s' expecting '%ld', got '%ld'.\n",
                key, want, got);
        exit(EXIT_FAILURE);
    }
}

static void assert_string(const char *key, const char *want, const char *got)
{
    if (strcmp(got, want)) {
        fprintf(stderr,"fail: '%s' expecting '%s', got '%s'.\n",
                key, want, got);
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char *argv[])
{
    int ret = EXIT_FAILURE;
    int status;
    FILE *fp;
    struct config got = {0};
    const struct toml_key_t gps_keys[] = {
        {"device", string_t, .addr.string = got.gps.device,
         .len = sizeof(got.gps.device)},
        {"latitude", real_t, .addr.real = &got.gps.lat},
        {"longitude", real_t, .addr.real = &got.gps.lon},
        {"altitude", real_t, .addr.real = &got.gps.alt}
    };
    const struct toml_key_t beacon_keys[] = {
        {"period", integer_t, .addr.integer = &got.beacon.period},
        {"freq_nb", integer_t, .addr.integer = &got.beacon.freq.nb},
        {"freq_hz", integer_t, .addr.integer = &got.beacon.freq.hz},
        {"freq_step", integer_t, .addr.integer = &got.beacon.freq.step},
        {"datarate", integer_t, .addr.integer = &got.beacon.dr},
        {"bw_hz", integer_t, .addr.integer = &got.beacon.bw},
        {"power", integer_t, .addr.integer = &got.beacon.power},
        {"beacon_infodesc", integer_t, .addr.integer = &got.beacon.id}
    };
    const struct toml_key_t channels_keys[] = {
        {"enable", boolean_t, TABLEFIELD(struct channel, enable)},
        {"radio", integer_t, TABLEFIELD(struct channel, radio)},
        {"if", integer_t, TABLEFIELD(struct channel, if_freq)}
    };
    const struct toml_key_t lora_channel_keys[] = {
        {"enable", boolean_t, .addr.boolean = &got.chan_lora.enable},
        {"radio", integer_t, .addr.integer = &got.chan_lora.radio},
        {"if", integer_t, .addr.integer = &got.chan_lora.if_freq},
        {"bandwidth", integer_t, .addr.integer = &got.chan_lora.bw},
        {"spread_factor", integer_t, .addr.integer = &got.chan_lora.sf},
        {"implicit_hdr", boolean_t, .addr.boolean = &got.chan_lora.implicit_hdr},
        {"implicit_crc_en", boolean_t,
         .addr.boolean = &got.chan_lora.implicit_crc},
        {"implicit_payload_length", integer_t,
         .addr.integer = &got.chan_lora.implicit_payload_len},
        {"implicit_coderate", integer_t,
         .addr.integer = &got.chan_lora.implicit_coderate}
    };
    const struct toml_key_t gain_lut_keys[] = {
        {"rf_power", integer_t, TABLEFIELD(struct gain_lut, rf_power)},
        {"pa_gain", integer_t, TABLEFIELD(struct gain_lut, pa_gain)},
        {"pwr_idx", integer_t, TABLEFIELD(struct gain_lut, pwr_idx)},
        {NULL}
    };
    const struct toml_key_t radio_0_keys[] = {
        {"enable", boolean_t, .addr.boolean = &got.radios[0].enable},
        {"type", string_t, .addr.string = got.radios[0].type,
         .len = sizeof(got.radios[0].type)},
        {"freq", integer_t, .addr.integer = &got.radios[0].freq},
        {"rssi_offset", real_t, .addr.real = &got.radios[0].rssi_offset},
        {"rssi_tcomp_coeff_a", real_t,
         .addr.real = &got.radios[0].rssi_tcomp.coeff_a},
        {"rssi_tcomp_coeff_b", real_t,
         .addr.real = &got.radios[0].rssi_tcomp.coeff_b},
        {"rssi_tcomp_coeff_c", real_t,
         .addr.real = &got.radios[0].rssi_tcomp.coeff_c},
        {"rssi_tcomp_coeff_d", real_t,
         .addr.real = &got.radios[0].rssi_tcomp.coeff_d},
        {"rssi_tcomp_coeff_e", real_t,
         .addr.real = &got.radios[0].rssi_tcomp.coeff_e},
        {"tx_enable", boolean_t, .addr.boolean = &got.radios[0].tx.enable},
        {"tx_freq_min", integer_t, .addr.integer = &got.radios[0].tx.freq_min},
        {"tx_freq_max", integer_t, .addr.integer = &got.radios[0].tx.freq_max},
        {"tx_gain_lut", array_t,
         TABLEARRAY(got.radios[0].tx.gains, gain_lut_keys,
                    &got.radios[0].tx.ngains)},
        {NULL}
    };
    const struct toml_key_t radio_1_keys[] = {
        {"enable", boolean_t, .addr.boolean = &got.radios[1].enable},
        {"type", string_t, .addr.string = got.radios[1].type,
         .len = sizeof(got.radios[1].type)},
        {"freq", integer_t, .addr.integer = &got.radios[1].freq},
        {"rssi_offset", real_t, .addr.real = &got.radios[1].rssi_offset},
        {"rssi_tcomp_coeff_a", real_t,
         .addr.real = &got.radios[1].rssi_tcomp.coeff_a},
        {"rssi_tcomp_coeff_b", real_t,
         .addr.real = &got.radios[1].rssi_tcomp.coeff_b},
        {"rssi_tcomp_coeff_c", real_t,
         .addr.real = &got.radios[1].rssi_tcomp.coeff_c},
        {"rssi_tcomp_coeff_d", real_t,
         .addr.real = &got.radios[1].rssi_tcomp.coeff_d},
        {"rssi_tcomp_coeff_e", real_t,
         .addr.real = &got.radios[1].rssi_tcomp.coeff_e},
        {"tx_enable", boolean_t, .addr.boolean = &got.radios[1].tx.enable},
        {"tx_freq_min", integer_t, .addr.integer = &got.radios[1].tx.freq_min},
        {"tx_freq_max", integer_t, .addr.integer = &got.radios[1].tx.freq_max},
        {"tx_gain_lut", array_t,
         TABLEARRAY(got.radios[1].tx.gains, gain_lut_keys,
                    &got.radios[1].tx.ngains)},
        {NULL}
    };
    const struct toml_key_t config_keys[] = {
        {"type", string_t, .addr.string = got.type,
         .len = sizeof(got.type)},
        {"device", string_t, .addr.string = got.device,
         .len = sizeof(got.device)},
        {"clksrc", integer_t, .addr.integer = &got.clksrc},
        {"antenna_gain", integer_t, .addr.integer = &got.antenna_gain},
        {"lorawan_public", boolean_t, .addr.boolean = &got.lorawan_public},
        {"full_duplex", boolean_t, .addr.boolean = &got.full_duplex},
        {"fine_timestamp_enable", boolean_t, .addr.boolean = &got.ts.enable},
        {"fine_timestamp_mode", string_t, .addr.string = got.ts.mode,
         .len = sizeof(got.ts.mode)},
        {"channels", array_t,
         TABLEARRAY(got.channels, channels_keys, &got.nchannels)},
        {"LoRa_channel", table_t, .addr.keys = lora_channel_keys},
        {"radio_0", table_t, .addr.keys = radio_0_keys},
        {"radio_1", table_t, .addr.keys = radio_1_keys},
        {"beacon", table_t, .addr.keys = beacon_keys},
        {"gps", table_t, .addr.keys = gps_keys},
        {NULL}
    };

    struct config want = {
        .gps = {
            .device = "/dev/ttyAMA0",
            .lat = 0.0,
            .lon = 0.0,
            .alt = 0.0
        },
        .beacon = {
            .period = 0,
            .freq = {
                .hz = 923300000,
                .nb = 8,
                .step = 600000,
            },
            .dr = 12,
            .bw = 500000,
            .power = 27,
        },
        .type = "SPI",
        .device = "/dev/spidev0.0",
        .lorawan_public = true,
        .clksrc = 0,
        .antenna_gain = 0,
        .full_duplex = false,
        .ts = {
            .enable = false,
            .mode = "all_sf"
        },
        .radios[0] = {
            .enable = true,
            .type = "SX1250",
            .freq = 917200000,
            .rssi_offset = -215.4,
            .rssi_tcomp = {
                .coeff_a = 0,
                .coeff_b = 0,
                .coeff_c = 20.41,
                .coeff_d = 2162.56,
                .coeff_e = 0
            },
            .tx = {
                .enable = true,
                .freq_min = 915000000,
                .freq_max = 928000000,
                .ngains = 16,
                .gains[0] = {
                    .rf_power = 12,
                    .pa_gain = 1,
                    .pwr_idx = 6
                },
                .gains[1] = {
                    .rf_power = 13,
                    .pa_gain = 1,
                    .pwr_idx = 7
                },
                .gains[2] = {
                    .rf_power = 14,
                    .pa_gain = 1,
                    .pwr_idx = 8
                },
                .gains[3] = {
                    .rf_power = 15,
                    .pa_gain = 1,
                    .pwr_idx = 9
                },
                .gains[4] = {
                    .rf_power = 16,
                    .pa_gain = 1,
                    .pwr_idx = 10
                },
                .gains[5] = {
                    .rf_power = 17,
                    .pa_gain = 1,
                    .pwr_idx = 11
                },
                .gains[6] = {
                    .rf_power = 18,
                    .pa_gain = 1,
                    .pwr_idx = 12
                },
                .gains[7] = {
                    .rf_power = 19,
                    .pa_gain = 1,
                    .pwr_idx = 13
                },
                .gains[8] = {
                    .rf_power = 20,
                    .pa_gain = 1,
                    .pwr_idx = 14
                },
                .gains[9] = {
                    .rf_power = 21,
                    .pa_gain = 1,
                    .pwr_idx = 15
                },
                .gains[10] = {
                    .rf_power = 22,
                    .pa_gain = 1,
                    .pwr_idx = 16
                },
                .gains[11] = {
                    .rf_power = 23,
                    .pa_gain = 1,
                    .pwr_idx = 17
                },
                .gains[12] = {
                    .rf_power = 24,
                    .pa_gain = 1,
                    .pwr_idx = 18
                },
                .gains[13] = {
                    .rf_power = 25,
                    .pa_gain = 1,
                    .pwr_idx = 19
                },
                .gains[14] = {
                    .rf_power = 26,
                    .pa_gain = 1,
                    .pwr_idx = 21
                },
                .gains[15] = {
                    .rf_power = 27,
                    .pa_gain = 1,
                    .pwr_idx = 22
                },
            }
        },
        .radios[1] = {
            .enable = true,
            .type = "SX1250",
            .freq = 917900000,
            .rssi_offset = -215.4,
            .rssi_tcomp = {
                .coeff_a = 0,
                .coeff_b = 0,
                .coeff_c = 20.41,
                .coeff_d = 2162.56,
                .coeff_e = 0
            },
            .tx = {
                .enable = false
            }
        },
        .nchannels = 8,
        .channels[0] = {
            .enable = true,
            .radio = 0,
            .if_freq = -400000
        },
        .channels[1] = {
            .enable = true,
            .radio = 0,
            .if_freq = -200000
        },
        .channels[2] = {
            .enable = true,
            .radio = 0,
            .if_freq = 0
        },
        .channels[3] = {
            .enable = true,
            .radio = 0,
            .if_freq = 200000
        },
        .channels[4] = {
            .enable = true,
            .radio = 1,
            .if_freq = -300000
        },
        .channels[5] = {
            .enable = true,
            .radio = 1,
            .if_freq = -100000
        },
        .channels[6] = {
            .enable = true,
            .radio = 1,
            .if_freq = 100000
        },
        .channels[7] = {
            .enable = true,
            .radio = 1,
            .if_freq = 300000
        },
        .chan_lora = {
            .enable = true,
            .radio = 0,
            .if_freq = 300000,
            .bw = 500000,
            .sf = 8,
            .implicit_hdr = false,
            .implicit_crc = false,
            .implicit_payload_len = 17,
            .implicit_coderate = 1
        }
    };

    if (argc != 2) {
        fprintf(stderr, "Usage: %s file\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    if ((fp = fopen(argv[1], "r")) == NULL)
        fprintf(stderr, "Can't open file \"%s\".\n", argv[1]);
    else if ((status = toml_load(fp, config_keys)) == -1)
        fprintf(stderr, "toml_load failed: %d.\n", status);
    else {
        fclose(fp);
        fputs("Test TOML: ", stderr);

        assert_string("type", want.type, got.type);
        assert_string("device", want.device, got.device);
        assert_boolean("lorawan_public", want.lorawan_public,
                       got.lorawan_public);
        assert_signed_integer("clksrc", want.clksrc, got.clksrc);
        assert_signed_integer("antenna_gain", want.antenna_gain,
                              got.antenna_gain);
        assert_boolean("full_duplex", want.full_duplex, got.full_duplex);
        assert_boolean("fine_timestamp_enable", want.ts.enable, got.ts.enable);
        assert_string("fine_timestamp_mode", want.ts.mode, got.ts.mode);
        assert_boolean("radio_0.enable", want.radios[0].enable,
                       got.radios[0].enable);
        assert_string("radio_0.type", want.radios[0].type,
                      got.radios[0].type);
        assert_signed_integer("radio_0.freq", want.radios[0].freq,
                              got.radios[0].freq);
        assert_real("radio_0.rssi_offset", want.radios[0].rssi_offset,
                    got.radios[0].rssi_offset);
        assert_real("radio_0.rssi_tcomp_coeff_a",
                    want.radios[0].rssi_tcomp.coeff_a,
                    got.radios[0].rssi_tcomp.coeff_a);
        assert_real("radio_0.rssi_tcomp_coeff_b",
                    want.radios[0].rssi_tcomp.coeff_b,
                    got.radios[0].rssi_tcomp.coeff_b);
        assert_real("radio_0.rssi_tcomp_coeff_c",
                    want.radios[0].rssi_tcomp.coeff_c,
                    got.radios[0].rssi_tcomp.coeff_c);
        assert_real("radio_0.rssi_tcomp_coeff_d",
                    want.radios[0].rssi_tcomp.coeff_d,
                    got.radios[0].rssi_tcomp.coeff_d);
        assert_real("radio_0.rssi_tcomp_coeff_e",
                    want.radios[0].rssi_tcomp.coeff_e,
                    got.radios[0].rssi_tcomp.coeff_e);
        assert_boolean("radio_0.tx_enable", want.radios[0].tx.enable,
                       got.radios[0].enable);
        assert_signed_integer("radio_0.tx_freq_min",
                              want.radios[0].tx.freq_min,
                              got.radios[0].tx.freq_min);
        assert_signed_integer("radio_0.tx_freq_min",
                              want.radios[0].tx.freq_max,
                              got.radios[0].tx.freq_max);
        assert_signed_integer("ngains", want.radios[0].tx.ngains,
                              got.radios[0].tx.ngains);

        for (int8_t i = 0; i < (int8_t) got.radios[0].tx.ngains; i++) {
            char key[40];

            snprintf(key, sizeof(key), "radio_0.tx_gain_lut[%d].rf_power", i);
            assert_signed_integer(key, want.radios[0].tx.gains[i].rf_power,
                                  got.radios[0].tx.gains[i].rf_power);
            snprintf(key, sizeof(key), "radio_0.tx_gain_lut[%d].pa_gain", i);
            assert_signed_integer(key, want.radios[0].tx.gains[i].pa_gain,
                                  got.radios[0].tx.gains[i].pa_gain);
            snprintf(key, sizeof(key), "radio_0.tx_gain_lut[%d].pwr_idx", i);
            assert_signed_integer(key, want.radios[0].tx.gains[i].pwr_idx,
                                  got.radios[0].tx.gains[i].pwr_idx);
        }

        assert_boolean("radio_1.enable", want.radios[1].enable,
                       got.radios[1].enable);
        assert_string("radio_1.type", want.radios[1].type,
                      got.radios[1].type);
        assert_signed_integer("radio_1.freq", want.radios[1].freq,
                              got.radios[1].freq);
        assert_real("radio_1.rssi_offset", want.radios[1].rssi_offset,
                    got.radios[1].rssi_offset);
        assert_real("radio_1.rssi_tcomp_coeff_a",
                    want.radios[1].rssi_tcomp.coeff_a,
                    got.radios[1].rssi_tcomp.coeff_a);
        assert_real("radio_1.rssi_tcomp_coeff_b",
                    want.radios[1].rssi_tcomp.coeff_b,
                    got.radios[1].rssi_tcomp.coeff_b);
        assert_real("radio_1.rssi_tcomp_coeff_c",
                    want.radios[1].rssi_tcomp.coeff_c,
                    got.radios[1].rssi_tcomp.coeff_c);
        assert_real("radio_1.rssi_tcomp_coeff_d",
                    want.radios[1].rssi_tcomp.coeff_d,
                    got.radios[1].rssi_tcomp.coeff_d);
        assert_real("radio_1.rssi_tcomp_coeff_e",
                    want.radios[1].rssi_tcomp.coeff_e,
                    got.radios[1].rssi_tcomp.coeff_e);
        assert_boolean("radio_1.tx_enable", want.radios[1].tx.enable,
                       got.radios[1].tx.enable);

        assert_signed_integer("nchannels", want.nchannels, got.nchannels);
        for (int8_t i = 0; i < (int8_t) got.nchannels; i++) {
            char key[40];

            snprintf(key, sizeof(key), "channels[%d].enable", i);
            assert_boolean(key, want.channels[i].enable,
                           got.channels[i].enable);
            snprintf(key, sizeof(key), "channels[%d].radio", i);
            assert_signed_integer(key, want.channels[i].radio,
                                  got.channels[i].radio);
            snprintf(key, sizeof(key), "channels[%d].if", i);
            assert_signed_integer(key, want.channels[i].if_freq,
                                  got.channels[i].if_freq);
        }

        assert_boolean("LoRa_channel.enable", want.chan_lora.enable,
                       got.chan_lora.enable);
        assert_signed_integer("LoRa_channel.radio", want.chan_lora.radio,
                              got.chan_lora.radio);
        assert_signed_integer("LoRa_channel.if", want.chan_lora.if_freq,
                              got.chan_lora.if_freq);
        assert_signed_integer("LoRa_channel.spread_factor", want.chan_lora.sf,
                              got.chan_lora.sf);
        assert_signed_integer("LoRa_channel.bandwidth", want.chan_lora.bw,
                              got.chan_lora.bw);
        assert_signed_integer("LoRa_channel.implicit_payload_length",
                              want.chan_lora.implicit_payload_len,
                              got.chan_lora.implicit_payload_len);
        assert_signed_integer("LoRa_channel.implicit_coderate",
                              want.chan_lora.implicit_coderate,
                              got.chan_lora.implicit_coderate);
        assert_boolean("LoRa_channel.implicit_crc_en",
                       want.chan_lora.implicit_crc,
                       got.chan_lora.implicit_crc);
        assert_boolean("LoRa_channel.implicit_hdr",
                       want.chan_lora.implicit_hdr,
                       got.chan_lora.implicit_hdr);

        assert_string("gps.device", want.gps.device, got.gps.device);
        assert_real("gps.latitude", want.gps.lat, got.gps.lat);
        assert_real("gps.longitude", want.gps.lon, got.gps.lon);
        assert_real("gps.altitude", want.gps.alt, got.gps.alt);
        assert_signed_integer("beacon.period", want.beacon.period,
                              got.beacon.period);
        assert_signed_integer("beacon.freq_hz", want.beacon.freq.hz,
                              got.beacon.freq.hz);
        assert_signed_integer("beacon.freq_nb", want.beacon.freq.nb,
                              got.beacon.freq.nb);
        assert_signed_integer("beacon.freq_step", want.beacon.freq.step,
                              got.beacon.freq.step);
        assert_signed_integer("beacon.datarate", want.beacon.dr,
                              got.beacon.dr);
        assert_signed_integer("beacon.bw_hz", want.beacon.bw,
                              got.beacon.bw);
        assert_signed_integer("beacon.power", want.beacon.power,
                              got.beacon.power);

        fputs("ok\n", stderr);
        ret = EXIT_SUCCESS;
    }
    return ret;
}
